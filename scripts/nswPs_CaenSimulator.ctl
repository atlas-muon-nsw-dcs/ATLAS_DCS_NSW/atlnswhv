/* *****************************************************************************
 * Simulation of CAEN Power system 
 * by: S. Zimmermann
 * Mods:  05/05/2009  added simulation for BrCtrl Vsel settings
 *        03/06/2009  added EASY Crate remote reset simulation
 *        16/01/2010  added SY1527 GenSignCfg simulation
 *        23/02/2011  added local system to sysList for all simulate functions which
 *                    propagate values to more than 1 system
 *        23/02/2011  added v0, v1 transition (ramp) simulation on change of mainframe vSel
 *        26/09/2011  fixed interim ON state immediately after switch on for a HV channel before
 *                    state became ramping, since it was not as hardware behaves
 *        26/09/2011  fixed channel tobe turned on again when manually setting actual.status to a TRIP.
 * ***************************************************************************** */

// #uses "mdtConstants.ctl"
// #uses "mdtUtil.ctl"
#uses "nswPsUtil.ctl"

const bool simulateChannelStatus = TRUE;
const bool simulateVoltageAndCurrents = FALSE;
const bool simulateSettings = TRUE;                  // propagate settings to readBackSettings
const bool simulateVselSettings = TRUE;              // propagate BrCtrl .Commands.Vsel to .Actual.Vsel, also
                                                     // in remote connected projects (PS1 --> PS2, PS3)
const bool simulateRemoteCrateReset = TRUE;
const bool simulateSY1527GenSignCfg = TRUE;
const bool simulateSY1527BaseSettings = TRUE;

const bool simulateSY1527VselV0V1Transition = TRUE;  // simulate ramp between v0/v1 if mainframe vSel changes

main () {
  
  dyn_string chanList, brCtrlList, mainframeList;
  int chanNum;
              
  chanList = dpNames("*", "FwCaenChannel");
  chanNum = dynlen(chanList);
  information("Found " + chanNum + " channels in the local system. Now setting up simulation ...");
  
  if (simulateChannelStatus) {
    information("Setting up channel status simulation ...");
    //setChannelsOff(chanList);  
    for (int i = 1; i <= chanNum; i++) {
      dpConnect("simulateChannelStatus", FALSE, chanList[i] + ".settings.onOff", chanList[i] + ".actual.status");
    }
  }
  
  if (simulateSY1527VselV0V1Transition) {
    mainframeList = dpNames("*", "FwCaenCrateSY1527");
    information("Found " + dynlen(mainframeList) + " mainframes in the local system. " +
                "Setting up V0 - V1 transition simulation depending on Vsel ..."); 
    for (int i = 1; i <=dynlen(mainframeList); i++) {
      dpConnect("simulateSY1527Vsel", FALSE, mainframeList[i] + ".FrontPanInP.Vsel");
    }
  }
  if (simulateSettings) {
     information("Setting up channel settings simulation ...");
    for (int i = 1; i <= chanNum; i++) {
      dpConnect("simulateSettings", FALSE, chanList[i] + ".settings.v0", chanList[i] + ".settings.v1",
                chanList[i] + ".settings.i0", chanList[i] + ".settings.rUp", chanList[i] + ".settings.rDwn");
    }
  }
  
  if (simulateVselSettings) {
    brCtrlList = dpNames("*", "FwCaenBoardSY1527A1676");
    information("Found " + dynlen(brCtrlList) + " A1676 branch controllers in the local system. " +
                "Now setting up Vsel simulation ...");
  
    for (int i = 1; i <= dynlen(brCtrlList); i++) {
      dpConnect("simulateVselSettings", brCtrlList[i] + ".Commands.Vsel");
    }
  }
  if (simulateRemoteCrateReset) {
    brCtrlList = dpNames("*", "FwCaenBoardSY1527A1676");
    information("Found " + dynlen(brCtrlList) + " A1676 branch controllers in the local system. " +
                "Now setting up Crate Reset simulation ...");
    for (int i = 1; i <= dynlen(brCtrlList); i++) {
      for (int j = 0; j <= 5; j++) {
        dpConnect("simulateCrateReset", brCtrlList[i] + ".Commands.RemoteCrates.Recovery" + j,
                  brCtrlList[i] + ".Commands.RemoteCrates.HwReset" + j);
      }
    }
  }
  if (simulateSY1527GenSignCfg) {
    mainframeList = dpNames("*", "FwCaenCrateSY1527");
    information("Found " + dynlen(mainframeList) + " mainframes in the local system. " +
                "Now setting up GenOut Config simulation ...");
    for (int i = 1; i <= dynlen(mainframeList); i++) {
      dpConnect("simulateSY1527GenSignCfg", mainframeList[i] + ".Commands.GenSignCfg");
    }
  }
  
  if (simulateSY1527BaseSettings) {
    mainframeList = dpNames("*", "FwCaenCrateSY1527");
    information("Found " + dynlen(mainframeList) + " mainframes in the local system. " +
                "Now setting up base settings simulation ...");
    for (int i = 1; i <= dynlen(mainframeList); i++) {
      dpConnect("simulateSY1527BaseSettings", mainframeList[i] + ".userDefined.Commands.SymbolicName");
    }
  }
      
  information("Simulation setup completed.");
    
}


void setChannelsOff(dyn_string chanList) {
  
  for (int i = 1; i <= dynlen(chanList); i++) {
    dpSet(chanList[i] + ".actual.status", 0, chanList[i] + ".actual.vMon", 0, chanList[i] + ".actual.iMon", 0);
  }
}  

void setChannelOffWithDelay(string chanDp, int delayTime) {
  
  int status;
  int secs = 0;
  float voltage;
  
  if (delayTime <= 10) delay(delayTime);
  else {
    dpGet(chanDp + ".actual.vMon", voltage);
    while(secs < delayTime) {
      delay(((delayTime - secs) < 10) ? (delayTime - secs) : 10);
      secs += ((delayTime - secs) < 10) ? (delayTime - secs) : 10;
      dpSetWait(chanDp + ".actual.vMon", voltage * (1 - ((float) secs/delayTime)));
    }
  }
  
  dpGet(chanDp + ".actual.status", status);
   
  if (nswPsUtil_isOn(status) && !nswPsUtil_isRampUp(status)) {
// Take care of intermittent ON state at the end of ramp down, see notes.    
    dpSet(chanDp + ".actual.status", 1, chanDp + ".actual.vMon", 0, chanDp + ".actual.iMon", 0);  
    delay(0, 500);
    dpSet(chanDp + ".actual.status", 0);
  }
}

void setChannelOnWithDelay(string chanDp, int delayTime, float voltage, bool ignoreRampDown = FALSE) {
  
  int status;
  int secs = 0;
  
  if (delayTime <= 10) delay(delayTime);
  else {
    while(secs < delayTime) {
      delay(((delayTime - secs) < 10) ? (delayTime - secs) : 10);
      secs += ((delayTime - secs) < 10) ? (delayTime - secs) : 10;
      dpSetWait(chanDp + ".actual.vMon", voltage * ((float) secs)/delayTime);
    }
  }
  dpGet(chanDp + ".actual.status", status);
   
  if (ignoreRampDown || !nswPsUtil_isRampDown(status))
    dpSetWait(chanDp + ".actual.status", 1, chanDp + ".actual.vMon", voltage, chanDp + ".actual.iMon", 2);
}


///////////////////////// Callback functions /////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////
// simulate CAEN channel status 
// Note: CAEN hardware behavior when switching OFF a channel with non-zero ramp-down time (HV)
//       is the following:
//        ON: bit 0 set --> GOTO_OFF --> RAMP_DOWN: bits 0 and 2 set --> voltage down: bit 0 set, bit 0 not 
//        set ("ON") intermittent --> status = 0 (OFF).
//        This beavior is normally not seen when running in OPC server polling mode, where status word is 
//        updsted only every xxx seconds, put may become relevant in event mode.     
//////////////////////////////////////////////////////////////////////////////////////////////

void simulateChannelStatus(string dpe1, int onOff, string dpe2, int status) {
  
  float rampUp, rampDown, v0, vMon;
  int val;
  string chanDp;
  string model;
  
  chanDp = dpSubStr(dpe1, DPSUB_DP);
  
  //dpGet(chanDp + ".settings.onOff", onOff, chanDp + ".actual.status", status);  // re-read values, this is for the case many calls
                                                                                  // of the callback have queued to be sure the 
                                                                                  //CURRENT values are indeed used.
  
  if (nswPsUtil_isTrip(status) && nswPsUtil_isOn(status)) {                   // TRIP flag present. 
    dpSetWait(chanDp + ".settings.onOff", 0,                                  // need to clear onOff to avoid callback to repower channel immediately
          chanDp + ".actual.status", status & 0xffffff8);                     // set last 3 status bits to '0', leave rest untouched.
    dpSetWait(chanDp + ".actual.vMon", 0, chanDp + ".actual.iMon", 0);        // this clears any ramping and On indication.
    return;
  }
  else if ((!onOff) && (nswPsUtil_isOn(status) == FALSE)) return;                  // channel already OFF
  
  else if (!onOff && (nswPsUtil_isOn(status) == TRUE)) {                      // channel is ON, switch it off.
    dpGet(chanDp + ".model", model);
//     if (mdtPsUtil_isCaenLvBoard(model)) {                                     // No ramp bits set in status for LV type boards
//       delay(2);                                                               // delay to simulate communication latency
//       dpGet(chanDp + ".actual.vMon", vMon);
// /*      if (vMon > 0) {
//         dpSet(chanDp + ".actual.status", 1, chanDp + ".actual.vMon", 0,       // take care of intermittent ON state at the end 
//               chanDp + ".actual.iMon", 0);                                    // of a ramp down, see notes
//         delay(0, 500);
//       }                                                                     
// */                                                                            // --> not for LV channels which do not have ramp states
//       dpSetWait(chanDp + ".actual.status", 0, chanDp + ".actual.vMon", 0,
//                 chanDp + ".actual.iMon", 0);
//     }
//     else {                                                                    // HV type board. Here we have a ramp state.
      dpGet(chanDp + ".settings.rDwn", rampDown, chanDp + ".actual.vMon", vMon);
      if (vMon == 0.0) dpSetWait(chanDp + ".actual.status", 0);
      else {
        if (status != 0x5) dpSetWait(chanDp + ".actual.status", 0x5);
        delay(0, 200);
        startThread("setChannelOffWithDelay", chanDp, (int) (rampDown > 0 ? vMon/rampDown : 0));
      }
//     }
    return;
  }
  
  else if (onOff && nswPsUtil_isOn(status)) return;                           // channel already ON
  
  else if (onOff && (nswPsUtil_isOn(status) == FALSE)) {                      // channel is OFF, switch it ON
    dpGet(chanDp + ".actual.status", status, chanDp + ".settings.v0", v0, chanDp + ".model", model);
//     if (mdtPsUtil_isCaenLvBoard(model)) {                                     // No ramp bits set in status for LV type boards
//       delay(2);                                                               // delay to simulate communication latency
//       dpSetWait(chanDp + ".actual.status", ((status & 0xffffdf7) | 0x1),      // clear trip bits, set ON bit
//                 chanDp + ".actual.vMon", v0, chanDp + ".actual.iMon", 5.0);
//     }
//     else {                                                                    // HV type board. Here we have a ramp state.
      dpGet(chanDp + ".settings.rUp", rampUp);
      dpSetWait(chanDp + ".actual.status", ((status & 0xffffdf7) | 0x3));    // clear trip bits, set ramp flag and ON bit
      delay(0, 200);
      startThread("setChannelOnWithDelay", chanDp, (int) (rampUp > 0 ? v0/rampUp : 0), v0);
    }
    return;    
//   }
}  
   
//////////////////////////////////////////////////////////////////////////////////////////////
// Simulate transition between V0 and V1 driven by mainframe Vsel flag 
//////////////////////////////////////////////////////////////////////////////////////////////

void simulateSY1527Vsel(string dpe1, bool vSel) {
  
  dyn_string sysList = makeDynString(getSystemName());
  dynUnique(sysList);
  
  string mainframe = nswPsUtil_getMainframeName(dpe1);
  dyn_string chanList;
  string model;
  int status;
  float rUp, rDwn, v0, v1, vMon;
  
  for (int i = 1; i <= dynlen(sysList); i++) {
    chanList = dpNames(sysList[i] + "CAEN/" + mainframe + "/*", "FwCaenChannel");
    for (int j = 1; j <= dynlen(chanList); j++) {
      dpGet(chanList[j] + ".actual.status", status, chanList[j] + ".model", model);
      if (!nswPsUtil_isCaenHvBoard(model)) continue;
      if (!nswPsUtil_isOn(status)) continue;
      dpGet(chanList[j] + ".actual.vMon", vMon, chanList[j] + ".settings.v0", v0,
            chanList[j] + ".settings.v1", v1, chanList[j] + ".settings.rUp", rUp,
            chanList[j] + ".settings.rDwn", rDwn);
// decide if there will be a ramp or not ....
      if (vSel && (vMon == v1)) continue;             // nothing to do ...
      else if (!vSel && (vMon == v0)) continue;       // nothing to do ...  
      else if (vSel) {
        dpSet(chanList[j] + ".actual.status", 0x3);   // set ramp up flag and ON bit
        startThread("setChannelOnWithDelay", chanList[j], (int) (rUp > 0 ? (v1 - v0)/rUp : 0), v1); 
      }
      else if (!vSel) {
        dpSet(chanList[j] + ".actual.status", 0x5);   // set ramp down flag and ON bit
        startThread("setChannelOnWithDelay", chanList[j], (int) (rDwn > 0 ? (v1 - v0)/rDwn : 0), v0,
                    TRUE); 
      } 
    }
  }
      
}
    
//////////////////////////////////////////////////////////////////////////////////////////////
// Simulate changes to settings by propagating values to readBackSettings
//////////////////////////////////////////////////////////////////////////////////////////////


void simulateSettings(string dpe1, float v0, string dpe2, float v1, string dpe3, float i0, 
                      string dpe4, float rUp, string dpe5, float rDwn) {
  
  strreplace(dpe1, "settings", "readBackSettings");
  strreplace(dpe2, "settings", "readBackSettings");
  strreplace(dpe3, "settings", "readBackSettings");
  strreplace(dpe4, "settings", "readBackSettings");
  strreplace(dpe5, "settings", "readBackSettings");
  
  delay(1);
  dpSet(dpSubStr(dpe1, DPSUB_DP_EL), v0, dpSubStr(dpe2, DPSUB_DP_EL), v1, 
        dpSubStr(dpe3, DPSUB_DP_EL), i0, dpSubStr(dpe4, DPSUB_DP_EL), rUp, 
        dpSubStr(dpe5, DPSUB_DP_EL), rDwn);
}

//////////////////////////////////////////////////////////////////////////////////////////////
// Simulate changes to Vsel settings of branch controllers by propagating values to .Actual.Vsel
//////////////////////////////////////////////////////////////////////////////////////////////

void simulateVselSettings(string dpe1, string val) {
  
  dyn_string sysList = makeDynString(getSystemName());
  
  dynUnique(sysList);
  
  strreplace(dpe1, "Commands", "Actual");
  delay(1);
  
  if ((val == "0") || (val == "1") || (val == "2")) dpSet(dpSubStr(dpe1, DPSUB_DP_EL), val);
  else dpSet(dpSubStr(dpe1, DPSUB_DP_EL), "0");
  
  for (int i = 1; i <= dynlen(sysList); i++) {
    if (dpExists(sysList[i] + dpSubStr(dpe1, DPSUB_DP)))
      if ((val == "0") || (val == "1") || (val == "2")) 
        dpSet(sysList[i] + dpSubStr(dpe1, DPSUB_DP_EL), val);
      else 
        dpSet(sysList[i] + dpSubStr(dpe1, DPSUB_DP_EL), "0");
  }
}

void simulateCrateReset(string dpe1, bool recover, string dpe2, bool reset) {
  
  dyn_string sysList = makeDynString(getSystemName());
  
  dynUnique(sysList);
  dyn_string chanList;
  int crateId;
  
  strreplace(dpe1, ":_online.._value", "");
  strreplace(dpe2, ":_online.._value", "");
  
  if (recover) {
    delay(5);
    dpSet(dpe1, FALSE);
  }
  if (reset) {
    delay(5);
    crateId = (int) substr(dpe2, strlen(dpe2) - 1, 1);
    chanList = dpNames(dpSubStr(dpe2, DPSUB_DP) + "/easyCrate" + crateId + "/*", "FwCaenChannel");
    for (int i = 1; i <= dynlen(chanList); i++) 
      dpSet(chanList[i] + ".actual.status", 0);
    for (int i = 1; i <= dynlen(sysList); i++) {
      if (dpExists(sysList[i] + dpSubStr(dpe2, DPSUB_DP))) {
        chanList = dpNames(sysList[i] + dpSubStr(dpe2, DPSUB_DP) + "/easyCrate" + crateId + "/*", "FwCaenChannel");
        for (int j = 1; j <= dynlen(chanList); j++) 
          dpSet(chanList[j] + ".actual.status", 0);
      }
    }
    dpSet(dpe2, FALSE);
  }       
}
 
//////////////////////////////////////////////////////////////////////////////////////////////
// Simulate changes to SY1527 mainframe GenOut Config
// Note: Upper 8 bits of the .Commands.GenSignCfg DPE are an 'set enable' mask, while lower
//       8 bits define the value to set for bits enabled in the mask.
//       The ingoing direction, .Information.GenSignCfg contains the lower 8 bits only.
//////////////////////////////////////////////////////////////////////////////////////////////

void simulateSY1527GenSignCfg(string dpe1, int val) {
  
  int mask;
  bool bit;
  int currVal, newVal;
  string text;
  dyn_string sysList = makeDynString(getSystemName());
  
  dynUnique(sysList);
  
  strreplace(dpe1, "Commands", "Information");

  mask = (val & 0xff00) >> 8;
  
  dpGet(dpSubStr(dpe1, DPSUB_DP_EL), currVal);
  
  newVal = 0;
  for (int i = 7; i >= 0; i--) {
    if (getBit(mask, i)) bit = getBit(val, i);
    else bit = getBit(currVal, i);
    newVal = (newVal << 1) | bit;
  }
      
  sprintf(text, "Set 0x%x, Mask 0x%x, Current 0x%x, New 0x%x", val, mask, currVal, newVal);
  DebugTN("simulateSY1527GenSignCfg: " + text);
  
  if (mask == 0) return;
  delay(5);         
  dpSet(dpSubStr(dpe1, DPSUB_DP_EL), newVal);
  for (int i = 1; i <= dynlen(sysList); i++) {
    if (dpExists(sysList[i] + dpSubStr(dpe1, DPSUB_DP)))
      if (sysList[i] != getSystemName())
        dpSet(sysList[i] + dpSubStr(dpe1, DPSUB_DP_EL), newVal);
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////
// Simulate changes to SY1527 mainframe base settings configurable via OPC
//////////////////////////////////////////////////////////////////////////////////////////////

void simulateSY1527BaseSettings(string dpe1, string symbolicName) {
 
  dyn_string sysList = makeDynString(getSystemName());
  
  dynUnique(sysList);
  
  delay(5);         
  dpSet(dpSubStr(dpe1, DPSUB_DP) + ".Information.SymbolicName", symbolicName);
  for (int i = 1; i <= dynlen(sysList); i++) {
    if (dpExists(sysList[i] + dpSubStr(dpe1, DPSUB_DP)))
      if (sysList[i] != getSystemName())
        dpSet(sysList[i] + dpSubStr(dpe1, DPSUB_DP) + ".Information.SymbolicName", symbolicName);
  }
}
