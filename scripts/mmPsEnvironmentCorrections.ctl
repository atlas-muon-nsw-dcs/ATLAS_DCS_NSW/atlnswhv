#uses "nswPsGeneral.ctl"
#uses "mmPs_EC/mmPs_EC.ctl"

dyn_errClass err;
int replycode;
bool showdebugmessages = true;

///////////////////////////////////////////////////////////////////////////////////
// |> T. Klapdor-Kleingrothaus, Uni Freiburg, 2018 <|
// Gets the pressure value and calls correction function
///////////////////////////////////////////////////////////////////////////////////
void main()
{
  nswPsGeneral_displayLogo();
  if(showdebugmessages){DebugN("Hello!\n This is |> mmPsEnvironmentCorrections.ctl <|");}
  // get pressure from pressure meter
//   dyn_string names  = dpNames("*EnvPressure*");
//   DebugN("names: " + names);
//   names = names + ".actualvalue";
//   
  // call function to calculate and set new voltages
//   replycode =  dpConnect("mmPs_EC_Pcorr",names);
  time mytime = getCurrentTime();
  DebugTN("START");
  DebugTN("timed func started at" );
  DebugTN(mytime);
  timedFunc("blink", "dist_1:_thortime");
  
//   nswPsGeneral_ErrorHandling_displayerrormessage();
}

blink(string test1, time t1, time t2)
{
  time  mytime = getCurrentTime();
  DebugTN("blink function" );
  DebugTN(mytime);
  DebugTN("hier sind die Variablen");
  DebugTN(test1 );
  DebugTN(t1 );
  DebugTN(t2  );
  // get pressure from pressure meter
  dyn_string names  = dpNames("*EnvPressure*");
  DebugN("names: " + names);
  names = names + ".actualvalue";
  
  // call function to calculate and set new voltages
  replycode =  dpConnect("mmPs_EC_Pcorr",names);
}
