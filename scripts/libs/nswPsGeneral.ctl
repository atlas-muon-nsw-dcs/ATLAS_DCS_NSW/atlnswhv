///////////////////////////////////////////////////////////////////////////////////
// |> T. Klapdor-Kleingrothaus, Uni Freiburg, 2018 <|
// 
///////////////////////////////////////////////////////////////////////////////////
string nswPsGeneral_getSystemName()
{
//  return "ATLnswSCS:";
} 

///////////////////////////////////////////////////////////////////////////////////
// |> T. Klapdor-Kleingrothaus, Uni Freiburg, 2018 <|
//   Displays stupid logos
///////////////////////////////////////////////////////////////////////////////////
void nswPsGeneral_displayLogo()
{
  DebugN("  \n                                         #`                                              ``          \n                                       .##                                         -####--####.     \n                                       #`############-.                        -###.          -##-  \n                                  .##### .#          `-######.             .###.                 -##\n                             `####-`  #`  #.                 .#####`   `###-                        \n                         .####`      -#   `#                      `#####`                           \n                    .####-           #`    #.                   -###.                               \n.-.......--#########-               -#     `#          ``.-#####.                                   \n`.------##############################      h###########--.`                                        \n                                    ##      #                                                       \n                                    ##      #                                                       \n                                    ##      #                 `                                     \n                                    ##      #                -##`                                   \n                                    ##      #                # ##......                             \n                             ##     ##      #----------------# `......##                            \n                         .##.###    ##      ``````````````````         .#`                          \n              `   ##     #-##-.#    ##                                  #.                          \n##############-###..#`##.# `.  # ## ##                                  .##.                        \n                    `#- ##     # ## ##                                     #                        \n                               #`##.#-                                     #                        \n                               ####-#-                                     #                        \n                               -h####`                                     ########################.\n                                . -##`                                                              \n                                  `##                                                               \n                                   ##                               ");
  DebugN("\n #     #             #######                                             \n #     # #    # #    #       #####  ###### # #####  #    # #####   ####  \n #     # ##   # #    #       #    # #      # #    # #    # #    # #    # \n #     # # #  # #    #####   #    # #####  # #####  #    # #    # #      \n #     # #  # # #    #       #####  #      # #    # #    # #####  #  ### \n #     # #   ## #    #       #   #  #      # #    # #    # #   #  #    # \n  #####  #    # #    #       #    # ###### # #####   ####  #    #  #### \n");
}


///////////////////////////////////////////////////////////////////////////////////
// |> T. Klapdor-Kleingrothaus, Uni Freiburg, 2018 <|
// 
///////////////////////////////////////////////////////////////////////////////////
string nswPsGeneral_IsChamber(string chamber)
{
//         DebugN("chamber: " + chamber);
    string name = nswPsUtil_removeSystemName(chamber);
//     DebugN("name: " + name);
    dyn_int sTGC_identifier = makeDynInt(1,4);
    dyn_int MM_identifier = makeDynInt(2,3);
    int zidicator = nswPsUtil_getZaxisfromChamber(chamber);
//     DebugN("the zidicator identifier is: "+ zidicator);
    if(zidicator == sTGC_identifier[1] || zidicator == sTGC_identifier[2]) 
    {
      return "sTGC";
    }
    if(zidicator == MM_identifier[1] || zidicator == MM_identifier[2]) 
    {
      return "MM";
    }
    else
    {
      DebugN("This is nswPsGeneral_IsChamber \n Chamber cannot be determined");
      return "";
    }
      
}

///////////////////////////////////////////////////////////////////////////////////
// |> T. Klapdor-Kleingrothaus, Uni Freiburg, 2018 <|
// Is called to show the latest error
///////////////////////////////////////////////////////////////////////////////////
nswPsGeneral_ErrorHandling_displayerrormessage()
{
  DebugN("Hello!\n This is |> mmPs_ErrorHandling_displayerrormessage <|");
  dyn_errClass err = getLastError();
  if(dynlen(err) > 0){DebugN("An error has occurred:", err);}
}


string nswPsGeneral_SystemMM()
{
  return "dist_1:";
}

string nswPsGeneral_SystemsTGC()
{
  return "dist_1:";
}
