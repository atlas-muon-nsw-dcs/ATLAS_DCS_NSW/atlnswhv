/* *****************************************************************************
 * Library nswConstants.ctl
 * Manager: UI.Ctrl. 
 * Use: Internal  
 * Notes on DB connect info: 
 *  ++ ATLAS_NSW_DCS schema should be accessed via ATONR_NSW_DCS as DbName, this
 *     directs activity to a dedicated rack node with optimisation.
 *                        
 * by: T. Klapdor-Kleingrothaus
 * Mods: 
 * ***************************************************************************** */

// #uses "NSWPs_datapointHandling.ctl"
//#uses "mmgPs_datapointHandling.ctl"

//////////////////////////////////////////////////////////////////////////
// Constants
//////////////////////////////////////////////////////////////////////////

const bool NSW_CONF_DB_IS_DEV = 0;

const string nswConstants_NSW_CONF_DB_PACKAGE_DEV = "atlas_NSW_config";        // DEVDB10
const string nswConstants_NSW_CONF_DB_PACKAGE_PROD = "atlas_conf_NSW";         // ATONR

//const string NSW_CONF_DB_PACKAGE = "atlas_NSW_config";        // DEVDB10
const string NSW_CONF_DB_PACKAGE = "atlas_conf_NSW";          // ATONR


/////////////////////////////////////////////////////////////////////////////////////////
// Set NSW config DB package name
// This function should be called at the beginning/in the initialize script of each script/
// panel accessing the conf DB, to set package name according to whether DEV or PROD DB is
// used
/////////////////////////////////////////////////////////////////////////////////////////
    
void nswConstants_setConfDBPackageName() {
  
  if (!globalExists("_nswConstants_NSW_CONF_DB_PACKAGE"))
    addGlobal("_nswConstants_NSW_CONF_DB_PACKAGE", STRING_VAR);
              
  if (NSW_CONF_DB_IS_DEV)
    _nswConstants_NSW_CONF_DB_PACKAGE = nswConstants_NSW_CONF_DB_PACKAGE_DEV;
  else 
    _nswConstants_NSW_CONF_DB_PACKAGE = nswConstants_NSW_CONF_DB_PACKAGE_PROD;
    
}


//////////////////////////////////////////////////////////////////////////

string nswConstants_getGasProjectName() {
  return "ATLNSWMON:";
}

int nswConstants_getGasProjectNum() {
  return 100;
}

string nswConstants_getDDCProjectName() {
  return "ATLNSWSCS:";
}

string nswConstants_getSCSProjectName() {
  return "ATLNSWSCS:";
}

string nswConstants_getDssProjectName() {
  return "ATLNSWSCS:";
}

string nswConstants_getUXCoolingProjectName() {
  return "ATLMUOLCS01:";
}


int nswConstants_getSCSProjectNum() {
  return 90;
}

string nswConstants_getBarrelPSProjectName() {
  return "dist_1:";
}

string nswConstants_getEndcapPSProjectName() {
  return "dist_1:";
}

dyn_string nswConstants_getPsProjectNames() 
{
  return makeDynString(nswConstants_getBarrelPSProjectName(), nswConstants_getEndcapPSProjectName());
}

int nswConstants_getBarrelPSProjectNum() {
  return 102;
}

int nswConstants_getEndcapPSProjectNum() {
  return 103;
}

string nswConstants_getPsGeneratorProjectName() {
  return "ATLNSWAUX1:";
}

string nswConstants_getPsRackProjectName() {          // Project NSW power system rack (US15, UX) DPs are located in
  
  return "ATLNSWAUX1:";
}
 
string nswConstants_getCaenResetProjectName() {      // Project where CAEN reset network control is located

  return "ATLMUOLCS01:";
}
 
string nswConstants_getCaenResetFsmProjectName() {      // Project where NSW FSM for CAEN reset network functions is located

  return "ATLNSWAUX1:";                                  // this is a remote FSM ...
}


string nswConstants_getBisProjectName() {     // Common muon beam interlock system project name
  return "ATLMUOLCS01:";
}

string nswConstants_getBisHvCtrlProjectName() {     // Beam interlock system related HV control, NSW V0/V1 switching
  return "ATLNSWAUX1:";
}

string nswConstants_getBisHvCtrlDPName() {     // Beam interlock system related HV control, NSW V0/V1 switching
  return "NSWBisHvCtrl1";
}

string nswConstants_getMtmProjectName() {
  return "ATLNSWMTM:";
}

string nswConstants_getEltxProjectName() {
  return "ATLNSWELTX:";
}

string nswConstants_getBmonProjectName() {
  return "ATLNSWBMON:";
}
      
string nswConstants_getIS4GasProjectName() {
  return "ATLGCSIS2:";
}

string nswConstants_getIS4DssProjectName() {
  return "ATLGCSIS1:";
}

string nswConstants_getEalProjectName() {     // endcap alignment
  return "ATLNSWAUX1:";
}

dyn_string nswConstants_getBalProjectNames() {     // barrel alignment
  return makeDynString("ATLMDBAL1:", "ATLMDBAL2:", "ATLMDBAL3:", "ATLMDBAL4:",
                       "ATLMDBAL5:", "ATLMDBAL6:", "ATLMDBAL7:", "ATLMDBAL8:");
}

string nswConstants_getBalSupProjectName() {
  return "ATLNSWBAL9:";
}


dyn_string nswConstants_getMdmProjectNames() {
  return makeDynString("ATLNSWMDM1:", "ATLNSWMDM2:", "ATLNSWMDM3:", "ATLNSWMDM4:", "ATLNSWMDM5:",
                       "ATLNSWMDM6:");
}

string nswConstants_getMdmSupProjectName() {
  return "ATLNSWMDM7:";
}

string nswConstants_getMdmPsuProjectName() {
  return "ATLNSWMDM7:";
}

string nswConstants_getJtagExtInfoProjectName() {
  return "ATLNSWSCS:";
}

dyn_string nswConstants_getNSWProjectNames() {
  return makeDynString("ATLNSWMDM1:", "ATLNSWMDM2:", "ATLNSWMDM3:", "ATLNSWMDM4:", "ATLNSWMDM5:",
                       "ATLNSWMDM6:", "ATLNSWMDM7:", "ATLNSWSCS:", "ATLNSWMON:", "ATLNSWAUX1:",
                       "ATLNSWPS2:", "ATLNSWPS3:", "ATLNSWBAL9:", "ATLNSWELTX:", "ATLNSWGMC1:",
                       "ATLNSWGMC2:");
}

dyn_string nswConstants_getNSWDcsComputerNames() {
  return makeDynString("pcatlNSWmdm1", "pcatlNSWmdm2", "pcatlNSWmdm3", "pcatlNSWmdm4", "pcatlNSWmdm5",
                       "pcatlNSWmdm6", "pcatlNSWmdm7", "pcatlNSWaux1", "pcatlNSWps2", "pcatlNSWps3",
                       "pcatlNSWbal9", "pcatlNSWscs", "pcatlNSWmon", "pcatlNSWgmc1", "pcatlNSWgmc2",
                       "pcatlNSWeltx");
}

dyn_string nswConstants_getCicRackProjectNames() {
  return makeDynString("ATLCICUS15:", "ATLCICUSA15L1:", "ATLCICUX:");
}
  
string nswConstants_getCicRackUX15ProjectName() {
  return "ATLCICUX:";
}

// deprecated, replaced by getWienerCrateProjectName, will be removed

string nswConstants_getRodMonProjectName() {     // MROD crate monitoring
  
  return "ATLNSWMDM7:";
}

string nswConstants_getWienerCrateProjectName() {     // Wiener VME crate monitoring (TTC, MROD, ECAlign)
  
  return "ATLNSWMDM7:";
}

string nswConstants_getLHCProjectName() {
  
  return "ATLGCSLHC:";
}



///// Gas Monitoring

// return the predefined manager number for PVSSCtrl for certain scripts
// used for project monitoring. Must be consistent with progs file.
 
int nswConstants_getGasCtrlManNum(string scriptName) {
  
  if (scriptName == "fwAtlasGas/fwAtlasGas_copyGasData.ctl")
    return 90;
  else if (scriptName == "NSWGas_HvInterlock.ctl")
    return 92;
  else if (scriptName == "NSWGasMichGMC_importData.ctl") 
    return 94;
  else return 0;
}

////// PS projects

int nswConstants_getPsCtrlManNum(string scriptName) {
  
  //DebugN(scriptName);
  if (scriptName == "NSWPs_CaenWatchdog.ctl")
    return 90;
  else if (scriptName == "NSWPs_TripRecovery.ctl")
    return 92;
// PS supervisor
  else if (scriptName == "NSWPs_copyCAENGeneratorElmbData.ctl")
    return 92;
  else if (scriptName == "NSWPs_SupervisorWatchdog.ctl")
    return 95;  
  else if (scriptName == "NSWPs_calcBeamAsymm.ctl")
    return 100;
  
  else 
    return 0;
}

string nswConstants_getPsCaenWatchdogChan(string sysName) {
  
  if (sysName == nswConstants_getBarrelPSProjectName())
    return NSWPs_getWatchdogChannelOfPartition("B");
  else if (sysName == nswConstants_getEndcapPSProjectName())
    return NSWPs_getWatchdogChannelOfPartition("E");
  else
    return "";
}


////// SCS 

int nswConstants_getScsCtrlManNum(string scriptName) {
  
  if (scriptName == "NSWDdcWatchdog.ctl")
    return 93;
  
  if (scriptName == "NSWDdcRecoveryCtrl.ctl") 
    return 94;
  
if (scriptName == "NSWDdcAutoRecovery.ctl") 
    return 95;
  
}

////// MDM projects

// return the predefined manager number for PVSSCtrl for certain scripts
// used for project monitoring. Must be consistent with progs file.

int nswConstants_getMdmCtrlManNum(string scriptName) {
  
  if (scriptName == "MDM_Watchdog.ctl")
    return 81;
  else if (scriptName == "MDM_Backup.ctl")
    return 80;
  else if (scriptName == "MDM_Jtag.ctl")
    return 90;
  else return 0;
}

int nswConstants_getMdmCanOPCClientManNum() { 
  return 7;                               
}

int nswConstants_getWienerOPCClientManNum() { 
  return 14;                               
}

int nswConstants_getJtagCtrlManNum(string scriptName) {
  
  if (scriptName == "NSWJtag_updateExternalParams.ctl")
    return 105;
  else return 0;
}

int nswConstants_getAgilentCtrlManNum(string scriptName) {
  
  if (scriptName == "NSWAgilentCTRL.ctl")
    return 90;
  else if (scriptName == "NSWAgilentReadoutLoop.ctl")
    return 91;
  else
    return 0;
}

int nswConstants_getGEOpticaCtrlManNum(string scriptName) {
  
  if (scriptName == "NSWGEOpticaCTRL.ctl")
    return 100;
  else
    return 0;
}

int nswConstants_getGasH2OCtrlManNum(string scriptName) {
  
  if (scriptName == "NSWGasH2OCTRL.ctl")
    return 101;
  else
    return 0;
}


////// Database connection information for various NSW projects


string nswConstants_getNSWConfDBConnectString_W() {
   
  const string DBConnectDP = "NSWDbConnections/ATONR/ATLAS_CONF_NSW_W";
  string connectString = "";
  
  if (dpExists(nswConstants_getSCSProjectName() + DBConnectDP)) {
    dpGet(nswConstants_getSCSProjectName() + DBConnectDP + ".connectString", connectString);
    if (connectString != "") return connectString;
  }
  else if (dpExists(DBConnectDP))
    dpGet(DBConnectDP + ".connectString", connectString);
  
  return connectString;
}

string nswConstants_getNSWConfDBConnectString_R() {
  
  const string DBConnectDP = "NSWDbConnections/ATONR/ATLAS_CONF_NSW_R";
  string connectString = "";
  
  if (dpExists(nswConstants_getSCSProjectName() + DBConnectDP)) {
    dpGet(nswConstants_getSCSProjectName() + DBConnectDP + ".connectString", connectString);
    if (connectString != "") return connectString;
  }
  else if (dpExists(DBConnectDP))
    dpGet(DBConnectDP + ".connectString", connectString);
  
  return connectString;
}

// For JCOP Conf DB

string nswConstants_getNSWJCOPConfDBConnectString_W() {
  
  const string DBConnectDP = "NSWDbConnections/ATONR/JCOP_PVSSCONF";
  string connectString = "";
  
  if (dpExists(nswConstants_getSCSProjectName() + DBConnectDP)) {
    dpGet(nswConstants_getSCSProjectName() + DBConnectDP + ".connectString", connectString);
    if (connectString != "") return connectString;
  }
  else if( dpExists(DBConnectDP))
    dpGet(DBConnectDP + ".connectString", connectString);
  
  return connectString;
}

// CERN Development server

string nswConstants_getNSWConfDBConnectString_Dev_R() {
  
  const string DBConnectDP = "NSWDbConnections/DEV/ATLAS_CONF_NSW_R";
  string connectString = "";
  
  if (dpExists(nswConstants_getSCSProjectName() + DBConnectDP)) {
    dpGet(nswConstants_getSCSProjectName() + DBConnectDP + ".connectString", connectString);
    if (connectString != "") return connectString;
  }
  else if (dpExists(DBConnectDP))
    dpGet(DBConnectDP + ".connectString", connectString);
  
  return connectString;
}  

string nswConstants_getNSWConfDBConnectString_Dev_W() {
  
  const string DBConnectDP = "NSWDbConnections/DEV/ATLAS_CONF_NSW_W";
  string connectString = "";
  
  if (dpExists(nswConstants_getSCSProjectName() + DBConnectDP)) {
    dpGet(nswConstants_getSCSProjectName() + DBConnectDP + ".connectString", connectString);
    if (connectString != "") return connectString;
  }
  else if (dpExists(DBConnectDP))
    dpGet(DBConnectDP + ".connectString", connectString);
  
  return connectString;
}

string nswConstants_getNSWDcsDBConnectString_R() {
  
  const string DBConnectDP = "NSWDbConnections/ATONR/ATLAS_NSW_DCS_R";
  string connectString = "";
  
  if (dpExists(nswConstants_getSCSProjectName() + DBConnectDP)) {
    dpGet(nswConstants_getSCSProjectName() + DBConnectDP + ".connectString", connectString);
    if (connectString != "") return connectString;
  }
  else if (dpExists(DBConnectDP))
    dpGet(DBConnectDP + ".connectString", connectString);
  
  return connectString;
}

string nswConstants_getNSWDcsDBConnectString_W() {
  
  const string DBConnectDP = "NSWDbConnections/ATONR/ATLAS_NSW_DCS_W";
  string connectString = "";
  
  if (dpExists(nswConstants_getSCSProjectName() + DBConnectDP)) {
    dpGet(nswConstants_getSCSProjectName() + DBConnectDP + ".connectString", connectString);
    if (connectString != "") return connectString;
  }
  else if (dpExists(DBConnectDP))
    dpGet(DBConnectDP + ".connectString", connectString);
  
  return connectString;
}

// Michigan Gas Monitoring Chamber Data

string nswConstants_getMichGasMonDBConnectString() {
  
  const string DBConnectDP = "NSWDbConnections/ATLR/ATLAS_MUONCERT_R";
  string connectString = "";
  
  if (dpExists(nswConstants_getSCSProjectName() + DBConnectDP)) {
    dpGet(nswConstants_getSCSProjectName() + DBConnectDP + ".connectString", connectString);
    if (connectString != "") return connectString;
  }
  else if (dpExists(DBConnectDP))
    dpGet(DBConnectDP + ".connectString", connectString);
  
  return connectString;
  
}



//=========================================================================
//MTM project
//=========================================================================
//please do not change
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

string nswConstants_getMtmAtonrReadAutentification()
{
  return nswConstants_getNSWDcsDBConnectString_R();
}

string nswConstants_getMtmAtonrWriteAutentification()
{
  return nswConstants_getNSWDcsDBConnectString_W();
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//end of MTM project code
//========================================================================


//==========================================================================
//NSW ELTX project
//==========================================================================
//functions for the connection to databases...

string nswConstants_ATONR_WriteAutentification()
{
  return nswConstants_getNSWDcsDBConnectString_W();
}
string nswConstants_ATONR_ReadAutentification()
{
  return nswConstants_getNSWConfDBConnectString_R();
}


string nswConstants_ORADEV10_WriteAutentification()
{
  return nswConstants_getNSWConfDBConnectString_Dev_W();
}
//============================================================================
