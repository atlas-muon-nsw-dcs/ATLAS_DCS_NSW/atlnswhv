#uses "nswPsUtil.ctl"
#uses "nswPsGeneral.ctl"
// T. Klapdor-Kleingrothaus, Uni Freiburg, 2018
/////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
// Generate Chambernames and add symbols for MM or STGC for function muUiGeo_nsw_instantiateWheel_PsView   |> T. Klapdor-Kleingrothaus <|
/////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
dyn_string muUiGeo_nsw_instantiateWheel_PsView(string wheel_type, char side, string  polygon_file, string zAxisSuffix,
                                        dyn_string dollarParams) {

  string this_module = myModuleName();
  string this_panel = myPanelName();
  dyn_string dollar_params;            // parameters to pass to reference object/panel when calling addSymbol
  string chamberName;
  dyn_string shapeList;
  string chambername_prefix = "EI" + zAxisSuffix + "R";
  
  if (!dynContains(makeDynString("MM","STGC"), wheel_type)) {
    DebugN("muUiGeo_nsw_instantiateWheel: Function does not know wheel_type = " + wheel_type);
    return "";
  }
// MMG
  if(wheel_type == "MM")
  {
    for (int i = 1; i <= 16; i += 1) { 
      for (int j = 1; j <= 2; j++) {
        string sector;
        if(i<=9){sector = "0" + i ; }
        else{sector = i;}
        chamberName = chambername_prefix + j  + side + sector;
//                DebugN("instiantiate " + chamberName);
        dollar_params = makeDynString("$chamber:" + chamberName, "$domain:MM_SIDE_" + side, "$zaxis:" + substr(zAxisSuffix,2,2),"$radial:"+j,"$side:"+side,"$sector:"+sector, "$statusname:STATUS_"+chamberName);
        dyn_string tmpArray = dollarParams;        // we need this since dynAppend(x,y) with y of type dyn_array empties the array y!
        dynAppend(dollar_params, tmpArray); 
        addSymbol(this_module, this_panel,  polygon_file, chamberName, dollar_params, 100,100,0,1,1);
        dynAppend(shapeList, chamberName);
      }
    }
  }

  
// STGC  
  if(wheel_type == "STGC")
  {
    for (int i = 1; i <= 16; i += 1) { 
      for (int j = 1; j <= 4; j++) {
        string sector;
        if(i<=9){sector = "0" + i ; }
        else{sector = i;}
        chamberName = chambername_prefix + j  + side + sector;
//                DebugN("instiantiate " + chamberName);
        dollar_params = makeDynString("$chamber:" + chamberName,"$domain:STGC_SIDE_" + side, "$zaxis:" + substr(zAxisSuffix,2,2),"$radial:"+j,"$side:"+side,"$sector:"+sector, "$statusname:STATUS_"+chamberName);
        dyn_string tmpArray = dollarParams;        // we need this since dynAppend(x,y) with y of type dyn_array empties the array y!
        dynAppend(dollar_params, tmpArray); 
        addSymbol(this_module, this_panel,  polygon_file, chamberName, dollar_params, 100,100,0,1,1);
        dynAppend(shapeList, chamberName);
      }
    }
  }
  
  
  
  return shapeList;
 
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
// Generate Chambernames and add symbols for MM or STGC for function muUiGeo_nsw_instantiateLayerWheel_PsSideView   |> T. Klapdor-Kleingrothaus <|
/////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
dyn_string muUiGeo_nsw_instantiateLayerWheel_PsSideView(string wheel_type, char side, string  zAxisIdentifier,  string polygon_file, string zAxisSuffix,
                                        dyn_string dollarParams) {

//   DebugN("This is muUiGeo_nsw_instantiateLayerWheel_PsSideView");
  string this_module = myModuleName();
  string this_panel = myPanelName();
  string wheelswitch, statusname;
  dyn_string dollar_params;            // parameters to pass to reference object/panel when calling addSymbol
  string chamberName;
  dyn_string shapeList;
  string wheelz;
  int layernumber;
      
      
  if(patternMatch( zAxisIdentifier,"HO") && patternMatch(wheel_type,"MM")){wheelz = "Z3";}
  if(patternMatch( zAxisIdentifier,"IP") && patternMatch(wheel_type,"MM")){wheelz = "Z2";}
  if(patternMatch( zAxisIdentifier,"HO") && patternMatch(wheel_type,"STGC")){wheelz = "Z4";}
  if(patternMatch( zAxisIdentifier,"IP") && patternMatch(wheel_type,"STGC")){wheelz = "Z1";}
  string chambername_prefix = "EI" + wheelz + "R";
  
  if (!dynContains(makeDynString("MM","STGC"), wheel_type)) {
    DebugN("muUiGeo_nsw_instantiateLayerWheel: Function does not know wheel_type = " + wheel_type);
    return "";
  }
// MMG
  if(patternMatch(wheel_type, "MM"))
  {
//     DebugN(" muUiGeo_nsw_instantiateLayerWheel_PsSideView switches to " + wheel_type);
    if(patternMatch( zAxisIdentifier,"HO")){wheelswitch = "3";};
    if(patternMatch( zAxisIdentifier,"IP")){wheelswitch = "2";};
    for (int i = 1; i <= 16; i += 1) { 
      string sector;
      if(i<=9){sector = "0" + i ; }
      else{sector = i;}
      for (int j = 1; j <= 2; j++) {
        chamberName = chambername_prefix + j  + side + sector;
        dyn_string channels = nswPsUtil_getChannelsOfChamber(chamberName);
        string layer;
        if (patternMatch(zAxisSuffix,"-D")) layernumber = 5;
        else 
        {
          layer= strltrim(zAxisSuffix, "-L");
          layernumber = (int)layer;
        }
//         DebugN("layer: " + layer);
        if(patternMatch(layer,"Drift")){layernumber = 5;};
        statusname =strltrim(channels[layernumber],nswPsGeneral_SystemMM());
//         DebugN("statusname: " +  statusname);        

        dollar_params = makeDynString("$chamber:" + chamberName + zAxisSuffix,"$statusname:STATUS_"+statusname);
        dyn_string tmpArray = dollarParams;        // we need this since dynAppend(x,y) with y of type dyn_array empties the array y!
        dynAppend(dollar_params, tmpArray); 
        addSymbol(this_module, this_panel, polygon_file, chamberName + zAxisSuffix, dollar_params, 100,100,0,1,1);
        dynAppend(shapeList, chamberName + zAxisSuffix);
      }
    }
  }

  
// STGC  
  if(patternMatch(wheel_type,"STGC"))
  {
//     DebugN(" muUiGeo_nsw_instantiateLayerWheel_PsSideView switches to " + wheel_type);
    if(patternMatch( zAxisIdentifier,"HO")){wheelswitch = "4";};
    if(patternMatch( zAxisIdentifier,"IP")){wheelswitch = "1";};
    for (int i = 1; i <= 16; i += 1) { 
      string sector;
      if(i<=9){sector = "0" + i ; }
      else{sector = i;}
      for (int j = 1; j <= 4; j++) {
        chamberName = chambername_prefix + j + side + sector;
        dollar_params = makeDynString("$chamber:" + chamberName+ zAxisSuffix);
        dyn_string tmpArray = dollarParams;        // we need this since dynAppend(x,y) with y of type dyn_array empties the array y!
        dynAppend(dollar_params, tmpArray); 
        addSymbol(this_module, this_panel, polygon_file, chamberName + zAxisSuffix, dollar_params, 100,100,0,1,1);
        dynAppend(shapeList, chamberName + zAxisSuffix);
      }
    }
  }
  
  
  
  return shapeList;
 
}


dyn_string muUiGeo_nsw_instantiateSector(string wheel_type, char side, string wheel, string sector,  string refFileName, string refNameSuffix,
                                        dyn_string dollarParams) {

  string this_module = myModuleName();
  string this_panel = myPanelName();
  string wheelswitch;
  dyn_string dollar_params;            // parameters to pass to reference object/panel when calling addSymbol
  string chamberName;
  dyn_string shapeList;
  string chambername_prefix = "EI" + refNameSuffix + "R";
  
  if (!dynContains(makeDynString("MM","STGC"), wheel_type)) {
    DebugN("muUiGeo_nsw_instantiateSector: Function does not know wheel_type = " + wheel_type);
    return "";
  }
// MMG
  if(patternMatch(wheel_type, "MM"))
  {
    if(patternMatch(wheel,"HO")){wheelswitch = "3";};
    if(patternMatch(wheel,"IP")){wheelswitch = "2";};
    int i = 5;
      for (int j = 1; j <= 2; j++) {
        chamberName = chambername_prefix + j  + side + sector;
        dollar_params = makeDynString("$chamber:" + chamberName);
        dyn_string tmpArray = dollarParams;        // we need this since dynAppend(x,y) with y of type dyn_array empties the array y!
        dynAppend(dollar_params, tmpArray); 
        addSymbol(this_module, this_panel, refFileName, chamberName + refNameSuffix, dollar_params, 100,100,0,1,1);
        dynAppend(shapeList, chamberName + refNameSuffix);
      }
  }

  
// STGC  
  if(patternMatch(wheel_type,"STGC"))
  {
    if(patternMatch(wheel,"HO")){wheelswitch = "4";};
    if(patternMatch(wheel,"IP")){wheelswitch = "1";};
    int i = 5;
      for (int j = 1; j <= 3; j++) {
        if(i%2 ==0 ){chamberName = "EIS-A"+ wheelswitch + "-R" + j + "-" + side + sector;}
        else{chamberName = "EIL-A"+ wheelswitch + "-R" + j + "-" + side + sector;}
//                DebugN("instiantiate " + chamberName);
        dollar_params = makeDynString("$chamber:" + chamberName);
        dyn_string tmpArray = dollarParams;        // we need this since dynAppend(x,y) with y of type dyn_array empties the array y!
        dynAppend(dollar_params, tmpArray); 
        addSymbol(this_module, this_panel, refFileName, chamberName + refNameSuffix, dollar_params, 100,100,0,1,1);
        dynAppend(shapeList, chamberName + refNameSuffix);
      }
  }
  
  
  
  return shapeList;
 
}

 
void muUiGeo_nsw_drawCircles_PsView(string wheel_type, char side, string zAxisSuffix, string shapeIdentifier, int center_x, int center_y, 
                           float scale, dyn_dyn_float rad_pos, dyn_float phi_angle, dyn_float open_angle) {
  string  chamberName;
  dyn_dyn_int points;
  string wheelswitch;
  int numberofR, size_x, size_y;
  if(patternMatch(wheel_type,"STGC")){numberofR = 4;size_x = 10; size_y = 10;}
  if(patternMatch(wheel_type,"MM")){numberofR = 2;size_x = 15; size_y = 15;}
  string chambername_prefix = "EI" + zAxisSuffix + "R";
  // for circle
        scale = 0.028;
            for (int i = 1; i <= 16; i += 1) { 
      string sector;
      if(i<=9){sector = "0" + i ; }
      else{sector = i;}
      for (int j = 1; j <= numberofR; j++) {
        chamberName = chambername_prefix + j + side + sector;
        int x = center_x + scale *((( rad_pos[i][j+1] - rad_pos[i][j])/2)+rad_pos[i][j]) * cos(deg2rad(phi_angle[i]));
        int y = center_y + scale * ((( rad_pos[i][j+1] - rad_pos[i][j])/2)+rad_pos[i][j]) * sin(-deg2rad(phi_angle[i]));
//         DebugN(chamberName + " x: " + x + " y: " + y);
            setValue(chamberName + "." + "NSWCircle", "position", x,y);
            setValue(chamberName + "." + "NSWCircle", "size", size_x,size_y);

           }}
    }

void muUiGeo_nsw_drawCircles_PsSideView(string wheel_type, char side, string zAxisIdentifier, string zAxisSuffix, string shapeIdentifier, int center_x, int center_y, 
                           float scale, dyn_dyn_float rad_pos, dyn_float phi_angle, dyn_float open_angle)
{
//   DebugN("This is PsSideView draw circles function");
//       DebugN("openangle: " + open_angle);
  string obj_name, chamberName;
  dyn_dyn_int points;
    int numberofR, size_x, size_y;
  if(patternMatch(wheel_type,"STGC")){numberofR = 4;size_x = 2; size_y = 2;}
  if(patternMatch(wheel_type,"MM")){numberofR = 2;size_x = 9; size_y = 9;}
  string wheelswitch;
  string chambername_prefix = "EI" + zAxisSuffix + "R";
  string wheelz;
  if(patternMatch(zAxisIdentifier,"HO") && patternMatch(wheel_type,"MM")){wheelz = "Z3";}
  if(patternMatch(zAxisIdentifier,"IP") && patternMatch(wheel_type,"MM")){wheelz = "Z2";}
  if(patternMatch(zAxisIdentifier,"HO") && patternMatch(wheel_type,"STGC")){wheelz = "Z4";}
  if(patternMatch(zAxisIdentifier,"IP") && patternMatch(wheel_type,"STGC")){wheelz = "Z1";}
  string chambername_prefix = "EI" + wheelz + "R";
  if (!dynContains(makeDynString("MM", "STGC"), wheel_type)) {
    DebugN("muUiGeo_mdt_drawWheel: Function does not know wheel_type = " + wheel_type);
  }
// MMG
  if(patternMatch(wheel_type,"MM")){
   if(patternMatch(zAxisIdentifier,"HO")){wheelswitch = "3";};
   if(patternMatch(zAxisIdentifier,"IP")){wheelswitch = "2";};
//     DebugN( "chamber " + wheelz + " wheelswitch " + wheelswitch);
    for (int i = 1; i <= 16; i += 1) { 
      string sector;
      if(i<=9){sector = "0" + i ; }
      else{sector = i;}
      for (int j = 1; j <= 2; j++) {
       chamberName = chambername_prefix + j +  side + sector + zAxisSuffix;  
//       DebugN(" > This is muUiGeo_nsw_drawCircles_PsSideView <   ---> Chambername is: " + chamberName);
      obj_name = chamberName;// + zAxisSuffix;
//       DebugN("obj_name: " + obj_name);
//       DebugN("shapeIdentigier:" + shapeIdentifier);
      
      if (shapeExists(obj_name)) {
                int x = center_x + scale *((( rad_pos[i][j+1] - rad_pos[i][j])/2)+rad_pos[i][j]) * cos(deg2rad(phi_angle[i]));
        int y = center_y + scale * ((( rad_pos[i][j+1] - rad_pos[i][j])/2)+rad_pos[i][j]) * sin(-deg2rad(phi_angle[i]));
//         DebugN(chamberName + " x: " + x + " y: " + y);
        setValue(obj_name + "." + "NSWCircle", "position", x,y);
        setValue(obj_name + "." + "NSWCircle", "size", size_x,size_y);
      }}
    } 
 }
  
  /*
  // wheel_type == stgc  
  if(patternMatch(wheel_type,"STGC")){
   if(patternMatch(zAxisIdentifier,"HO")){wheelswitch = "4";};
   if(patternMatch(zAxisIdentifier,"IP")){wheelswitch = "1";};
    for (int i = 1; i <= 16; i += 1) { 
      string sector;
      if(i<=9){sector = "0" + i ; }
      else{sector = i;}
      for (int j = 1; j <= 4; j++) {
       chamberName = chambername_prefix + j +  side + sector + zAxisSuffix;  

      obj_name = chamberName ;//+ zAxisSuffix;
      DebugN("obj_name: " + obj_name);
//       DebugN("shapeIdentigier:" + shapeIdentifier);
      
      if (shapeExists(obj_name)) {
        points[1][1] = center_x + scale * rad_pos[i][j] * cos(deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[1][2] = center_y + scale * rad_pos[i][j] * sin(-deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[2][1] = center_x + scale * rad_pos[i][j+1] * cos(deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[2][2] = center_y + scale * rad_pos[i][j+1] * sin(-deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[3][1] = center_x + scale * rad_pos[i][j+1] * cos(deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[3][2] = center_y + scale * rad_pos[i][j+1] * sin(-deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[4][1] = center_x + scale * rad_pos[i][j] * cos(deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[4][2] = center_y + scale * rad_pos[i][j] * sin(-deg2rad(phi_angle[i] - open_angle[i]/2.));
//         DebugN(" p1: " + points[1][1] +" p1: " + points[1][2] +" p2: " + points[2][1]+ " p2: " + points[2][2]);
//         DebugN(" p3: " + points[3][1] +" p3: " + points[3][2] +" p4: " + points[4][1]+ " p4: " + points[4][2]);
        setValue(obj_name + "." + shapeIdentifier, "points", points);
        setValue(obj_name + "." + shapeIdentifier, "visible", TRUE);
      }
      else
      {
        DebugN("Shape " + obj_name + " does not exist!");
      }
    }
    } 
    }
  
  string  chamberName;
  dyn_dyn_int points;
  string wheelswitch;
  int numberofR, size_x, size_y;
  if(patternMatch(wheel_type,"STGC")){numberofR = 4;size_x = 10; size_y = 10;}
  if(patternMatch(wheel_type,"MM")){numberofR = 2;size_x = 15; size_y = 15;}
  string chambername_prefix = "EI" + zAxisSuffix + "R";
  // for circle
        scale = 0.028;
            for (int i = 1; i <= 16; i += 1) { 
      string sector;
      if(i<=9){sector = "0" + i ; }
      else{sector = i;}
      for (int j = 1; j <= numberofR; j++) {
        chamberName = chambername_prefix + j + side + sector;
        int x = center_x + scale *((( rad_pos[i][j+1] - rad_pos[i][j])/2)+rad_pos[i][j]) * cos(deg2rad(phi_angle[i]));
        int y = center_y + scale * ((( rad_pos[i][j+1] - rad_pos[i][j])/2)+rad_pos[i][j]) * sin(-deg2rad(phi_angle[i]));
//         DebugN(chamberName + " x: " + x + " y: " + y);
            setValue(chamberName + "." + "NSWCircle", "position", x,y);
            setValue(chamberName + "." + "NSWCircle", "size", size_x,size_y);

           }}*/
}
//////////////////////////////////////////////////////////////////////////////////////////
// Function to draw wheel type Atlas geometry (MM, STGC) for function muUiGeo_nsw_drawWheel_PsView
// Chamber objects must have been created with addSymbol or otherwise before with reference
// names = chamber names, i.e. BOL1A05.
// Parameters:
// wheel_type: "MM", "STGC"
// side: "A" or "C", used to construct chamber names
// zAxisSuffix: used in chamber names to obtain the Z position of the wheel 
// shapeIdentifier: name of the shape representing the chamber (type polygon) 
// rad_pos: 2dim array with radial positions of chamber corner points, numbering: [sector][chamber]
////////////////////////////////////////////////////////////////////////////////////////// |> T. Klapdor-Kleingrothaus <|
 
void muUiGeo_nsw_drawWheel_PsView(string wheel_type, char side, string zAxisSuffix, string shapeIdentifier, int center_x, int center_y, 
                           float scale, dyn_dyn_float rad_pos, dyn_float phi_angle, dyn_float open_angle) {
  
//   DebugN("This is draw wheel function");
//       DebugN("openangle: " + open_angle);
  string obj_name, chamberName;
  dyn_dyn_int points;
  string wheelswitch;
  string chambername_prefix = "EI" + zAxisSuffix + "R";

  if (!dynContains(makeDynString("MM", "STGC"), wheel_type)) {
    DebugN("muUiGeo_mdt_drawWheel: Function does not know wheel_type = " + wheel_type);
  }
// MMG
  if(patternMatch(wheel_type,"MM")){
    for (int i = 1; i <= 16; i += 1) { 
      string sector;
      if(i<=9){sector = "0" + i ; }
      else{sector = i;}
      for (int j = 1; j <= 2; j++) {
        chamberName = chambername_prefix + j + side + sector;
//         dollar_params = makeDynString("$chamber:" + chamberName, "$dom:MM_SIDE_" + side);

      obj_name = chamberName;
//       DebugN("obj_name: " + obj_name);
//       DebugN("shapeIdentigier:" + shapeIdentifier);
      // for chamber
      if (shapeExists(obj_name)) {
        points[1][1] = center_x + scale * rad_pos[i][j] * cos(deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[1][2] = center_y + scale * rad_pos[i][j] * sin(-deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[2][1] = center_x + scale * rad_pos[i][j+1] * cos(deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[2][2] = center_y + scale * rad_pos[i][j+1] * sin(-deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[3][1] = center_x + scale * rad_pos[i][j+1] * cos(deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[3][2] = center_y + scale * rad_pos[i][j+1] * sin(-deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[4][1] = center_x + scale * rad_pos[i][j] * cos(deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[4][2] = center_y + scale * rad_pos[i][j] * sin(-deg2rad(phi_angle[i] - open_angle[i]/2.));
//         DebugN(" p1: " + points[1][1] +" p1: " + points[1][2] +" p2: " + points[2][1]+ " p2: " + points[2][2]);
//         DebugN(" p3: " + points[3][1] +" p3: " + points[3][2] +" p4: " + points[4][1]+ " p4: " + points[4][2]);
        setValue(obj_name + "." + shapeIdentifier, "points", points);
        setValue(obj_name + "." + shapeIdentifier, "visible", TRUE);
    }
    } 
 }
  }


// wheel_type == stgc  
  if(patternMatch(wheel_type,"STGC")){
    for (int i = 1; i <= 16; i += 1) { 
      string sector;
      if(i<=9){sector = "0" + i ; }
      else{sector = i;}
      for (int j = 1; j <= 4; j++) {
        chamberName = chambername_prefix + j +  side + sector;  


      obj_name = chamberName;
//       DebugN("obj_name: " + obj_name);
//       DebugN("shapeIdentigier:" + shapeIdentifier);
      
      if (shapeExists(obj_name)) {
        points[1][1] = center_x + scale * rad_pos[i][j] * cos(deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[1][2] = center_y + scale * rad_pos[i][j] * sin(-deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[2][1] = center_x + scale * rad_pos[i][j+1] * cos(deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[2][2] = center_y + scale * rad_pos[i][j+1] * sin(-deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[3][1] = center_x + scale * rad_pos[i][j+1] * cos(deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[3][2] = center_y + scale * rad_pos[i][j+1] * sin(-deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[4][1] = center_x + scale * rad_pos[i][j] * cos(deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[4][2] = center_y + scale * rad_pos[i][j] * sin(-deg2rad(phi_angle[i] - open_angle[i]/2.));
//         DebugN(" p1: " + points[1][1] +" p1: " + points[1][2] +" p2: " + points[2][1]+ " p2: " + points[2][2]);
//         DebugN(" p3: " + points[3][1] +" p3: " + points[3][2] +" p4: " + points[4][1]+ " p4: " + points[4][2]);
        setValue(obj_name + "." + shapeIdentifier, "points", points);
        setValue(obj_name + "." + shapeIdentifier, "visible", TRUE);
      }
    } 
 }
  }
}

//////////////////////////////////////////////////////////////////////////////////////////
// Function to draw wheel type Atlas geometry (MM, STGC) for function muUiGeo_nsw_drawWheel_PsSideView
// Chamber objects must have been created with addSymbol or otherwise before with reference
// names = chamber names, i.e. BOL1A05.
// Parameters:
// wheel_type: "MM", "STGC"
// side: "A" or "C", used to construct chamber names
// zAxisSuffix: used in chamber names to obtain the Z position of the wheel 
// shapeIdentifier: name of the shape representing the chamber (type polygon) 
// rad_pos: 2dim array with radial positions of chamber corner points, numbering: [sector][chamber]
////////////////////////////////////////////////////////////////////////////////////////// |> T. Klapdor-Kleingrothaus <|

void muUiGeo_nsw_drawWheel_PsSideView(string wheel_type, char side, string zAxisIdentifier, string zAxisSuffix, string shapeIdentifier, int center_x, int center_y, 
                           float scale, dyn_dyn_float rad_pos, dyn_float phi_angle, dyn_float open_angle) {
  
//   DebugN("This is draw wheel function");
//       DebugN("openangle: " + open_angle);
  string obj_name, chamberName;
  dyn_dyn_int points;
  string wheelswitch;
  string chambername_prefix = "EI" + zAxisSuffix + "R";
  string wheelz;
  if(patternMatch(zAxisIdentifier,"HO") && patternMatch(wheel_type,"MM")){wheelz = "Z3";}
  if(patternMatch(zAxisIdentifier,"IP") && patternMatch(wheel_type,"MM")){wheelz = "Z2";}
  if(patternMatch(zAxisIdentifier,"HO") && patternMatch(wheel_type,"STGC")){wheelz = "Z4";}
  if(patternMatch(zAxisIdentifier,"IP") && patternMatch(wheel_type,"STGC")){wheelz = "Z1";}
  string chambername_prefix = "EI" + wheelz + "R";
  if (!dynContains(makeDynString("MM", "STGC"), wheel_type)) {
    DebugN("muUiGeo_mdt_drawWheel: Function does not know wheel_type = " + wheel_type);
  }
// MMG
  if(patternMatch(wheel_type,"MM")){
   if(patternMatch(zAxisIdentifier,"HO")){wheelswitch = "3";};
   if(patternMatch(zAxisIdentifier,"IP")){wheelswitch = "2";};
//     DebugN( "chamber " + wheelz + " wheelswitch " + wheelswitch);
    for (int i = 1; i <= 16; i += 1) { 
      string sector;
      if(i<=9){sector = "0" + i ; }
      else{sector = i;}
      for (int j = 1; j <= 2; j++) {
       chamberName = chambername_prefix + j +  side + sector + zAxisSuffix;  
//                DebugN(" > This is muUiGeo_nsw_drawChamberWheel <   ---> Chambername is: " + chamberName);
      obj_name = chamberName;// + zAxisSuffix;
//       DebugN("obj_name: " + obj_name);
//       DebugN("shapeIdentigier:" + shapeIdentifier);
      
      if (shapeExists(obj_name)) {
        points[1][1] = center_x + scale * rad_pos[i][j] * cos(deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[1][2] = center_y + scale * rad_pos[i][j] * sin(-deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[2][1] = center_x + scale * rad_pos[i][j+1] * cos(deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[2][2] = center_y + scale * rad_pos[i][j+1] * sin(-deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[3][1] = center_x + scale * rad_pos[i][j+1] * cos(deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[3][2] = center_y + scale * rad_pos[i][j+1] * sin(-deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[4][1] = center_x + scale * rad_pos[i][j] * cos(deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[4][2] = center_y + scale * rad_pos[i][j] * sin(-deg2rad(phi_angle[i] - open_angle[i]/2.));
//         DebugN(" p1: " + points[1][1] +" p1: " + points[1][2] +" p2: " + points[2][1]+ " p2: " + points[2][2]);
//         DebugN(" p3: " + points[3][1] +" p3: " + points[3][2] +" p4: " + points[4][1]+ " p4: " + points[4][2]);
        setValue(obj_name + "." + shapeIdentifier, "points", points);
        setValue(obj_name + "." + shapeIdentifier, "visible", TRUE);
      }}
    } 
 }
  
  
  // wheel_type == stgc  
  if(patternMatch(wheel_type,"STGC")){
   if(patternMatch(zAxisIdentifier,"HO")){wheelswitch = "4";};
   if(patternMatch(zAxisIdentifier,"IP")){wheelswitch = "1";};
    for (int i = 1; i <= 16; i += 1) { 
      string sector;
      if(i<=9){sector = "0" + i ; }
      else{sector = i;}
      for (int j = 1; j <= 4; j++) {
       chamberName = chambername_prefix + j +  side + sector + zAxisSuffix;  

      obj_name = chamberName ;//+ zAxisSuffix;
//       DebugN("obj_name: " + obj_name);
//       DebugN("shapeIdentigier:" + shapeIdentifier);
      
      if (shapeExists(obj_name)) {
        points[1][1] = center_x + scale * rad_pos[i][j] * cos(deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[1][2] = center_y + scale * rad_pos[i][j] * sin(-deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[2][1] = center_x + scale * rad_pos[i][j+1] * cos(deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[2][2] = center_y + scale * rad_pos[i][j+1] * sin(-deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[3][1] = center_x + scale * rad_pos[i][j+1] * cos(deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[3][2] = center_y + scale * rad_pos[i][j+1] * sin(-deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[4][1] = center_x + scale * rad_pos[i][j] * cos(deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[4][2] = center_y + scale * rad_pos[i][j] * sin(-deg2rad(phi_angle[i] - open_angle[i]/2.));
//         DebugN(" p1: " + points[1][1] +" p1: " + points[1][2] +" p2: " + points[2][1]+ " p2: " + points[2][2]);
//         DebugN(" p3: " + points[3][1] +" p3: " + points[3][2] +" p4: " + points[4][1]+ " p4: " + points[4][2]);
        setValue(obj_name + "." + shapeIdentifier, "points", points);
        setValue(obj_name + "." + shapeIdentifier, "visible", TRUE);
      }
      else
      {
        DebugN("Shape " + obj_name + " does not exist!");
      }
    }
    } 
    }
 
}
//////////////////////////////////////////////////////////////////////////////////////////  
void muUiGeo_nsw_drawSector(string wheel_type, char side, string chamber, string sector, string ObjIdentifier, string shapeIdentifier, int center_x, int center_y, 
                           float scale, dyn_dyn_float rad_pos, dyn_float phi_angle, dyn_float open_angle) {
  
//   DebugN("This is draw wheel function");
  string obj_name, chamberName;
  dyn_dyn_int points;
  string wheelswitch;

  if (!dynContains(makeDynString("MM", "STGC"), wheel_type)) {
    DebugN("muUiGeo_mdt_drawWheel: Function does not know wheel_type = " + wheel_type);
  }
// MMG
  if(patternMatch(wheel_type,"MM")){
   if(patternMatch(chamber,"HO")){wheelswitch = "3";};
   if(patternMatch(chamber,"IP")){wheelswitch = "2";};
    DebugN( "chamber " + chamber + " wheelswitch " + wheelswitch);
// 2 radial
    int i = 5;
      for (int j = 1; j <= 2; j++) {
        if(i%2 ==0 ){chamberName = "EIS-A"+ wheelswitch + "-R" + j + "-" + side + sector;}
        else{chamberName = "EIL-A"+ wheelswitch + "-R" + j + "-" + side + sector;}
  
//        DebugN("drawhweel: " + chamberName);

      obj_name = chamberName + ObjIdentifier;
//       DebugN("obj_name: " + obj_name);
//       DebugN("shapeIdentigier:" + shapeIdentifier);
      
      if (shapeExists(obj_name)) {
        points[1][1] = center_x + scale * rad_pos[i][j] * cos(deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[1][2] = center_y + scale * rad_pos[i][j] * sin(-deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[2][1] = center_x + scale * rad_pos[i][j+1] * cos(deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[2][2] = center_y + scale * rad_pos[i][j+1] * sin(-deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[3][1] = center_x + scale * rad_pos[i][j+1] * cos(deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[3][2] = center_y + scale * rad_pos[i][j+1] * sin(-deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[4][1] = center_x + scale * rad_pos[i][j] * cos(deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[4][2] = center_y + scale * rad_pos[i][j] * sin(-deg2rad(phi_angle[i] - open_angle[i]/2.));
//         DebugN(" p1: " + points[1][1] +" p1: " + points[1][2] +" p2: " + points[2][1]+ " p2: " + points[2][2]);
//         DebugN(" p3: " + points[3][1] +" p3: " + points[3][2] +" p4: " + points[4][1]+ " p4: " + points[4][2]);
        setValue(obj_name + "." + shapeIdentifier, "points", points);
        setValue(obj_name + "." + shapeIdentifier, "visible", TRUE);
      }
    } 
 
  }


// wheel_type == stgc  
  if(patternMatch(wheel_type,"STGC")){
   if(patternMatch(chamber,"HO")){wheelswitch = "4";};
    if(patternMatch(chamber,"IP")){wheelswitch = "1";};
// 3 radial
        int i = 5;
      for (int j = 1; j <= 3; j++) {
        if(i%2 ==0 ){chamberName = "EIS-A"+ wheelswitch + "-R" + j + "-" + side + sector;}
        else{chamberName = "EIL-A"+ wheelswitch + "-R" + j + "-" + side + sector;}  
//        DebugN("drawhweel: " + chamberName);

      obj_name = chamberName + ObjIdentifier;
//       DebugN("obj_name: " + obj_name);
//       DebugN("shapeIdentigier:" + shapeIdentifier);
      
      if (shapeExists(obj_name)) {
        points[1][1] = center_x + scale * rad_pos[i][j] * cos(deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[1][2] = center_y + scale * rad_pos[i][j] * sin(-deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[2][1] = center_x + scale * rad_pos[i][j+1] * cos(deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[2][2] = center_y + scale * rad_pos[i][j+1] * sin(-deg2rad(phi_angle[i] + open_angle[i]/2.));
        points[3][1] = center_x + scale * rad_pos[i][j+1] * cos(deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[3][2] = center_y + scale * rad_pos[i][j+1] * sin(-deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[4][1] = center_x + scale * rad_pos[i][j] * cos(deg2rad(phi_angle[i] - open_angle[i]/2.));
        points[4][2] = center_y + scale * rad_pos[i][j] * sin(-deg2rad(phi_angle[i] - open_angle[i]/2.));
//         DebugN(" p1: " + points[1][1] +" p1: " + points[1][2] +" p2: " + points[2][1]+ " p2: " + points[2][2]);
//         DebugN(" p3: " + points[3][1] +" p3: " + points[3][2] +" p4: " + points[4][1]+ " p4: " + points[4][2]);
        setValue(obj_name + "." + shapeIdentifier, "points", points);
        setValue(obj_name + "." + shapeIdentifier, "visible", TRUE);
      }
    } 
 }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
// Assign opening angles, angles and radial positions to variables  |> T. Klapdor-Kleingrothaus <|
/////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

void muUiGeo_nsw_assignWheelGeometry(dyn_float &angle, dyn_float &open_angle, dyn_dyn_float &rad_pos_mm, dyn_dyn_float &rad_pos_stgc) {
    
  angle  =  makeDynFloat(180, 157.5, 135, 112.5, 90, 67.5, 45, 22.5, 0, -22.5, -45, -67.5, -90, -112.5, -135, -157.5);  
                                                                 // phi position (angle) of sector middle line
  open_angle  = makeDynFloat(25, 20, 25, 20, 25, 20, 25, 20, 25, 20, 25, 20, 25, 20, 25, 20);
                                                                 // sector opening angle 
// MMG 
  for (int i = 1; i <= 16; i++) { 
    dynAppend(rad_pos_mm, makeDynFloat(1000, 4000, 6000));  // radial position of chamber corners                                                                                                             
  }
  
// sTGC
  for (int i = 1; i <= 16; i++) {  
    dynAppend(rad_pos_stgc, makeDynFloat(1000,2000, 3500, 4750, 6000));  // radial position of chamber corners                                                        
  }  
                  
                    
}
