// # uses "nswPs_datapointHandling.ctl"
# uses "nswPs_deviceUnitHandling.ctl"
# uses "muPsUtil.ctl"
# uses "muUtil.ctl"
# uses "fwFsmAtlas.ctl"




// +FSM+ <> 1.
nswPsFsm_setHvChannelStateAndStatus(string nodeDpeDp, string nodeDpe, 
                                 string statusDp, int channelStatus,
                                 string invalidDp, bool invalid,
                                 string opcConnectionDp, bool opcConnection, 
                                 string driversDp, dyn_int driverNums,                          
                                 string mainframeVselDp, bool mainframeVsel)
{
  string state, status, domain, device, channel;
//   string state, status, channel, channelNode, chamber, domain, device;
//   dyn_string exceptionInfo;
   bool vSelV1;  
  
// -------------get OPC client number-------------
  
//   if (dpSubStr(opcConnectionDp, DPSUB_DP) == "_CAENOPCServer2") num = 26;
//   else num = 6;
   
  int num = 26;

  //string channel = nswPs_getChannelDpfromFsmChannel(device);                

  fwFsmAtlas_getNodeNameComponents(nodeDpeDp, domain, device);

//  DebugN("!!!!!!!!!!!!"+nodeDpe,nodeDpeDp, domain, device);

 channel = dpSubStr(statusDp, DPSUB_SYS_DP);     //to get the channel dp element
 // DebugN("!!!!!!!!!!!!"+channel);

  if ((!opcConnection) || !dynContains(driverNums, num) || invalid)
  {
    state = "UNKNOWN";
    status = "ERROR";
   //  DebugN("nswPsFsm.ctl/setHvChannelStateAndStatus/opcConnection : "+opcConnection,"driversNums : \n"+driverNums);
 //   DebugN("nswPsFsm.ctl/setHvChannelStateAndStatus/ invalid : "+invalid);
  }
  else
  {
     state = nswPs_getChannelState(channel);     // implement, on basis of actual.status = channelStatus
    status = nswPs_getChannelStatus(channel);     // implement 
    vSelV1 = TRUE; //nswPs_isVselV1(bcVsel, mainframeVsel);
    
 //   DebugN("nswPsFsm.ctl/setHvChannelStateAndStatus/else statement... "+state,status);
    
    if ((state == "ON") && (!vSelV1))
      state = "STANDBY";
  }
 // DebugN("state&status HV : "+state,status);
  fwFsmAtlas_setStatus(domain, device/*, channelNode*/, status, "");
  fwFsmAtlas_setDUState(domain, device/*channelNode*/, state, "");          
 // fwFsmAtlas_setDUState(domain, channel, state, "");
  
  //DebugN("++++++fwFsmAtlas_setStatus : "+domain, device , status);
  //DebugN("++++++fwFsmAtlas_setDUState : "+domain, device , state);
  //DebugN("++++++fwFsmAtlas_setDUState : "+domain, channel , state);
 
}


// +FSM+ <> 2.
 bool nswPsFsm_setupLvDuDpConnect(string domain, string device) {              
  
 //  DebugN("============= LV -- nswPsFsm_setupLvDuDpConnect =============");
   bool ret = TRUE;
  string channel, nodeDp;
  string mainframe, opcServer;
 //string board;
  
  channel = nswPs_getLVChannelDpfromFsmChannel(device);  

 // DebugN(channel,"dp exists = "+dpExists(channel));
 // DebugN("domain = "+domain ,"\n device = "+ device,"\n channel = "+ channel);
 
  if (!dpExists(channel)) {
    error("ERROR setting up LV DU Callback for " + device + ", channel " + channel + " does not exist. Aborting.");
    fwFsmAtlas_setStatus(domain, device, "ERROR");
    fwFsmAtlas_setDUState(domain, device, "UNKNOWN");
    return FALSE;
  }
 
  nodeDp = fwFsmAtlas_getNodeDPEName(domain, device); 
//DebugN("nodeDp : "+nodeDp);  
//  board = nswPs_getBoardDpOfDp(channel); 
  mainframe = nswPsUtil_getMainframeDpOfDp(channel);   
 // DebugN("mainframe : "+mainframe);  
 // opcServer = "_WienerMarathonOPCServer";              
 opcServer = "_CAENOPCServer";    
  if (dpConnect("nswPsFsm_setLvChannelStateAndStatus", 
                nodeDp, 
              //  channel + ".Status.On",
                channel + ".actual.status",
                channel + ".actual.status:_online.._invalid", 
                opcServer + ".Connected", 
                "_Connections.Driver.ManNums"/*, 
                mainframe + ".FrontPanInP.Vsel"*/) != 0) ret = FALSE;


//DebugN("**************** ret : "+ret);
 
  bool opcCon=TRUE;
  dpGet(opcServer + ".Connected",opcCon);  
 
DebugN(opcCon);
  return ret;
  
}
 
 
// +FSM+ <> 3.
nswPsFsm_setLvChannelStateAndStatus(string nodeDpeDp, string nodeDpe, 
                                 string statusDp, int channelStatus,
                                 string invalidDp, bool invalid,
                                 string opcConnectionDp, bool opcConnection, 
                                 string driversDp, dyn_int driverNums/*,                          
                                 string mainframeVselDp, bool mainframeVsel*/)
{
  string state, status, domain, device, channel;
 //  DebugN("================= LV -- nswPsFsm_setLvChannelStateAndStatus =================");
//DebugN("LV reads the channel : " + channel);
 
  bool vSelV1; 
 int num = 6;    // CAEN LV
 // int num = 14;   // Wiener LV 

 //DebugN("num for LV marat(h)on (StateLV) : "+num);
//  DebugN("opcConnection for LV marat(h)on (StateLV) : "+opcConnection);
//  DebugN("driverNums for LV marat(h)on (StateLV) : "+driverNums);
 
 DebugN("opcConnection for LV CAEN (StateLV) : "+opcConnection);
 DebugN("driverNums for LV CAEN (StateLV) : "+driverNums);
//     dpConnect("nswPs_getLVChannelStatus",
//             channel+".Status.On",
//             channel+".Status.On:_original.._invalid",
//             channel+".Status.FailureMaxTerminalVoltage",
//             channel+".Status.FailureMaxSenseVoltage", channel+".Status.FailureMaxCurrent",
//             channel+".Status.FailureMaxTemperature","ATLCSCSCS:_WienerMarathonOPCServer.Connected");
  
  
  fwFsmAtlas_getNodeNameComponents(nodeDpeDp, domain, device);
 
   channel = dpSubStr(statusDp, DPSUB_SYS_DP);
   
//DebugN("LV reads the channel (2) : " + channel);   
//DebugN("LV reads the channel (2) - statusDp : " + statusDp);         
 
   if ((!opcConnection) || !dynContains(driverNums, num) || invalid)
  {
    state = "UNKNOWN";
    status = "ERROR";
  }
  else
  {
     state = nswPs_getLVChannelState(channel);
//DebugN("++++++++++++++++ state LV :"+state);     
    status = nswPs_getLVChannelStatus(channel);    
//DebugN("++++++++++++++++ status LV :"+status);    
    vSelV1 = TRUE; // nswPs_isVselV1(bcVsel, mainframeVsel);
//     if ((state == "ON") && (!vSelV1))
//      state = "STANDBY";
  }
  
//DebugN("++++++++++++++++ state&status LV :"+state, status);

  fwFsmAtlas_setStatus(domain,device,status,"");
  fwFsmAtlas_setDUState(domain,device,state,"");

}



//////////////////////////////////////////////////////////////////////////////////////////////
// Replacement for PVSS dpSubStr, but without checking the DP exists !!!
// Currently works for sub = DPSUB_DP, _DP_EL, _SYS, _SYS_DP, SYS_DP_EL only.
//////////////////////////////////////////////////////////////////////////////////////////////
// +FSM+ <> 4.
// string nswUtil_dpSubStr(string dpeName, int sub = DPSUB_DP) {
//   
//   string sysName, dpName;
//   string config, attribute, tmp;
//   dyn_string comp, comp2, comp3;
//   
//   if (dpeName == "") return "";
//     
// remove the config part of dpeName. The config :_<config> of a DPE, care is needed to handle corrctly internal DPs of form <sysName>:_<DP>....
//   
//   if (strpos(dpeName, ":_") >= 0) {
//     if (strpos(dpeName, ".") < 0) {      // dpeName is a internal DP, no config part contained in dpeName
//       comp = dpeName;
//       comp2 = strsplit(dpeName, ":");  
//     }
//     else if (strpos(dpeName, ":_") > strpos(dpeName, ".")) {       // dpeName contains a config, but is not of <sysName>:_<DP> .... internal DP with sysName format
//       strreplace(dpeName, ":_", "&");   // since we can not do a strsplit with delimiter ":_"
//       comp = strsplit(dpeName, "&");    // remove config part
//       comp2 = strsplit(comp[1], ":");
//     }
//     else {                              // <sysName>:_<DP>.<EL> (_<config>) 
//       strreplace(dpeName, ":_", "&");   // since we can not do a strsplit with delimiter ":_"
//       comp = strsplit(dpeName, "&");    
//       comp[1] = comp[1] + ":_" + comp[2];
//       comp2 = strsplit(comp[1], ":");
//     }
//   }
//   else {
//     comp = dpeName;
//     comp2 = strsplit(dpeName, ":");  // neither contains a config nor is of form <sysName>:_<DP>.....
//   }
//        
///*
//  strreplace(dpeName, ":_", "&");   // since we can not do a strsplit with delimiter ":_"
//  comp = strsplit(dpeName, "&");    // remove config part
//  comp2 = strsplit(comp[1], ":");
// */
//   if (dynlen(comp2) > 1) {
//     sysName = comp2[1];
//     comp3 = strsplit(comp2[2], ".");
//   }
//   else comp3 = strsplit(comp2[1], ".");
//   
//   dpName = comp3[1]; 
//       
//   if (sub == DPSUB_DP) return (dpName);  
//   else if (sub == DPSUB_SYS) {
//     if (sysName != "")
//       return (sysName + ":");
//     else 
//       return "";
//   }
//   else if (sub == DPSUB_SYS_DP) {
//     if (sysName != "") return (sysName + ":" + dpName);
//     else return dpName;
//   }
//   else if (sub == DPSUB_SYS_DP_EL)
//     return comp[1];
//   else if (sub == DPSUB_DP_EL) {
//     tmp = comp[1];
//     strreplace(tmp, sysName + ":", "");
//     return tmp;
//   }
//   else return "";
//    
// }
// 

//////////////////////////////////////////////////////////////////////////////////////////
// Setting up DU callback for SY1527 Mainframe. To be called in DU initialize script.
//////////////////////////////////////////////////////////////////////////////////////////
// +FSM+ <> 5.
bool nswPsCaenFsm_SY1527_setupDuDpConnect(string domain, string device) {
 //   mdtCaenFsm_SY1527_setupDuDpConnect

  DebugN("This is nswPsCaenFsm_SY1527_setupDuDpConnect \t --> mainframe: ");
 return 1;
}


//////////////////////////////////////////////////////////////////////////////////////////
// SY1527 DU actions
//////////////////////////////////////////////////////////////////////////////////////////
// +FSM+ <> 6.
bool nswCaenFsm_SY1527_refresh(string domain, string device) {
  DebugN("SY1527_refresh");  
    return 1;
}
// +FSM+ <> 7.
bool nswCaenFsm_SY1527_clearAlarms(string domain, string device) {
  DebugN("SY1527_clearAlarms");
    return 1;
}
// +FSM+ <> 8.
bool nswCaenFsm_SY1527_killChans(string domain, string device) {
  DebugN("SY1527_killChans");
    return 1;
}

///////////////////////////////////////////////////////////////////////////////////////
// Trigger a Soft Reset via the external reset input.
// During a soft reset the mainframe CPU should reboot with the backend (branch controllers)
// unaffected.
///////////////////////////////////////////////////////////////////////////////////////
// +FSM+ <> 9.
bool nswCaenFsm_SY1527_softReset(string domain, string device) {
   DebugN("SY1527_softReset"); 
    return 1;
}

///////////////////////////////////////////////////////////////////////////////////////
// Trigger a Hard Reset via the external reset input.
// Reboot the mainframe CPU and reset all branch controllers (Power off)
///////////////////////////////////////////////////////////////////////////////////////
// +FSM+ <> 10.
bool nswCaenFsm_SY1527_hardReset(string domain, string device) {
  DebugN("SY1527_hardReset"); 
    return 1;
}


//////////////////////////////////////////////////////////////////////////////////////////
// Setting up DU callback for Branch Controller A1821 DUs. To be called in DU initialize script.
//////////////////////////////////////////////////////////////////////////////////////////
// +FSM+ <> 11.
bool nswCaenFsm_A1821_setupDuDpConnect(string domain, string device) {
  DebugN("This is : nswCaenFsm_A1821_setupDuDpConnect",domain,device);
 return 1;
 
}


//////////////////////////////////////////////////////////////////////////////////////////
// A1821 DU actions
//////////////////////////////////////////////////////////////////////////////////////////

// +FSM+ <> 12.
bool nswCaenFsm_A1821_refresh(string domain, string device) {
  DebugN("A1821_refresh");
  return 1;
}
// +FSM+ <> 13.
bool nswCaenFsm_A1821_setMode(string domain, string device, string mode) {
  DebugN("A1821_setMode");
  return 1;
}



///////////////////////////////////////////////////////////////////////////////////////////
// Do a remote recovery of the crate, without resetting channels, via the branch controller
// Remote Crate Recovery feature
///////////////////////////////////////////////////////////////////////////////////////////
// +FSM+ <> 14.
bool nswCaenFsm_A1821_sendCrateRecoverCmd(string domain, string device, int crate) {
 DebugN("A1821_sendCrateRecoverCmd"); 
    return 1;  
}

///////////////////////////////////////////////////////////////////////////////////////////
// Do a remote reset of the crate, WITH resetting channels, via the branch controller
// Remote Crate Reset feature
///////////////////////////////////////////////////////////////////////////////////////////
// +FSM+ <> 15.
bool nswCaenFsm_A1821_sendCrateResetCmd(string domain, string device, int crate) {
   DebugN("A1821_sendCrateResetCmd"); 
  return 1;
}
