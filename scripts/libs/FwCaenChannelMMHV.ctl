#uses "nswPs_deviceUnitHandling"

FwCaenChannelMMHV_initialize(string domain, string device)
{
  DebugN("initialize this domain: "  + domain + " for device: " + device);
  bool ret = nswPsHandl_Fsm_setupHvDuDpConnect_MM(domain, device);
  if (!ret) error("HV FSM: Setting up DU dpConnect failed for " + device, domain + "::" + device);
  
}


FwCaenChannelMMHV_valueChanged( string domain, string device, string &fwState )
{
}


FwCaenChannelMMHV_doCommand(string domain, string device, string command)
{
  string chamber, channelType, channel;
  dyn_string parts;
  DebugN("This is device actions!");
  parts = strsplit(device, "_");
  chamber = mdtPs_getChamberSystemName(parts[1]) + parts[1];
  channelType = parts[2];
  channel = mdtPs_getChannelOfChamber(chamber, channelType);
  DebugN("This is FwCaenChannel MM HV " );
  DebugN("chamber: " + chamber);
  DebugN("channel type: " + channelType);
  DebugN(" channel: " + channel);
  if (command == "SWITCH_ON")
    nswPsHandl_switchChannelOn( domain, device, channel, channelType, 1);
  else if (command == "RESET_TRIP")
    nswPsHandl_resetTripOnChannel(domain, device, channel, channelType, 0);
  else if (command == "SWITCH_OFF")
    nswPsHandl_switchChannelOff( domain, device, channel, channelType, 1);
  else if (command == "REFRESH")
    nswPsHandl_refreshChannelStatus(domain, device, channel, channelType, 0);
  else if (command == "LOCK_UNLOCK")
    nswPsHandl_lockUnlockChannel(domain, device, channel, channelType, 1);
}
