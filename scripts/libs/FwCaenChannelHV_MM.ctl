#uses "nswPs_deviceUnitHandling.ctl"
FwCaenChannelHV_MM_initialize(string domain, string device)
{
//   DebugN("this is init");
  nswPsHandl_Fsm_setupHvDuDpConnect_MM(domain,device);
}

FwCaenChannelHV_MM_valueChanged( string domain, string device, string &fwState )
{
}


#uses "nswPs_deviceUnitHandling"
#uses "nswPsUtil"

FwCaenChannelHV_MM_doCommand(string domain, string device, string command)
{
      DebugN("::::::::::::::::::::: THIS IST MM _ DO COMMAND ::::::::::::::::::::");
      DebugN("Domain is: " + domain);
      DebugN("Device is: " + device);
      DebugN("command is: " + command);
      
//         string chamber, channelType, channel;
//   dyn_string parts;
//   
//   parts = strsplit(device, "_");
//   chamber = nswPsUtil_getChamberSystemName(parts[1]) + parts[1];
//   channelType = parts[2];
//   channel = nswPsUtil_getChannelOfChamber(chamber, channelType);
  if (command == "SWITCH_ON")
    nswPsHandl_switchChannelOn( domain, device, /*channel, channelType,*/ 1);
  else if (command == "RESET_TRIP")
    nswPsHandl_resetTripOnChannel(domain, device, /*channel, channelType,*/ 0);
  else if (command == "SWITCH_OFF")
    nswPsHandl_switchChannelOff( domain, device,/* channel, channelType, */1);
  else if (command == "REFRESH")
    nswPsHandl_refreshChannelStatus(domain, device, /*channel, channelType,*/ 0);
  else if (command == "LOCK_UNLOCK")
    nswPsHandl_lockUnlockChannel(domain, device, /*channel, channelType,*/ 1);
}


