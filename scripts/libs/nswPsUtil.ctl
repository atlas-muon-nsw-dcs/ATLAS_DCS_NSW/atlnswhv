/* ********************************************************************************
 * Library nswPsUtil.ctl: nsw power system utility functions
 * Managers: Ctrl. Ui.
 * Usage: Internal
 * by: T. Klapdor-Kleingrothaus
 *       
 * ********************************************************************************* */

#uses "nswConstants.ctl"
#uses "nswUtil.ctl"
// #uses "nswUserDb.ctl"
#uses "nswPsFsm.ctl"
# uses "nswPsGeneral.ctl"

#uses "muPsUtil.ctl"
///////////////////////////////////////////////////////////////////////
// Implemented Functions
//
// Constants:
//	nswPsUtil_STGC_MAINFRAME_PREFIX = "PSSTGC01";
//	nswPsUtil_MMG_MAINFRAME_PREFIX
//	nswPsUtil_PS_CHAMBER_DPT
// Name Handling:
//	nswPsUtil_getMainframeFromDp
//	nswPsUtil_getMainframeName
//	nswPsUtil_getBranchCtrlFromDp
//	nswPsUtil_getCrateFromDp
//	nswPsUtil_getBoardFromDp
//	nswPsUtil_isCaenHvBoard
//	nswPsUtil_isCaenLvBoard
//	nswPsUtil_getBoardFunction
//	nswPsUtil_getCrateFunction
//	nswPsUtil_removeSystemName
//	nswPsUtil_getDpSystemName
//    nswPsUtil_getSectorfromChamber
//     nswPsUtil_getZaxisfromChamber
//     nswPsUtil_getRadialfromChamber
//    nswPsUtil_getSidefromChamber
// Mapping:
//	nswPsUtil_getHvChanChambMap_MM
//	nswPsUtil_getHvChanChambMap_sTGC

// Sector:
// Chamber: 
//	nswPsUtil_getChambers4ChanDp
//	nswPsUtil_getChambFromHvMap
//	nswPsUtil_getChambersByPattern
//	nswPsUtil_getChannelOfChamber
//	nswPsUtil_getAllChambers
//	nswPsUtil_getAllChambersGlobally
//	nswPsUtil_getChambersOfChannel
//	nswPsUtil_getChamberOfHVChannel
//	nswPsUtil_getAllChambersOfPartition
//	nswPsUtil_getAllChambersOfStation
//	nswPsUtil_getAllChambersOfLayer
//	nswPsUtil_getAllChambersOfSide
//	nswPsUtil_getAllChambersOfSector
//	nswPsUtil_selectChambersOfStation
//	nswPsUtil_selectChambersOfSide
//	nswPsUtil_selectChambersOfLayer
//	nswPsUtil_selectChambersOfSector
//	nswPsUtil_selectChambersOfPartition
//	nswPsUtil_isChamberInPartition
//	nswPsUtil_getChamberSystemName
//	nswPsUtil_createChamber_List
// Layer:
// Partition:
//	nswPsUtil_getPartitionSystemName
//	nswPsUtil_getSystemPartition
//	nswPsUtil_getPartitionFromSystemName
//	nswPsUtil_getDpSystemPartition
//	nswPsUtil_getPartitionOfDp
// OPC: 
//	nswPsUtil_getCAENOPCServerDp
//	nswPsUtil_getCAENOPCClientNum
//	nswPsUtil_OPCgroupCreate
//	nswPsUtil_defaultOpcGroupsConfig
// Rack:
//	nswPsUtil_getUX15Rack4EasyCrate
//	nswPsUtil_getUS15Rack4EasyCrate
//	nswPsUtil_getGeneratorUX15RackConnections
//	nswPsUtil_getGeneratorUX15CrateConnections
// Mainframe:
//	nswPsUtil_getMainframeNum
//	nswPsUtil_getBoardNumMF
//	nswPsUtil_getMainframeDpOfDp
//	nswPsUtil_getMainframeOfChamber
//	nswPsUtil_getMainframeOfId
//	nswPsUtil_getMainframeOfBr
// Branch Controller
//	nswPsUtil_getRegion4BrCtrl
//	nswPsUtil_getBranchCtrlNum
//	nswPsUtil_getBrCtrlList
//	nswPsUtil_getBranchControllerOfDp
//	nswPsUtil_getBranchControllerDpOfDp
//	nswPsUtil_getIdNumbersForController
//	nswPsUtil_getControllerDpOutOfIdNumbers
//	nswPsUtil_getAllControllers
//	nswPsUtil_getAllControllersGlobally
//	nswPsUtil_getAllControllersOfPartition
// Crate:
//	nswPsUtil_getCrateNum
//	nswPsUtil_getCrateList
//	nswPsUtil_getEasyCrateDp4UX15Crate
//	nswPsUtil_getEasyCrate4UX15Crate
//	nswPsUtil_getEasyCrates4UX15Rack
//	nswPsUtil_getUX15Crate4EasyCrate
//	nswPsUtil_getGeneratorEasyCrateConnections
//	nswPsUtil_getGenerators4EasyCrate
//	nswPsUtil_getCrateOfDp
//	nswPsUtil_getCrateDpOfDp
//	nswPsUtil_getIdNumbersForCrate
//	nswPsUtil_getCrateDpOutOfIdNumbers
//	nswPsUtil_getGeneratorsOfCrate
//	nswPsUtil_getAllCrates
//	nswPsUtil_getAllCratesGlobally
//	nswPsUtil_getAllCratesOfPartition
//	nswPsUtil_getAllCratesOfBranchController
// Board: 
//	nswPsUtil_getBoardNum
//	nswPsUtil_getBoardList_MainFrame
//	nswPsUtil_getBoardList
//	nswPsUtil_getBoardDps
//	nswPsUtil_getBoardOfDp
//	nswPsUtil_typeOfBoard
//	nswPsUtil_modelOfBoard
//	nswPsUtil_getIdNumbersForBoard
//	nswPsUtil_getBoardDpOutOfIdNumbers
//	nswPsUtil_getAllBoards
//	nswPsUtil_getAllBoardsGlobally
//	nswPsUtil_getAllBoardsOfPartition
//	nswPsUtil_getAllBoardsOfCrate
//	nswPsUtil_getBoardDpOfDp
//	nswPsUtil_isVselV1
// Channel:
//	nswPsUtil_getHvChanDp
//	nswPsUtil_getHvChanChambFromMap
//	nswPsUtil_getChanNum
//	nswPsUtil_getChanDps
//	nswPsUtil_getChansForChambers
//	nswPsUtil_getChannelOfDp
//	nswPsUtil_typeOfChannel
//	nswPsUtil_getFlagsOfChannel
//	nswPsUtil_getFlagNamesOfChannel
//	nswPsUtil_addChannelInfo
//	nswPsUtil_getIdNumbersForChannel
//	nswPsUtil_getChannelDpOutOfIdNumbers
//	nswPsUtil_getChannelDpfromFsmChannel
//	nswPsUtil_getChannelsOfChamber
//	nswPsUtil_getChannelsOfBoard
//	nswPsUtil_getAllChannelsOfBoard
//	nswPsUtil_getAllChannelsOfCrate
//	nswPsUtil_getAllChannelsOfBranchController
//	nswPsUtil_getAllChannelsOfPartition
//	nswPsUtil_getAllChannels
//	nswPsUtil_getAllChannelsGlobally
//	nswPsUtil_getChannelState
//	nswPsUtil_getChannelStatus
// Status:
//	nswPsUtil_getStatus
//	nswPsUtil_isOn
//	nswPsUtil_isRamping
//	nswPsUtil_isRampUp
//	nswPsUtil_isRampDown
//	nswPsUtil_isOvC
//	nswPsUtil_isOvV
//	nswPsUtil_isUnV
//	nswPsUtil_isIntTrip
//	nswPsUtil_isExtTrip
//	nswPsUtil_isTrip
//	nswPsUtil_isHVMax
//	nswPsUtil_isExtDis
//	nswPsUtil_isCalErr
//	nswPsUtil_isUnplugged
//	nswPsUtil_isPowFail
//	nswPsUtil_isTErr
//	nswPsUtil_isOvVProt
//	nswPsUtil_isChanErr
//	nswPsUtil_getUnpluggedChannels
//	nswPsUtil_getAllUnpluggedChannelsOfPartition
//	nswPsUtil_getAllUnpluggedChannelsGlobally
//	nswPsUtil_isChannelUnplugged
//	nswPsUtil_isChannelSpare
//	nswPsUtil_isChannelSpareInPartition
//    nswPsUtil_getErrorFromStatus
// Generators:
//	nswPsUtil_generator_isOn
//	nswPsUtil_generator_isLocked
//	nswPsUtil_generator_switchPower
//	nswPsUtil_generator_switchConnectedChans
//	nswPsUtil_getGeneratorRack
//	nswPsUtil_getGeneratorChannel
//	nswPsUtil_getGeneratorNum
//	nswPsUtil_getGeneratorFilterNum
//	nswPsUtil_getGeneratorAssociatedGenerators
// FSM:
//	nswPsUtil_getFsmLayerNodeName
//	nswPsUtil_getFsmLVStatesColors
//	nswPsUtil_getFsmHVStatesColors
//	nswPsUtil_getHVChannelFSMType
//	nswPsUtil_getLVChannelFSMType
// FileDumps:
//	nswPsUtil_dumpUX15CrateMap
//	nswPsUtil_dumpGeneratorMap
//	nswPsUtil_dumpChamberMap
// Other:
//	nswPsUtil_getWatchdogChannelOfPartition
// Query:
// nswPsUtil_queryChanVals
// nswPsUtil_queryChansOn
// nswPsUtil_queryChambers4Board
// nswPsUtil_queryChanMaxVal
// nswPsUtil_queryChanMinVal


/////////////////////////////////////////////////////////////////////////////
//////////////////     +CONSTANTS+    /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// Device hierarchy prefix for the barrel mainframe
/////////////////////////////////////////////////////////////////////////////

const string nswPsUtil_STGC_MAINFRAME_PREFIX = "PSSTGC01";

/////////////////////////////////////////////////////////////////////////////
// Device hierarchy prefix for the endcap mainframe
/////////////////////////////////////////////////////////////////////////////

const string nswPsUtil_MMG_MAINFRAME_PREFIX = "PSMMG";    
    
/////////////////////////////////////////////////////////////////////////////
// DPT Name for PS Project MDT Chamber DPs
/////////////////////////////////////////////////////////////////////////////

const string nswPsUtil_PS_CHAMBER_DPT = "nswChamber";
 /////////////////////////////////////////////////////////////////////////////
//////////////////     +Name Handling+    /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
   
// +NameHandling+ <> 1.
// Get the SY4527 Mainframe DP from a channel, board, brController or crate DP
string nswPsUtil_getMainframeFromDp(string dpName) {
  
  const string separator = "/";
  string dp = "";
  dyn_string comp;  
  
  comp = strsplit(dpName, separator);
  
  if (dynlen(comp) >= 2) {
    dp = /*muUtil_dpSubStr(dpName, DPSUB_SYS) + "CAEN/" +*/ comp[2];
  }
  
  return dp;
}
// Get the SY4527 Mainframe DP from a channel, board, brController or crate DP
string nswPsUtil_getMainframeNrFromDp(string dpName) {
  string number = strltrim(nswPsUtil_getMainframeFromDp(dpName),"NSWMMG");
  return number;
}
//void  nswPsUtil_getMainframeFromDp(string dpName) {
//
//  const string separator = "/";
//  string dp = "";
//  dyn_string comp;  
//  
//  comp = strsplit(dpName, separator);
//  
//  if (dynlen(comp) >= 2) {
//  dp = muUtil_dpSubStr(dpName, DPSUB_SYS) + "CAEN/" + comp[2];
//  }
// //DebugN("......dp = " +dp);  
// //DebugN("........comp = " +comp[2]);   
// 
// return dp;
//}




// +NameHandling+ <> 2.
// Get the SY1527 Mainframe DP from a channel, board, brController or crate DP
string nswPsUtil_getMainframeName(string dpName) {
  
  return muPsUtil_getMainframeName(dpName);
  
}

// +NameHandling+ <> 3.
// Get the A1676 Branch controller DP from a channel, board or crate DP
string nswPsUtil_getBranchCtrlFromDp(string dpName) {
  const string separator = "/";
  dyn_string comp;    
  comp = strsplit(dpName, separator);
  return comp[3];
}

string nswPsUtil_getBranchCtrlNrFromDp(string dpName) {
  string number = strltrim(nswPsUtil_getBranchCtrlFromDp(dpName),"branchController");
  return number;
}

// +NameHandling+ <> 4.
// Get the Easy Crate DP from a channel or board DP
string nswPsUtil_getCrateFromDp(string dpName) {
  const string separator = "/";
  dyn_string comp;    
  comp = strsplit(dpName, separator);
  return comp[4];
}

string nswPsUtil_getCrateNrFromDp(string dpName) {
  string number = strltrim(nswPsUtil_getCrateFromDp(dpName),"easyCrate");
  return number;
}

// +NameHandling+ <> 5.
// Get the Easy Board DP from a channel DP
string nswPsUtil_getBoardFromDp(string dpName) {
  const string separator = "/";
  string dp = "";
  dyn_string comp;  
  
  comp = strsplit(dpName, separator);
  
  if (dynlen(comp) >= 5) {dp = comp[5];  }
  if (dynlen(comp) == 4) {dp = comp[3] ; }
  return dp;
}

// Get the Easy Board DP from a channel DP
string nswPsUtil_getBoardNrFromDp(string dpName) {
  string number, board;
  board= nswPsUtil_getBoardFromDp(dpName);
  if(strpos(board,"easyBoard") >= 0){number = strltrim(board,"easyBoard");}
  if(strpos(board,"board") >= 0){number = strltrim(board,"board");}
  return number;
}

string nswPsUtil_getChannelFromDP(string dpName) {
  string chan, type;
  
  type = strsplit(dpName,"/")[2];
  if( patternMatch("*STGC", type)) chan = strsplit(dpName, "/")[6];
  else if(patternMatch("*MM*", type)) chan = strsplit(dpName,"/")[4];
  else {
    errClass err;
    string cat, note;
    int prio, typ, co;
    note = "String not in specified range";
    prio = PRIO_INFO;
    typ = ERR_PARAM;
    co = 2;
    
    err = makeError(cat,prio,typ,co,note);
    throwError(err);
    }
    
  
  return chan;
}

string nswPsUtil_getChannelNrFromDP(string dpName) {
  string chan, chanNr;
  
  chan = nswPsUtil_getChannelFromDP(dpName);
  chanNr = substr(chan,7,2);
  
  return chanNr;
}


// +NameHandling+ <> 6.
// Caen device information
bool nswPsUtil_isCaenHvBoard(string model) {
  return muPsUtil_isCaenHvBoard(model);
}
  
// +NameHandling+ <> 7.
bool nswPsUtil_isCaenLvBoard(string model) {
  return muPsUtil_isCaenLvBoard(model);
}

// +NameHandling+ <> 8.
// Determine function of a Easy board or crate: HV, LV or MIXED (for crates)
string nswPsUtil_getBoardFunction(string boardDp) {
  return muPsUtil_getBoardFunction(boardDp);
}

// +NameHandling+ <> 9.
string nswPsUtil_getCrateFunction(string crateDp) {
  return muPsUtil_getCrateFunction(crateDp);
}
  
// +NameHandling+ <> 10.
string nswPsUtil_removeSystemName(string dp)
{
//   DebugN("This is nswPsUtil_removeSystemName \n with variable " + dp);
  string newDp;
  if (patternMatch(nswPsGeneral_SystemMM()+"*", dp) || patternMatch(nswPsGeneral_SystemsTGC()+"*", dp))
  {
    newDp = dpSubStr(dp,DPSUB_DP);
    return newDp;
  }
  else
    return dp;
}

// +NameHandling+ <> 11.
dyn_string nswPsUtil_getDpSystemName(string dp)
{
  dyn_string allSystems = makeDynString(mdtConstants_getBarrelPSProjectName(), mdtConstants_getEndcapPSProjectName());
  dyn_string dpSystems;
  
  for (int s=1; s<=dynlen(allSystems); s++)
  {
    if (dpExists(allSystems[s] + dp))
      dynAppend(dpSystems, allSystems[s]);
  }
  
  return dpSystems;
}

// +NameHandling+ <> 12.
string nswPs_removeSystemName(string dp)
{
  string newDp;
//   DebugN("This is nswPs_removeSystemName: " + dp);
  if ( patternMatch(nswPsGeneral_SystemMM()+"*", dp) ||  patternMatch(nswPsGeneral_SystemsTGC()+"*", dp))
  {
    newDp =dpSubStr(dp,DPSUB_DP);
//     DebugN("This is nswPs_removeSystemName with a new dp: " + newDp);
    return newDp;
  }
  else
    return dp;
}

// +NameHandling+ <> 13.
int nswPsUtil_getSectorfromChamber(string obj)
{
  // obj = "EIZ2R1A01
   obj = nswPs_removeSystemName(obj);
  string sectorcheck , sector;
  sectorcheck = substr(obj,7,1);
  if(sectorcheck == 1){sector = substr(obj,7,2);}
  else{sector = substr(obj,8,1); }
  return sector;
}

// +NameHandling+ <> 14.
int nswPsUtil_getZaxisfromChamber(string obj)
{
  // obj = "EIZ2R1A01
   obj = nswPs_removeSystemName(obj);
  string zaxis = substr(obj,3,1);
  return zaxis;
}

// +NameHandling+ <> 15.
int nswPsUtil_getRadialfromChamber(string obj)
{
  // obj = "EIZ2R1A01
//   DebugN(" This is nswPsUtil_getRadialfromChamber" + obj);
  obj = nswPs_removeSystemName(obj);
//   DebugN("sys name removed: " + obj);
  string radial = substr(obj,5,1);
  return radial;
}

// +NameHandling+ <> 16.
string nswPsUtil_getSidefromChamber(string obj)
{
  // obj = "EIZ2R1A01
   obj = nswPs_removeSystemName(obj);
  string side = substr(obj,6,1);
//   DebugN(">>> side is: " + side);
  return side;
}


/////////////////////////////////////////////////////////////////////////////
//////////////////     +Mapping+    /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
// Prepare a mapping of HV channels to chambers
// Can be used in particular in panels where chan -- chamber maps are needed
// repeatedly for many channels
// ml: Multilayer, use -1 for both
// Note: This function does not include the "MDT-RPC" chambers BMR, BOR
//////////////////////////////////////////////////////////////////////////////

// +Mapping+ <> 1.
mapping nswPsUtil_getHvChanChambMap_MM(int layer = -1) {

  mapping Map;
  string key;

  dyn_string chamberList;
  dyn_string dpes0, dpes1, dpes2,dpes3,dpes4;
  dyn_string chans0, chans1, chans2,chans3,chans4;
  dyn_string sysList = makeDynString("dist_1:");
  
  for (int i = 1; i <= dynlen(sysList); i++) {
    chamberList = dpNames(sysList[i] + "EIZ*R*", "nswChamberMM");
    dynClear(dpes0); dynClear(dpes1); dynClear(dpes2); dynClear(chans0); dynClear(chans1); dynClear(chans2);
    dynClear(dpes3); dynClear(dpes4); dynClear(chans3); dynClear(chans4);
//     DebugN(chamberList);
    for (int j = 1; j <= dynlen(chamberList); j++) 
    {
      if ((layer == -1) || (layer == 1)) dynAppend(dpes1, chamberList[j] + ".Mapping.RO.L1");
      if ((layer == -1) || (layer == 2)) dynAppend(dpes2, chamberList[j] + ".Mapping.RO.L2");
      if ((layer == -1) || (layer == 3)) dynAppend(dpes3, chamberList[j] + ".Mapping.RO.L3");
      if ((layer == -1) || (layer == 4)) dynAppend(dpes4, chamberList[j] + ".Mapping.RO.L4");
      if ((layer == -1) || (layer == 5)) dynAppend(dpes0, chamberList[j] + ".Mapping.Drift");

    }
    DebugN("dynlen Chamberlist " + dynlen(chamberList));
    if (dynlen(dpes0) > 0) dpGet(dpes0, chans0);
    if (dynlen(dpes1) > 0) dpGet(dpes1, chans1);    // do a single dpGet for all DPEs in a system, for efficiency 
    if (dynlen(dpes2) > 0) dpGet(dpes2, chans2);
    if (dynlen(dpes3) > 0) dpGet(dpes3, chans3);
    if (dynlen(dpes4) > 0) dpGet(dpes4, chans4);
    
   for (int j = 1; j <= dynlen(chans0);  j++) {
      key = chans0[j];
      if (layer == -1) Map[key] = dpSubStr(dpes0[j], DPSUB_DP) + "-D";
      else Map[key] = dpSubStr(dpes0[j], DPSUB_DP);
    }
    for (int j = 1; j <= dynlen(chans1);  j++) {
      key = chans1[j];
//       DebugN("key is: " + key);
      if (layer == -1) Map[key] = dpSubStr(dpes1[j], DPSUB_DP) + "-L1";
      else Map[key] = dpSubStr(dpes1[j], DPSUB_DP);
    }
     for (int j = 1; j <= dynlen(chans2);  j++) {
      key = chans2[j];
      if (layer == -1) Map[key] = dpSubStr(dpes2[j], DPSUB_DP) + "-L2";
      else Map[key] = dpSubStr(dpes2[j], DPSUB_DP);
    }
     for (int j = 1; j <= dynlen(chans3);  j++) {
      key = chans3[j];
      if (layer == -1) Map[key] = dpSubStr(dpes3[j], DPSUB_DP) + "-L3";
      else Map[key] = dpSubStr(dpes3[j], DPSUB_DP);
    }   
     for (int j = 1; j <= dynlen(chans4);  j++) {
      key = chans4[j];
      if (layer == -1) Map[key] = dpSubStr(dpes4[j], DPSUB_DP) + "-L4";
      else Map[key] = dpSubStr(dpes4[j], DPSUB_DP);
    }
  }
//  DebugN("Length: " + dynlen(chans1));
//   DebugN(Map["dist_1:MyMainFrame/board01/channel08"]);
//   DebugN(mappingGetKey(Map,1));
//   DebugN(Map);
  return Map;    
}


// +Mapping+ <> 2.
mapping nswPsUtil_getHvChanChambMap_sTGC(int layer = -1) {

  mapping Map;
  string key;

  dyn_string chamberList;
  dyn_string dpes0, dpes1, dpes2,dpes3,dpes4;
  dyn_string chans0, chans1, chans2,chans3,chans4;
  dyn_string sysList = makeDynString("dist_1:");
  
  for (int i = 1; i <= dynlen(sysList); i++) {
    chamberList = dpNames(sysList[i] + "EIZ*R*", "nswChambersTGC");
    dynClear(dpes0); dynClear(dpes1); dynClear(dpes2); dynClear(chans0); dynClear(chans1); dynClear(chans2);
    dynClear(dpes3); dynClear(dpes4); dynClear(chans3); dynClear(chans4);
//     DebugN(chamberList);
    for (int j = 1; j <= dynlen(chamberList); j++) 
    {
      if ((layer == -1) || (layer == 1)) dynAppend(dpes1, chamberList[j] + ".Mapping.RO.L1");
      if ((layer == -1) || (layer == 2)) dynAppend(dpes2, chamberList[j] + ".Mapping.RO.L2");
      if ((layer == -1) || (layer == 3)) dynAppend(dpes3, chamberList[j] + ".Mapping.RO.L3");
      if ((layer == -1) || (layer == 4)) dynAppend(dpes4, chamberList[j] + ".Mapping.RO.L4");

    }
    
    if (dynlen(dpes1) > 0) dpGet(dpes1, chans1);    // do a single dpGet for all DPEs in a system, for efficiency 
    if (dynlen(dpes2) > 0) dpGet(dpes2, chans2);
    if (dynlen(dpes3) > 0) dpGet(dpes3, chans3);
    if (dynlen(dpes4) > 0) dpGet(dpes4, chans4);
    for (int j = 1; j <= dynlen(chans1);  j++) {
      key = chans1[j];
//       DebugN("key is: " + key);
      if (layer == -1) Map[key] = dpSubStr(dpes1[j], DPSUB_DP) + "-L1";
      else Map[key] = dpSubStr(dpes1[j], DPSUB_DP);
    }
     for (int j = 1; j <= dynlen(chans2);  j++) {
      key = chans2[j];
      if (layer == -1) Map[key] = dpSubStr(dpes2[j], DPSUB_DP) + "-L2";
      else Map[key] = dpSubStr(dpes2[j], DPSUB_DP);
    }
     for (int j = 1; j <= dynlen(chans3);  j++) {
      key = chans3[j];
      if (layer == -1) Map[key] = dpSubStr(dpes3[j], DPSUB_DP) + "-L3";
      else Map[key] = dpSubStr(dpes3[j], DPSUB_DP);
    }   
     for (int j = 1; j <= dynlen(chans4);  j++) {
      key = chans4[j];
      if (layer == -1) Map[key] = dpSubStr(dpes4[j], DPSUB_DP) + "-L4";
      else Map[key] = dpSubStr(dpes4[j], DPSUB_DP);
    }
  }
//  DebugN("Length: " + dynlen(chans1));
//   DebugN(Map["dist_1:MyMainFrame/board01/channel08"]);
//   DebugN(mappingGetKey(Map,1));
//   DebugN(Map);
  return Map;    
}

 /////////////////////////////////////////////////////////////////////////////
//////////////////     Chamber    /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

 
// +Chamber+ <> 1.
// Find the chambers connected to a given CAEN channel
// Performance optimised due to stream-lined use of dpGet on arrays instead of loops
dyn_string nswPsUtil_getChambers4ChanDp(string chanDp, string inSystem = "") {
  
  dyn_string chamberList, dpList;
  dyn_string sysList;
  dyn_string valList;
 
  if (inSystem == "Local") dynAppend(sysList, getSystemName());
  else if (inSystem != "") dynAppend(sysList, inSystem);
  else sysList = makeDynString(nswConstants_getBarrelPSProjectName(), 
                               nswConstants_getEndcapPSProjectName());
  
  for (int i = 1; i <= dynlen(sysList); i++) {
    dpList = dpNames(sysList[i] + "*.Mapping.LV", nswPsUtil_PS_CHAMBER_DPT);
    dpGet(dpList, valList);
    for (int j = 1; j <= dynlen(dpList); j++) {
      if ((dpSubStr(valList[j], DPSUB_DP) == dpSubStr(chanDp, DPSUB_DP)) &&
          !dynContains(chamberList, dpSubStr(dpList[j], DPSUB_DP)))
        dynAppend(chamberList, dpSubStr(dpList[j], DPSUB_DP));
    }
    dpList = dpNames(sysList[i] + "*.Mapping.ML1", nswPsUtil_PS_CHAMBER_DPT);
    dpGet(dpList, valList);
    for (int j = 1; j <= dynlen(dpList); j++) {
      if ((dpSubStr(valList[j], DPSUB_DP) == dpSubStr(chanDp, DPSUB_DP)) &&
          !dynContains(chamberList, dpSubStr(dpList[j], DPSUB_DP)))
        dynAppend(chamberList, dpSubStr(dpList[j], DPSUB_DP));
    }
    dpList = dpNames(sysList[i] + "*.Mapping.ML2", nswPsUtil_PS_CHAMBER_DPT);
    dpGet(dpList, valList);
    for (int j = 1; j <= dynlen(dpList); j++) {
      if ((dpSubStr(valList[j], DPSUB_DP) == dpSubStr(chanDp, DPSUB_DP)) &&
          !dynContains(chamberList, dpSubStr(dpList[j], DPSUB_DP)))
        dynAppend(chamberList, dpSubStr(dpList[j], DPSUB_DP));
    }
  }
    
  return chamberList;
}

// +Chamber+ <> 2.
// Get Chamber name from Electronic Paths
string nswPsUtil_getChambFromHvMap(mapping map, string chamberName) {
//   DebugN("This gets it from a Map" + map);
  string returnvalue;

  for(int i =1;i<mappinglen(map);i++)
  {
    string map_name = mappingGetValue(map,i);
    DebugN("entered chambername: " + chamberName + "  key: " + map_name);

    if(patternMatch(map_name,chamberName))
    {
      DebugN("found for chamber: " + chamberName + " the entry: "  + mappingGetValue(map,i) + "\t with its key: " + mappingGetKey(map,i));
      returnvalue = mappingGetKey(map,i);
      break;
    }
  }
 
  return returnvalue;    
}

// +Chamber+ <> 3.
dyn_string nswPsUtil_getChambersByPattern(string patternText)
{
  dyn_string chambers = dpNames("*" + patternText + "*", "Fw_DUwithScript");
  
  return chambers;
}

// +Chamber+ <> 7.
string nswPsUtil_getChannelOfChamber(string chamberDp, string channelType)
{
  string channel;
  
  dpGet(chamberDp + ".Mapping." + channelType, channel);
  
  return channel;
}

// +Chamber+ <> 8.
dyn_string nswPsUtil_getAllChambers() //only for expert panel due to getSystemName
{
  dyn_string chambers = dpNames(getSystemName() + "*", "Fw_DUwithScript");
  
  return chambers;
}

// +Chamber+ <> 9.
dyn_string nswPsUtil_getAllChambersGlobally()
{
  dyn_string chambers;
  dyn_string projects = mdtConstants_getPsProjectNames();
  
  for (int p=1; p<=dynlen(projects); p++)
    dynAppend(chambers, dpNames(projects[p] + "*", "Fw_DUwithScript"));
  
  return chambers;
}

// +Chamber+ <> 10.
dyn_string nswPsUtil_getChambersOfChannel(string channelDp)
{
  dyn_string chambers = nswPs_getAllChambersGlobally();
  dyn_string chamberOfChannel;
  string ml1, ml2, lv;
  
  for (int c=1; c<=dynlen(chambers); c++)
  {
    dpGet(chambers[c] + ".Mapping.ML1", ml1);
    dpGet(chambers[c] + ".Mapping.ML2", ml2);
    dpGet(chambers[c] + ".Mapping.LV", lv);
    if (ml1 == channelDp)
    {
      chamberOfChannel = makeDynString(chambers[c], "ML1");
      return chamberOfChannel;
    }
    else if (ml2 == channelDp)
    {
      chamberOfChannel = makeDynString(chambers[c], "ML2");
      return chamberOfChannel;
    }
    else if (lv == channelDp)
      dynAppend(chamberOfChannel, chambers[c]);
  }
  dynAppend(chamberOfChannel, "LV");
  
  return chamberOfChannel;
}

// +Chamber+ <> 11.
dyn_string nswPsUtil_getChamberOfHVChannel(string channelDp) /// to be removed nswPs_getAllChambers(); replaced by nswPs_getChambersOfChannel
{
  dyn_string chambers = nswPs_getAllChambers();
  dyn_string chamberOfChannel;
  string ml1, ml2;
  
  for (int c=1; c<=dynlen(chambers); c++)
  {
    dpGet(chambers[c] + ".Mapping.ML1", ml1);
    dpGet(chambers[c] + ".Mapping.ML2", ml2);
    if (ml1 == channelDp)
      chamberOfChannel = makeDynString(nswPs_removeSystemName(chambers[c]), "ML1");
    else if (ml2 == channelDp)
      chamberOfChannel = makeDynString(nswPs_removeSystemName(chambers[c]), "ML2");
  }
  return chamberOfChannel;
}

// +Chamber+ <> 12.
dyn_string nswPsUtil_getAllChambersOfPartition(string partition)
{
  dyn_string chambersOfPartition;
  
  if (partition == "B")
    chambersOfPartition = dpNames(nswConstants_getBarrelPSProjectName() + "*", "Fw_DUwithScript");
  if (partition == "E")
    chambersOfPartition = dpNames(nswConstants_getEndcapPSProjectName() + "*", "Fw_DUwithScript");
  
  return chambersOfPartition;
  //DebugN("----- Display of chambers that belong to partition " + partition + " has finished");
}

// +Chamber+ <> 13.
dyn_string nswPsUtil_getAllChambersOfStation(anytype station)
{
  dyn_string chambers;
  dyn_string projects = mdtConstants_getPsProjectNames();
  string Station = (int)station;
  
  for (int p=1; p<=dynlen(projects); p++)
    dynAppend(chambers, dpNames(projects[p] + "???" + Station + "*", "Fw_DUwithScript"));
  
  return chambers; 
}

// +Chamber+ <> 14.
dyn_string nswPsUtil_getAllChambersOfLayer(string layer)
{
  dyn_string chambers;
  dyn_string projects = mdtConstants_getPsProjectNames();
  
  for (int p=1; p<=dynlen(projects); p++)
    dynAppend(chambers, dpNames(projects[p] + "?" + layer + "*", "Fw_DUwithScript"));
  
  return chambers;
}

// +Chamber+ <> 15.
dyn_string nswPsUtil_getAllChambersOfSide(string side)
{
  dyn_string chambers;
  dyn_string projects = mdtConstants_getPsProjectNames();
  
  for (int p=1; p<=dynlen(projects); p++)
  {
    dynAppend(chambers, dpNames(projects[p] + "????" + side + "*", "Fw_DUwithScript"));
    if (side == "A")
      dynAppend(chambers, dpNames(projects[p] + "????B*", "Fw_DUwithScript"));
  }
  
  return chambers;
}

// +Chamber+ <> 16.
dyn_string nswPsUtil_getAllChambersOfSector(anytype sector)
{
  dyn_string chambers;
//   dyn_string projects = mdtConstants_getPsProjectNames();
  string Sector;
  
  if ((int)sector < 10)
    Sector = "0" + (int)sector;
  else
    Sector = (int)sector;
//   for (int p=1; p<=dynlen(projects); p++)
//     dynAppend(chambers, dpNames(projects[p] + "?????" + Sector, "Fw_DUwithScript"));
    dynAppend(chambers, dpNames("???????" + Sector, "nswChamberMM"));
  
  return chambers;
}


// +Chamber+ <> 17.
dyn_string nswPsUtil_selectChambersOfStation(dyn_string chamberList, int station)
{
  dyn_string chambersOfStation;
  string chamber;
  
  for (int c=1; c<=dynlen(chamberList); c++)
  {
    chamber = nswPs_removeSystemName(chamberList[c]);
    if (substr(chamber, 3, 1) == station)
    {
      dynAppend(chambersOfStation, chamberList[c]);
      //DebugN(chamberList[c]);
    }
  }
  
  return chambersOfStation;
}


// +Chamber+ <> 18.
dyn_string nswPsUtil_selectChambersOfSide(dyn_string chamberList, string side)
{
  dyn_string chambersOfSide;
  string chamber;
  
  //DebugN(chamberList);
  for (int c=1; c<=dynlen(chamberList); c++)
  {
    chamber = nswPs_removeSystemName(chamberList[c]);
    //DebugN(chamber, side);
    if (substr(chamber, 4, 1) == side)
    {
      dynAppend(chambersOfSide, chamberList[c]);
      //DebugN(chamberList[c]);
    }
    else if ((substr(chamber, 4, 1) == "B") && (side == "A"))
      dynAppend(chambersOfSide, chamberList[c]);
  }

  return chambersOfSide;
}

// +Chamber+ <> 19.
dyn_string nswPsUtil_selectChambersOfLayer(dyn_string chamberList, string layer)
{
  dyn_string chambersOfLayer;
  string chamber;
  
  for (int c=1; c<=dynlen(chamberList); c++)
  {
    chamber = nswPs_removeSystemName(chamberList[c]);
    if (substr(chamber, 1, 1) == layer)
    {
      dynAppend(chambersOfLayer, chamberList[c]);
      //DebugN(chamberList[c]);
    }
  }

  return chambersOfLayer;
}

// +Chamber+ <> 20.
dyn_string nswPsUtil_selectChambersOfSector(dyn_string chamberList, anytype sector)
{
  dyn_string chambersOfSector;
  string chamber;
  
  for (int c=1; c<=dynlen(chamberList); c++)
  {
    chamber = nswPs_removeSystemName(chamberList[c]);
    if ((substr(chamber, 5, 2) == sector) || (substr(chamber, 5, 2) == "0" + sector))
    {
      dynAppend(chambersOfSector, chamberList[c]);
      //DebugN(chamberList[c]);
    }
  }

  return chambersOfSector;
}

// +Chamber+ <> 21.
dyn_string nswPsUtil_selectChambersOfPartition(dyn_string chamberList, string partition)
{
  dyn_string chambersOfPartition;
  string chamber;
  
  for (int c=1; c<=dynlen(chamberList); c++)
  {
    chamber = nswPs_removeSystemName(chamberList[c]);
    if (nswPs_isChamberInPartition(chamber, partition))
    {
      dynAppend(chambersOfPartition, chamberList[c]);
      //DebugN(chamberList[c]);
    }
  }
  
  return chambersOfPartition;
}

// +Chamber+ <> 22.
bool nswPsUtil_isChamberInPartition(string chamber, string partition )
{
  string chamberPartition;
  
  chamber = nswPs_removeSystemName(chamber);
  chamberPartition = substr(chamber, 0, 1);
  if ((substr(chamber, 0, 4) == "BIS7") || (substr(chamber, 0, 4) == "BIS8") || (substr(chamber, 0, 3) == "BEE"))
    chamberPartition = "E";
  if (partition == chamberPartition)
    return 1;
  else
    return 0;
}

// +Chamber+ <> 23.
string nswPsUtil_getChamberSystemName(string chamber)
{
  chamber = nswPs_removeSystemName(chamber);
//   if (nswPs_isChamberInPartition(chamber, "B"))
//     return mdtConstants_getBarrelPSProjectName();
//   else if (nswPs_isChamberInPartition(chamber, "E"))
//     return mdtConstants_getEndcapPSProjectName();
//   else
//   {
//     DebugN("Cannot find system name for " + chamber);
//     return "";
//   }
  return chamber;
}

// +Chamber+ <> 24.
nswPsUtil_createChamber_List()
{ 
  dyn_dyn_string elements;
  dyn_dyn_int types;
 
  elements[1]=makeDynString("Chamber_List"); 
  types[1]=makeDynInt(DPEL_DYN_STRING);
  dpTypeCreate(elements,types);
  dpCreate("list","Chamber_List");
}


/////////////////////////////////////////////////////////////////////////////
//////////////////     +Partition+    /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

// +Partition+ <> 1.
string nswPsUtil_getPartitionSystemName(string partition)
{
  string systemName;
  
  if (partition == "B")
    systemName = mdtConstants_getBarrelPSProjectName();
  else if (partition == "E")
    systemName = mdtConstants_getEndcapPSProjectName();
  
  return systemName;
}

// +Partition+ <> 2.
string nswPsUtil_getSystemPartition() // to be removed/replaced by nswPs_getPartitionFromSystemName (getSystemName)
{
  string partition;
  
  if (getSystemName() == mdtConstants_getBarrelPSProjectName())
    partition = "B";
  else if (getSystemName() == mdtConstants_getEndcapPSProjectName())
    partition = "E";
  
  return partition;
}

// +Partition+ <> 3.
string nswPsUtil_getPartitionFromSystemName(string systemName)
{
  string partition;
  
  if (systemName == mdtConstants_getBarrelPSProjectName())
    partition = "B";
  else if (systemName == mdtConstants_getEndcapPSProjectName())
    partition = "E";
  
  return partition;
}

// +Partition+ <> 4.
string nswPsUtil_getDpSystemPartition(string dp) // to be replaced by nswPs_getPartitionOfDp
{
  string partition;
  
  if (dpSubStr(dp, DPSUB_SYS) == mdtConstants_getBarrelPSProjectName())
    partition = "B";
  else if (dpSubStr(dp, DPSUB_SYS) == mdtConstants_getEndcapPSProjectName())
    partition = "E";
  
  return partition;
}

// +Partition+ <> 5.
string nswPsUtil_getPartitionOfDp(string dp) 
{
  string partition;
  
  if (dpSubStr(dp, DPSUB_SYS) == mdtConstants_getBarrelPSProjectName())
    partition = "B";
  else if (dpSubStr(dp, DPSUB_SYS) == mdtConstants_getEndcapPSProjectName())
    partition = "E";
  
  return partition;
}
/////////////////////////////////////////////////////////////////////////////
//////////////////     +OPC+    /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

   
// +OPC+ <> 1.
// Function to obtain the OPC Server DP for a CAEN device
string nswPsUtil_getCAENOPCServerDp(string dp, string sysName = "") {
  
  string serverDp = "_CAENOPCServer";
  string mainframe;
  
  mainframe = dpSubStr(nswPsUtil_getMainframeFromDp(dp), DPSUB_DP);
  
  if (sysName == "") sysName = dpSubStr(dp, DPSUB_SYS);
  if (sysName == "") sysName = getSystemName();
  
  return serverDp;  
}

// +OPC+ <> 2.
// Function to obtain the OPC Client number for a CAEN device. 
string nswPsUtil_getCAENOPCClientNum(string dp, string sysName = "") {
  
  string mainframe;
  int driver = 6;
  
  mainframe = dpSubStr(nswPsUtil_getMainframeFromDp(dp), DPSUB_DP);
  
  if (sysName == "") sysName = dpSubStr(dp, DPSUB_SYS);
  if (sysName == "") sysName = getSystemName();
      
  if ((sysName == nswConstants_getEndcapPSProjectName()) && 
      (mainframe == "CAEN/" + nswPsUtil_BARREL_MAINFRAME_PREFIX)) driver = 26;
  
  return driver;  
}

// +OPC+ <> 3.
nswPsUtil_OPCgroupCreate(string inSystem , string OPCgroup )
{
  if(!dpExists(OPCgroup))
  {
    dpCreate(OPCgroup, "_OPCGroup");
    DebugN("OPCgroup " + OPCgroup + " added to OPC groups");
  }
  dpSetWait(inSystem + OPCgroup + ".Active", 1);
  dpSetWait(inSystem + OPCgroup + ".DataSourceDevice", 1);
  dpSetWait(inSystem + OPCgroup + ".EnableCallback", 1);
  dpSetWait(inSystem + OPCgroup + ".GetIds", 1);

  if (substr(OPCgroup, 0, 23) == "_CAENOPCGroupVeryFastIn")  
  {
    dpSetWait(inSystem + OPCgroup + ".UpdateRateReq", 1000);
    dpSetWait(inSystem + OPCgroup + ".UpdateRateAct", 1000);
  }
  else if (substr(OPCgroup, 0, 19) == "_CAENOPCGroupFastIn")  
  {
    dpSetWait(inSystem + OPCgroup + ".UpdateRateReq", 1000);
    dpSetWait(inSystem + OPCgroup + ".UpdateRateAct", 1000);
  }
  else if (substr(OPCgroup, 0, 24) == "_CAENOPCGroupVeryFastOut")  
  {
    dpSetWait(inSystem + OPCgroup + ".Refresh", 1);
    dpSetWait(inSystem + OPCgroup + ".UpdateRateReq", 10000);
    dpSetWait(inSystem + OPCgroup + ".UpdateRateAct", 10000);
  }
  else if (substr(OPCgroup, 0, 20) == "_CAENOPCGroupFastOut")  
  {
    dpSetWait(inSystem + OPCgroup + ".Refresh", 1);
    dpSetWait(inSystem + OPCgroup + ".UpdateRateReq", 10000);
    dpSetWait(inSystem + OPCgroup + ".UpdateRateAct", 10000);
  }
  else if (substr(OPCgroup,0,23) =="_CAENOPCGroupVerySlowIn")  
  {
    dpSetWait(inSystem + OPCgroup + ".UpdateRateReq", 10000);
    dpSetWait(inSystem + OPCgroup + ".UpdateRateAct", 10000);
  }
  else if (substr(OPCgroup, 0, 24) == "_CAENOPCGroupVerySlowOut")  
  {
    dpSetWait(inSystem + OPCgroup + ".Refresh", 1);  
    dpSetWait(inSystem + OPCgroup + ".UpdateRateReq", 10000);
    dpSetWait(inSystem + OPCgroup + ".UpdateRateAct", 10000);
  }
}

// +OPC+ <> 4.
nswPsUtil_defaultOpcGroupsConfig(string inSystem , string OPCgroup )
{
  if(!dpExists(OPCgroup))
  {
    dpCreate(OPCgroup, "_OPCGroup");
    DebugN("OPCgroup " + OPCgroup + " added to OPC groups");
  }
//   dpSetWait(inSystem + OPCgroup + ".Active", 1);
//   dpSetWait(inSystem + OPCgroup + ".DataSourceDevice", 1);
//   dpSetWait(inSystem + OPCgroup + ".EnableCallback", 1);
//   dpSetWait(inSystem + OPCgroup + ".GetIds", 1);

  if (OPCgroup == "_CAENOPCGroupIn")  
  {
    dpSetWait(inSystem + OPCgroup + ".UpdateRateReq", 1000);
    dpSetWait(inSystem + OPCgroup + ".UpdateRateAct", 1000);
  }
  else if (OPCgroup == "_CAENOPCGroupOut")  
  {
    dpSetWait(inSystem + OPCgroup + ".Refresh", 1);
    dpSetWait(inSystem + OPCgroup + ".UpdateRateReq", 10000);
    dpSetWait(inSystem + OPCgroup + ".UpdateRateAct", 10000);
  }
}


/////////////////////////////////////////////////////////////////////////////
//////////////////     +Rack+    /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// +Rack+ <> 1.
// Get UX15 Power Rack for a EASY crate specified by its branch controller and crate Id
string nswPsUtil_getUX15Rack4EasyCrate(string mainframe, int brCtrl, int crateId) {
  
  string sysName = nswConstants_getPsRackProjectName();
  string dp, subdet, rack;
  dyn_string dpList, comp;
  
  if (brCtrl <= 9) 
    dpList = dpNames(sysName + "*" + mainframe + "/branchController0" + brCtrl + "/easyCrate" + crateId, "muPsEasyCrate");
  else
    dpList = dpNames(sysName + "*" + mainframe + "/branchController" + brCtrl + "/easyCrate" + crateId, "muPsEasyCrate");
  
  for (int i = 1; i <= dynlen(dpList); i++) {
    dpGet(dpList[i] + ".UX15Crate", dp);
    if (dpExists(dp)) {
      dpGet(dp + ".SubDetector", subdet);
      if (subdet != "MDT") continue;
      else {
        comp = strsplit(dpSubStr(dp, DPSUB_DP), "/");
        rack = comp[2];
        return rack;
      }
    }
  }
    
    return "";
}

// +Rack+ <> 2.
// Get US15 Rack(s) a EASY crate specified by its branch controller and crate Id is connected
// to 48V wise. connType: Srv or Pw.
dyn_string nswPsUtil_getUS15Rack4EasyCrate(string connType, string mainframe, int brCtrl, int crateId) {
  
  string sysName = nswConstants_getPsRackProjectName();
  string uxCrate, rack;
  dyn_string dpList, racks;
  
  uxCrate = nswPsUtil_getUX15Crate4EasyCrate(mainframe, brCtrl, crateId);
  
  if (uxCrate == "") return makeDynString();
  if ((connType != "Srv") && (connType != "Pw")) return makeDynString();
  
  dpList = dpNames(sysName + "US15/*/" + connType  + "/" + uxCrate, "muPsGeneratorCrate");
  for (int i = 1; i <= dynlen(dpList); i++) {
    strreplace(dpList[i], sysName + "US15/", "");
    rack = nswPsUtil_getGeneratorRack(dpList[i]);
    if (!dynContains(racks, rack))
      dynAppend(racks, rack);
  }    
  
  return racks;
}


// +Rack+ <> 3.
// Get UX15 racks connected to a given generator.
// DP dpName can be both of muPsGenerator and mdtCaenA348x type, following MDT naming
// conventions. chan: A or B, connType: Pw or Srv.
dyn_string nswPsUtil_getGeneratorUX15RackConnections(string dpName, string connType = "", string chan = "") {
  
  dyn_string rackList, comp;
  string generator, rack;
  int gen;
  string sysName = nswConstants_getPsRackProjectName();
  
  rack = nswPsUtil_getGeneratorRack(dpName);
  gen = nswPsUtil_getGeneratorNum(dpName);
  
  generator = "US15/" + rack + "/Generator" + gen;
  if (!dpExists(sysName + generator)) return rackList;
  
  rackList = dpNames(sysName + generator + "/*", "muPsGeneratorCrate");
  
  for (int i = dynlen(rackList); i >= 1; i--) {
    if ((chan != "") && !patternMatch("*/Chan" + chan + "/*", rackList[i])) 
      dynRemove(rackList, i);
    else if ((connType != "") && !patternMatch("*/" + connType + "/*", rackList[i]))
      dynRemove(rackList, i);
    else {
      comp = strsplit(dpSubStr(rackList[i], DPSUB_DP), "/");
      rack = comp[7];
      rackList[i] = rack;
    }
  }
  dynUnique(rackList);
  
  return rackList;
}

// +Rack+ <> 4.
// Get UX15 crates connected to a given generator.
// DP dpName can be both of muPsGenerator and mdtCaenA348x type, following MDT naming
// conventions. chan: A or B, connType: Pw or Srv.
dyn_string nswPsUtil_getGeneratorUX15CrateConnections(string dpName, string connType = "", string chan = "") {
  
  dyn_string crateList, comp;
  string generator, rack, crate;
  int gen;
  string sysName = nswConstants_getPsRackProjectName();
  
  rack = nswPsUtil_getGeneratorRack(dpName);
  gen = nswPsUtil_getGeneratorNum(dpName);
  
  generator = "US15/" + rack + "/Generator" + gen;
  if (!dpExists(sysName + generator)) return crateList;
  
  crateList = dpNames(sysName + generator + "/*", "muPsGeneratorCrate");
  
  for (int i = dynlen(crateList); i >= 1; i--) {
    if ((chan != "") && !patternMatch("*/Chan" + chan + "/*", crateList[i])) 
      dynRemove(crateList, i);
    else if ((connType != "") && !patternMatch("*/" + connType + "/*", crateList[i]))
      dynRemove(crateList, i);
    else {
      comp = strsplit(dpSubStr(crateList[i], DPSUB_DP), "/");
      rack = comp[7];
      crate = comp[8];
      crateList[i] = rack + "/" + crate;
    }
  }
  dynUnique(crateList);
  
  return crateList;
}


/////////////////////////////////////////////////////////////////////////////
//////////////////     +MainFrame+    /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

// +MainFrame+ <> 1.
// Utility functions for decomposing FwCaen DP names into crate, board, etc. 
int nswPsUtil_getMainframeNum(string dpName) {
  
  int num = -1;
  dyn_string comp;

  comp = strsplit(nswUtil_stripSystemName(dpName), "/");
  
  num = (int) substr(comp[2], strlen(comp[2]) - 1, 1);
  
  return num;
}

// +MainFrame+ <> 2.
// Get the number of Mainframe Boards
int nswPsUtil_getBoardNumMF(string dpName) {
  
  int num = -1;
  dyn_string comp;
  
  comp = strsplit(dpSubStr(dpName, DPSUB_DP), "/");
  
  for (int i = 1; i <= dynlen(comp); i++) {
    if (patternMatch("board*", comp[i])) {
      strreplace(comp[i], "board", "");
      num = (int) comp[i];
    }
  }
  
  return num;
}

// +MainFrame+ <> 3.
string nswPsUtil_getMainframeDpOfDp(string dpe) 
{
  string mainframe = "";
  dyn_string comp;
  
  comp = strsplit(dpe, "/");
  if ((dynlen(comp) > 1) && patternMatch("*CAEN", comp[1]))
   mainframe = comp[1] + "/" + comp[2];
 
 return mainframe;
}

// +MainFrame+ <> 4.
//Function to Define mainframe of Chamber after the system Splitting
string nswPsUtil_getMainframeOfChamber(string chamber)
{
  string mainframe = "";
  if (strlen(chamber)!=7)
    DebugN("Error: This function gets the name of the chamber as input");
  else if (strlen(chamber)==7)
  {
    if((substr(chamber,0,1)=="E")||
       (substr(chamber,0,3)=="BEE"))
      mainframe="PSMDT01"; //endcap
    else
      mainframe="PSMDT02"; //barrel and the BIS7* BIS8* although they are on endcap project
  }
  return mainframe;
}

// +MainFrame+ <> 5.
//function to get the mainframe of id (id example 10-4-6-1) added after
string nswPsUtil_getMainframeOfId(string id)
{
  dyn_string parts;
  string mainframe;
  
  parts = strsplit(id, "-");
  if(patternMatch(parts[1],"NSWSTGC"))
  {
    mainframe = "NSWSTGC";
  }
  else if(((int)parts[1]<=7)&&
     ((int)parts[1]>=0))
  {
    mainframe="NSWMMG"+parts[1];
  }
  
  else
    DebugN("Error: Not A valid MDT id Number");
  
  return mainframe;
}

// +MainFrame+ <> 6.
//function to get the mainframe of branchcontroller added after
//splitting the system to 2 mainframes by George Iakovidis
string nswPsUtil_getMainframeOfBr(int br)
{
  string mainframe;
  
  if((br<=7)&&
     (br>=0))
    mainframe="PSMDT02";
  else if((br<=15)&&
          (br>=10))
    mainframe="PSMDT01";
  else
    DebugN("Error: Not A valid MDT id Number");
  
  return mainframe;
}



/////////////////////////////////////////////////////////////////////////////
//////////////////     +BranchController+    /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
     
 
// +BranchController+ <> 1.
// Find the detector region belonging to a branch controller. Possible return
// values are BARREL, EOA, EMA, SWA, EOC, EMC, SWC.
// This function is e.g. useful to decide which detector view needs to be shown.
// Note: Depends on clear separation of brCtrl assignment. BIS7,8 are considered
//       as barrel here since they are part of the barrel power racks.
string nswPsUtil_getRegion4BrCtrl(string mainframe, int brCtrl) {
  
  string region = "";
  dyn_string chamberList;
  
  chamberList = nswPsUtil_queryChambers4BrCtrl(mainframe, brCtrl);
  
  for (int i = 1; i <= dynlen(chamberList); i++) {
    if (mdtUtil_isBarrelChamber(chamberList[i]) || mdtUtil_isRpcChamber(chamberList[i])) {
      region = "BARREL";
      break;
    }
    else if (patternMatch("EO[123456]A*", cahmberList[i])) {
      region = "EOA";
      break;
    }
    else if (patternMatch("EO[123456]C*", cahmberList[i])) {
      region = "EOC";
      break;
    }
   
  }
       
  return region;
}

// +BranchController+ <> 2.
int nswPsUtil_getBranchCtrlNum(string dpName) {
  
  return muPsUtil_getBranchCtrlNum(dpName);
  
}

// +BranchController+ <> 3.
// Get the list of branch controllers connected to a given mainframe in a system
dyn_int nswPsUtil_getBrCtrlList(string sysName, string mainframe) {
  
  dyn_int BrCtrlList;
  dyn_string dpList;
  string pattern;
  int crate;
  
  pattern = sysName + "CAEN/" + mainframe + "/branchController*";
  dpList = dpNames(pattern, "FwCaenBoardSY1527A1676");
  
  for (int i = 1; i <= dynlen(dpList); i++) {
    crate = nswPsUtil_getBranchCtrlNum(dpList[i]);
    if (!dynContains(BrCtrlList, crate))
      dynAppend(BrCtrlList, crate);   
  }
//   DebugN(BrCtrlList);
  return BrCtrlList;
}

// +BranchController+ <> 4.
string nswPsUtil_getBranchControllerOfDp(string dp)
{
  int position = strpos(dp, "branchController");
  string brControllerNumber;
  if (position>-1) 
  brControllerNumber = substr(dp, position + 16, 2); // +16 ("branchController")
  else {
    position = strpos(dp, "BrCntr");
    brControllerNumber = substr(dp, position + 7, 2); 
    DebugN("---------------------> " + position + " : " + brControllerNumber);
  }

  
  return brControllerNumber;
}

// +BranchController+ <> 5.
string nswPsUtil_getBranchControllerDpOfDp(string dp)
{
  int position = strpos(dp, "branchController");
  string brControllerNumber = substr(dp, 0, position + 18); // +18 ("branchController00")
  
  return brControllerNumber;
}

// +BranchController+ <> 6.
string nswPsUtil_getIdNumbersForController(string controllerDp)
{
  string branchController;
  
  branchController = nswPs_getBranchControllerOfDp(controllerDp);
  
  return branchController;
}

// +BranchController+ <> 7.
dyn_string nswPsUtil_getControllerDpOutOfIdNumbers(string id)
{
  dyn_string controllers, dpSystems, parts;
  string controller, mainframe;
  
  mainframe = nswPs_getMainframeOfId(id);
  parts = strsplit(id, "-");
  controller = "CAEN/"+mainframe+"/branchController" + parts[1];
  dpSystems = nswPs_getDpSystemName(controller);
  for (int s=1; s<=dynlen(dpSystems); s++)
  {
    controllers[s] = dpSystems[s] + controller;
  }
  
  return controllers;
}

// +BranchController+ <> 8.
dyn_string nswPsUtil_getAllControllers() //only for expert panel due to getSystemName
{
  dyn_string controllers = dpNames(getSystemName() + "*", "FwCaenBoardSY1527A1676");
  
  return controllers;
}

// +BranchController+ <> 9.
dyn_string nswPsUtil_getAllControllersGlobally()
{
  dyn_string controllers;
  dyn_string projects = mdtConstants_getPsProjectNames();
  
  for (int p=1; p<=dynlen(projects); p++)
    dynAppend(controllers, dpNames(projects[p] + "*", "FwCaenBoardSY1527A1676"));
  return controllers;
}

// +BranchController+ <> 10.
dyn_string nswPsUtil_getAllControllersOfPartition(string partition)
{
  dyn_string controllers;
  
  if (partition == "B")
    controllers = dpNames(mdtConstants_getBarrelPSProjectName() + "*", "FwCaenBoardSY1527A1676");
  if (partition == "E")
    controllers = dpNames(mdtConstants_getEndcapPSProjectName() + "*", "FwCaenBoardSY1527A1676");
  
  return controllers;
}



/////////////////////////////////////////////////////////////////////////////
//////////////////     +Crate+    /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

// +Crate+ <> 1.
int nswPsUtil_getCrateNum(string dpName) {
  
  return muPsUtil_getCrateNum(dpName);
  
}

// +Crate+ <> 2.
// Get the list of crates connected to a given branch controller in a system
dyn_int nswPsUtil_getCrateList(string sysName, string mainframe, int brCtrlNum) {
  
  dyn_int crateList;
  dyn_string dpList;
  string num, pattern;
  int crate;
  
  if (brCtrlNum <= 9 ) num = "0" + brCtrlNum;
  else num = brCtrlNum;
  
  pattern = sysName + "CAEN/" + mainframe + "/branchController" + num + "*";
  dpList = dpNames(pattern, "FwCaenCrateEasy");
  
  for (int i = 1; i <= dynlen(dpList); i++) {
    crate = nswPsUtil_getCrateNum(dpList[i]);
    if (!dynContains(crateList, crate))
      dynAppend(crateList, crate);   
  }
  DebugN(crateList);
  return crateList;
}

// +Crate+ <> 3.
// Get FwCaenCrateEasy DP for a given UX15 power system crate
// parameter: crateDp: DP of DPT muPsRackUX15Crate
string nswPsUtil_getEasyCrateDp4UX15Crate(string crateDp) {
  
  string sysName = nswConstants_getPsRackProjectName();
  string fwCaenDp;
  
  if (!dpExists(crateDp) && !dpExists(sysName + crateDp)) return "";
  
  dpGet(crateDp + ".FwCaenCrateEasy", fwCaenDp);
  
  return fwCaenDp;
  
}

// +Crate+ <> 4.
// Get the branch Controller and crateId for a given UX15 power system crate
// parameter: crateDp: DP of DPT muPsRackUX15Crate
bool nswPsUtil_getEasyCrate4UX15Crate(string crateDp, string &mainframe, int &brCtrl, int &crateId) {
  
  string sysName = nswConstants_getPsRackProjectName();
  string fwCaenDp;
  
  mainframe = ""; brCtrl = -1; crateId = -1;
  if (!dpExists(crateDp) && !dpExists(sysName + crateDp)) return FALSE;
  
  dpGet(crateDp + ".FwCaenCrateEasy", fwCaenDp);
  
  if (fwCaenDp == "") return FALSE;
  
  mainframe = nswPsUtil_getMainframeName(fwCaenDp);
  brCtrl = nswPsUtil_getBranchCtrlNum(fwCaenDp);
  crateId = nswPsUtil_getCrateNum(fwCaenDp);
  
  return TRUE;
  
}

// +Crate+ <> 5.
// Get the branch Controllers and crateIds for a given UX15 power system rack and its crates
// parameter: crateDp: DP of DPT muPsRackUX15Crate
bool nswPsUtil_getEasyCrates4UX15Rack(string rack, dyn_dyn_string &crateList) {
  
  string sysName = nswConstants_getPsRackProjectName();
  dyn_string crates;
  string fwCaenDp, subdet;
  int brCtrl, crateId;
  string mainframe;
  
  dynClear(crateList);
  
  if (!patternMatch("*UX15/*", rack)) rack = "UX15/" + rack;
  
  if (!dpExists(rack) && !dpExists(sysName + rack)) return FALSE;
  
  crates = dpNames(sysName + dpSubStr(rack, DPSUB_DP) + "/Crate*", "muPsRackUX15Crate");
  for (int i = 1; i <= dynlen(crates); i++) {
    dpGet(crates[i] + ".FwCaenCrateEasy", fwCaenDp, crates[i] + ".SubDetector", subdet);
    if ((fwCaenDp != "") && (subdet == "MDT")) {
      mainframe = nswPsUtil_getMainframeName(fwCaenDp);
      brCtrl = nswPsUtil_getBranchCtrlNum(fwCaenDp);
      crateId = nswPsUtil_getCrateNum(fwCaenDp);
      if ((brCtrl >= 0) && (crateId >= 0)) 
        dynAppend(crateList, makeDynString(mainframe, brCtrl, crateId));
    }
  }
  
  return TRUE;
  
}

// +Crate+ <> 6.
// Get UX15 Power Crate DP for a EASY crate specified by its branch controller and crate Id
string nswPsUtil_getUX15Crate4EasyCrate(string mainframe, int brCtrl, int crateId) {
  
  string sysName = nswConstants_getPsRackProjectName();
  string dp, subdet;
  dyn_string dpList;
  
  if (brCtrl <= 9) 
    dpList = dpNames(sysName + "*" + mainframe + "/branchController0" + brCtrl + "/easyCrate" + crateId, "muPsEasyCrate");
  else
    dpList = dpNames(sysName + "*" + mainframe + "/branchController" + brCtrl + "/easyCrate" + crateId, "muPsEasyCrate");
  
  for (int i = 1; i <= dynlen(dpList); i++) {
    dpGet(dpList[i] + ".UX15Crate", dp);
    if (dpExists(dp)) {
      dpGet(dp + ".SubDetector", subdet);
      if (subdet == "MDT") return dpSubStr(dp, DPSUB_DP);
    }
  }
  return "";
}


// +Crate+ <> 7.
// Get easy crates connected to a given generator.
// DP dpName can be both of muPsGenerator and mdtCaenA348x type, following MDT naming
// conventions. chan: A or B, connType: Pw or Srv.
// Return value: dyn_dyn_string array with entries (mainframe, brCtrl, crateId)
dyn_dyn_string nswPsUtil_getGeneratorEasyCrateConnections(string dpName, string connType = "", string chan = "") {
  
  dyn_string rackList, comp;
  dyn_dyn_string crateList;
  string generator, rack;
  string dp, fwCaenDp;
  int gen, brCtrl, crateId;
  string mainframe;
  string sysName = nswConstants_getPsRackProjectName();
  
  rack = nswPsUtil_getGeneratorRack(dpName);
  gen = nswPsUtil_getGeneratorNum(dpName);
  
  generator = "US15/" + rack + "/Generator" + gen;
  if (!dpExists(sysName + generator)) return makeDynString();
  
  rackList = dpNames(sysName + generator + "/*", "muPsGeneratorCrate");
  
  
  for (int i = dynlen(rackList); i >= 1; i--) {
    if ((chan != "") && !patternMatch("*/Chan" + chan + "/*", rackList[i])) 
      dynRemove(rackList, i);
    else if ((connType != "") && !patternMatch("*/" + connType + "/*", rackList[i]))
      dynRemove(rackList, i);
    else {
      dp = sysName;
      fwCaenDp = "";
      comp = strsplit(dpSubStr(rackList[i], DPSUB_DP), "/");
      for (int j = 1; j <= dynlen(comp); j++) {
        if (patternMatch("UX15", comp[j])) dp += (comp[j] + "/");
        else if (patternMatch("Y????X[0123456789]", comp[j])) dp += (comp[j] + "/");
        else if (patternMatch("Y?????X[AC]", comp[j])) dp += (comp[j] + "/");
        else if (patternMatch("Crate*", comp[j])) dp += comp[j];
      }
      if (dpExists(dp)) {
        dpGet(dp + ".FwCaenCrateEasy", fwCaenDp);
        mainframe = nswPsUtil_getMainframeName(fwCaenDp);
        brCtrl = nswPsUtil_getBranchCtrlNum(fwCaenDp);
        crateId = nswPsUtil_getCrateNum(fwCaenDp);
        if ((brCtrl >= 0) && (crateId >= 0))
          dynAppend(crateList, makeDynString(mainframe, brCtrl, crateId)); 
      }
    }
  }
  dynUnique(crateList);
  
  return crateList;
}

// +Crate+ <> 8.
// Get Generator(s) a EASY crate specified by its branch controller and crate Id is connected
// to 48V wise. connType: Srv or Pw.
// Return value: dyn_string array with entries <Rack>:<GeneratorNum><Chan>, e.g. Y0805S2:3A
dyn_string nswPsUtil_getGenerators4EasyCrate(string connType, string mainframe, int brCtrl, int crateId) {
  
  string sysName = nswConstants_getPsRackProjectName();
  string uxCrate, rack, chan, gen;
  dyn_string dpList, generators;
  
  uxCrate = nswPsUtil_getUX15Crate4EasyCrate(mainframe, brCtrl, crateId);
  
  if (uxCrate == "") return makeDynString();
  if ((connType != "Srv") && (connType != "Pw")) return makeDynString();
  
  dpList = dpNames(sysName + "US15/*/" + connType  + "/" + uxCrate, "muPsGeneratorCrate");
  for (int i = 1; i <= dynlen(dpList); i++) {
    chan = nswPsUtil_getGeneratorChannel(dpList[i]);
    gen = nswPsUtil_getGeneratorNum(dpList[i]);
    strreplace(dpList[i], sysName + "US15/", "");
    rack = nswPsUtil_getGeneratorRack(dpList[i]);
    if (!dynContains(generators, rack + ":" + gen + chan))
      dynAppend(generators, rack + ":" + gen + chan);
  }    
  
  return generators;
}

// +Crate+ <> 9.
string nswPsUtil_getCrateOfDp(string dp)
{
  int position = strpos(dp, "easyCrate");
  string crateNumber = substr(dp, position + 9, 1); // +9 ("easyCrate")
  
  return crateNumber;
}


// +Crate+ <> 10.
string nswPsUtil_getCrateDpOfDp(string dp)
{
  int position = strpos(dp, "easyCrate");
  string crateNumber = substr(dp, 0, position + 10); // +10 ("easyCrate1")
  
  return crateNumber;
}

// +Crate+ <> 11.
string nswPsUtil_getIdNumbersForCrate(string crateDp)
{
  string branchController, crate, idNumbers;
  
  branchController = nswPs_getBranchControllerOfDp(crateDp);
  crate = nswPs_getCrateOfDp(crateDp);
  idNumbers = branchController + "-" + crate;
  
  return idNumbers;
}

// +Crate+ <> 11.
dyn_string nswPsUtil_getCrateDpOutOfIdNumbers(string id)
{
  dyn_string crates, dpSystems, parts;
  string crate, mainframe;

  mainframe = nswPs_getMainframeOfId(id);  
  parts = strsplit(id, "-");
  crate = "CAEN/"+mainframe+"/branchController" + parts[1] + "/easyCrate" + parts[2];
  dpSystems = nswPs_getDpSystemName(crate);
  for (int s=1; s<=dynlen(dpSystems); s++)
  {
    crates[s] = dpSystems[s] + crate;
  }
  
  return crates;
}

// +Crate+ <> 12.
dyn_string nswPsUtil_getGeneratorsOfCrate(string crateDp)
{
  dyn_string serviceGen, powerGen;

  serviceGen = nswPsUtil_getGenerators4EasyCrate("Srv", nswPsUtil_getMainframeName(crateDp),
                                                 nswPs_getBranchControllerOfDp(crateDp), nswPs_getCrateOfDp(crateDp));
  powerGen = nswPsUtil_getGenerators4EasyCrate("Pw", nswPsUtil_getMainframeName(crateDp),
                                               nswPs_getBranchControllerOfDp(crateDp), nswPs_getCrateOfDp(crateDp));
  for (int p=1; p<=dynlen(powerGen); p++)
  {
    if (dynContains(serviceGen, powerGen[p])>0)
      continue;
    else
      dynAppend(serviceGen, powerGen[p]);
  }
  DebugN(serviceGen);
  return serviceGen;
}


// +Crate+ <> 13.
dyn_string nswPsUtil_getAllCrates() // only for expert panel due to getSystemName
{
  dyn_string crates = dpNames(getSystemName() + "*", "FwCaenCrateEasy");
  
  return crates;
}

// +Crate+ <> 14.
dyn_string nswPsUtil_getAllCratesGlobally()
{
  dyn_string crates;
  dyn_string projects = mdtConstants_getPsProjectNames();
  
  for (int p=1; p<=dynlen(projects); p++)
    dynAppend(crates, dpNames(projects[p] + "*", "FwCaenCrateEasy"));
  return crates;
}

// +Crate+ <> 15.
dyn_string nswPsUtil_getAllCratesOfPartition(string partition)
{
  dyn_string crates;
  
  if (partition == "B")
    crates = dpNames(mdtConstants_getBarrelPSProjectName() + "*", "FwCaenCrateEasy");
  if (partition == "E")
    crates = dpNames(mdtConstants_getEndcapPSProjectName() + "*", "FwCaenCrateEasy");
  
  return crates;
}

// +Crate+ <> 16.
dyn_string nswPsUtil_getAllCratesOfBranchController(string controllerDp)
{
  dyn_string crates;
  dyn_string projects = mdtConstants_getPsProjectNames();
  
  controllerDp = nswPs_removeSystemName(controllerDp);
  for (int p=1; p<=dynlen(projects); p++)
    dynAppend(crates, dpNames(projects[p] + controllerDp + "/*","FwCaenCrateEasy"));
  
  return crates;
}

/////////////////////////////////////////////////////////////////////////////
//////////////////     +Board+    /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// +Board+ <> 1.
int nswPsUtil_getBoardNum(string dpName) {
    
  int num = -1;
  dyn_string comp;
  string comparepattern;
  comp = strsplit(nswUtil_dpSubStr(dpName, DPSUB_DP), "/");
//   DebugN("comp : " + comp);
  if(patternMatch("NSWSTGC",comp[2]))
  {
    comparepattern="easyBoard";
  }
  else
  {
    comparepattern="board";
  }
  for (int i = 1; i <= dynlen(comp); i++) {
//    DebugN("comparepatter: " + comparepattern); 
    if (patternMatch(comparepattern + "*", comp[i])) {
      strreplace(comp[i], comparepattern, "");
      num = (int) comp[i];
    }
//     DebugN("board number is: " + num);
  }
  return num;
}

// +Board+ <> 2.
// Get the list of boards connected to a given Mainframe in a system
dyn_int nswPsUtil_getBoardList_MainFrame(string sysName, string mainframe) {
  
  dyn_int boardList;
  dyn_string dpList;
  string num, pattern;
  int board;
  
  pattern = sysName + "CAEN/" + mainframe + "/*";

  dpList = dpNames(pattern, "FwCaenBoardSY1527");

  for (int i = 1; i <= dynlen(dpList); i++) {
    board = nswPsUtil_getBoardNumMF(dpList[i]);
    if (!dynContains(boardList, board))
      dynAppend(boardList, board);   
  }
  
  return boardList;
}

// +Board+ <> 3.
// Get the list of boards connected to a given branch controller in a system
dyn_int nswPsUtil_getBoardList(string sysName, string mainframe, int brCtrl, int crate) {
  
  dyn_int boardList;
  dyn_string dpList;
  string num, pattern;
  int board;
  DebugN("brCtrl: " + brCtrl);
  if (brCtrl <= 9 ) num = "0" + brCtrl;
  else num = brCtrl;
   DebugN("num: " + num);
  pattern = sysName + "CAEN/" + mainframe + "/branchController" + num + "/easyCrate" + crate + "/*";
  DebugN("pattern boardss: " + pattern);
  dpList = dpNames(pattern, "FwCaenBoardEasy");
  DebugN("dpList: " + dpList);
  
  for (int i = 1; i <= dynlen(dpList); i++) {
    board = nswPsUtil_getBoardNum(dpList[i]);
    DebugN("Board: " + board + " boardList: " + boardList);
    if (!dynContains(boardList, board))
      dynAppend(boardList, board);   
  }
  
  return boardList;
}
 
// +Board+ <> 4.
dyn_string nswPsUtil_getBoardDps(dyn_string sysList, string mainframe = "*", int brCtrl = -1, int crate = -1) {
  
  dyn_string dpList, tmpList; 
  string sBrCtrl, sCrate;
  string pattern;

  if (brCtrl == -1) sBrCtrl = "*";
  else {
    if ((brCtrl > 15) || (brCtrl < 0)) {
      DebugN("WARNING nswPsUtil_getChanDps: " + brCtrl + " is not a valid branch Controller.");
      return makeDynString();
    }
    if (brCtrl <= 9) sBrCtrl = "0" + brCtrl;
    else sBrCtrl = brCtrl;
  }
  if (crate == -1) sCrate = "*";
  else sCrate = crate;
  
  for (int i = 1; i <= dynlen(sysList); i++) {
    pattern = sysList[i] + "CAEN/" + mainframe + "/branchController" + sBrCtrl + "/easyCrate" + sCrate + 
              "/easyBoard*";
    tmpList = dpNames(pattern, "FwCaenBoardEasy");
    dynAppend(dpList, tmpList);
  }
  
  return dpList;
}


// +Board+ <> 5.
string nswPsUtil_getBoardOfDp(string dp)
{
  int position = strpos(dp, "easyBoard");
  string boardNumber = substr(dp, position + 9, 2); // +9 ("easyBoard")
  
  return boardNumber;
}

 

// +Board+ <> 7.
string nswPsUtil_typeOfBoard(string boardDp)
{
  dyn_string hvBoards = makeDynString("A3540AP", "A3540P");
  dyn_string lvBoards = makeDynString("A3025B", "A3016B", "A3016", "A3025");
  string model;
  
  dpGet(boardDp + ".model", model);
  if (dynContains(hvBoards, model) >= 1)
    return "HV";
  else if (dynContains(lvBoards, model) >= 1)
    return "LV";
  else
    return "";
}

// +Board+ <> 8.
string nswPsUtil_modelOfBoard(string boardDp)
{
  dyn_string stgcBoards1 = makeDynString("A3540AP", "A3540P", "A3540");
  dyn_string stgcBoards2 = makeDynString("A3535AP", "A3535P");
  dyn_string mmBoards = makeDynString("A7038P", "A7038N","A7038AP", "A7038AN", "A7038STN", "A7038STP");

  string model;
  
  dpGet(boardDp + ".model", model);
//   DebugN("the model of: " + boardDp + ".model" + " is: " +  model);
  if (dynContains(stgcBoards1, model) >= 1)
    return "3540";
  else if (dynContains(stgcBoards2, model) >= 1)
    return "3535";
  else if (dynContains(mmBoards, model) >= 1)
    return model;
  else
    return "";
}

// +Board+ <> 9.
string nswPsUtil_getIdNumbersForBoard(string boardDp)
{
  string branchController, crate, board, idNumbers;
  
  branchController = nswPs_getBranchControllerOfDp(boardDp);
  crate = nswPs_getCrateOfDp(boardDp);
  board = nswPs_getBoardOfDp(boardDp);
  idNumbers = branchController + "-" + crate + "-" + board;
  
  return idNumbers;
}

// +Board+ <> 9.
string nswPsUtil_getBoardDpOutOfIdNumbers(string id)
{
  dyn_string boards, dpSystems, parts;
  string board, mainframe;
//   DebugN("id is: " + id);
  mainframe = nswPsUtil_getMainframeOfId(id);
//   DebugN("mainframe: " + mainframe);
  parts = strsplit(id, "-");
  if(dynlen(parts) > 2)   // STGC
  {
    board = "CAEN/"+mainframe+"/branchController" + parts[2] + "/easyCrate" + parts[3] + "/easyBoard" + parts[4];
  }
  if(dynlen(parts) == 2)  // MM
  {
    board = "CAEN/"+ mainframe + "/board" + parts[2];
  }
//   DebugN("board: " + board);
//   dpSystems = nswPsUtil_getDpSystemName(board);
//   for (int s=1; s<=dynlen(dpSystems); s++)
//   {
//     boards[s] = /*dpSystems[s] +*/ board;
//   }
  
  return board;
}

// +Board+ <> 10.
dyn_string nswPsUtil_getAllBoards() //only for expert panel due to getSystemName
{
  dyn_string boards = dpNames(getSystemName() + "*", "FwCaenBoardEasy");
  
  return boards;
}

// +Board+ <> 11.
dyn_string nswPsUtil_getAllBoardsGlobally()
{
  dyn_string boards;
  dyn_string projects = mdtConstants_getPsProjectNames();
  
  for (int p=1; p<=dynlen(projects); p++)
    dynAppend(boards, dpNames(projects[p] + "*", "FwCaenBoardEasy"));
  
  return boards;
}

// +Board+ <> 12.
dyn_string nswPsUtil_getAllBoardsOfPartition(string partition)
{
  dyn_string boardsOfPartition;
  
  if (partition == "B")
    boardsOfPartition = dpNames(mdtConstants_getBarrelPSProjectName() + "*", "FwCaenBoardEasy");
  if (partition == "E")
    boardsOfPartition = dpNames(mdtConstants_getEndcapPSProjectName() + "*", "FwCaenBoardEasy");
  
  return boardsOfPartition;
}

// +Board+ <> 13.
dyn_string nswPsUtil_getAllBoardsOfCrate(string crateDp)
{
  dyn_string boardsOfCrate;
  dyn_string projects = mdtConstants_getPsProjectNames();
  
  crateDp = nswPs_removeSystemName(crateDp);  
  for (int p=1; p<=dynlen(projects); p++)
    dynAppend(boardsOfCrate, dpNames(projects[p] + crateDp + "/*", "FwCaenBoardEasy"));
  
  return boardsOfCrate;
}

// +Board+ <> 14.
string nswPsUtil_getBoardDpOfDp(string dp)
{
  int position = strpos(dp, "easyBoard");
  string boardNumber = substr(dp, 0, position + 11); // +11 ("easyBoard01")
  DebugN("*************** boardNumber: "+boardNumber);
  
  return boardNumber;
  
}

// +Board+ <> 15.
bool nswPsUtil_isVselV1(anytype bcVsel, bool mainframeVsel)
{
  if (bcVsel == "0")
    return mainframeVsel;
  else if (bcVsel == "1")
    return 0;
  else if (bcVsel == "2")
    return 1;
}
// 


/////////////////////////////////////////////////////////////////////////////
//////////////////     +Channel+    /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

// +Channel+ <> 1.
// Function to obtain the HV channel (fwCaenChannel) DP name of a chamber
// returns "" if no valid channel exists.
string nswPsUtil_getHvChanDp (string chamber, int ml){
  
  string ret = "";
  string sysName;
  string hvChanDp;

//   if ((ml != 1) && (ml != 2)) return ret;
//   
//   chamber = dpSubStr(chamber, DPSUB_DP);
//   
//   if (inSystem == "Local") sysName = getSystemName();
//   else if (inSystem  == "") {
//     if (mdtUtil_isBarrelChamber(chamber)) 
//       sysName = nswConstants_getBarrelPSProjectName();
//     else if (mdtUtil_isRpcChamber(chamber)) 
//       sysName = nswConstants_getBarrelPSProjectName();
//     else if (mdtUtil_isEndcapChamber(chamber)) 
//       sysName = nswConstants_getEndcapPSProjectName();
//     else return ret;
//   }
//   else sysName = inSystem;
//   
//   if (inSystem != "Local") {
//     if (!mdtUtil_isDistributedConnected(sysName)) 
//       return ret;
//   }
// 
//   if (!dpExists(sysName + chamber)) return ret;
  
  dpGet(chamber + ".Mapping." + ml, hvChanDp);
  return sysName + dpSubStr(hvChanDp, DPSUB_DP);

}

// +Channel+ <> 2.
// string nswPsUtil_getHvChanChambFromMap(mapping map, string HvPath) {
//   DebugN("This gets it from a Map" + map);
//   string returnvalue;
// 
//   for(int i =1;i<mappinglen(map);i++)
//   {
//     string key = mappingGetKey(map,i);
//     DebugN("HvPath: " + HvPath + "  key: " + key);
// 
//     if(patternMatch(key,HvPath))
//     {
//       DebugN("found for HvPath : "+  HvPath + " the entry: "  + mappingGetValue(map,i) + "\t with its key: " + mappingGetKey(map,i));
//       returnvalue = mappingGetValue(map,i);
//     }
//   }
//  
//   return returnvalue;    
// }
// 
string nswPsUtil_getHvChanChambFromMap(mapping map, string HvPath) {
  DebugN("This gets it from a Map" + map);
  string returnvalue;

  for(int i =1;i<mappinglen(map);i++)
  {
    string key = mappingGetKey(map,i);
//     DebugN("HvPath: " + HvPath + "  key: " + key);

    if(patternMatch(key,HvPath))
    {
      DebugN("found for HvPath : "+  HvPath + " the entry: "  + mappingGetKey(map,i) + "\t with its key: " + mappingGetValue(map,i) );
      returnvalue = mappingGetValue(map,i);
//       break;
    }
  }
 
  return returnvalue;    
}


// +Channel+ <> 3.
int nswPsUtil_getChanNum(string dpName) {
  
  return muPsUtil_getChanNum(dpName);
  
}
dyn_string nswPsUtil_getChanDpList4BoardDp(string sysName, string boardDp) {
  
  return dpNames(sysName + dpSubStr(boardDp, DPSUB_DP) + "/channel*", "FwCaenChannel");
  
} 

// +Channel+ <> 4.
// Functions to retrieve Caen DPs from one or more systems
dyn_string nswPsUtil_getChanDps(dyn_string sysList, string mainframe = "*", int brCtrl = -1, 
                                int crate = -1, int board = -1) {
  
  dyn_string dpList, tmpList; 
  string sMainFr, sBrCtrl, sCrate, sBoard;
  string pattern;

  if (brCtrl == -1) sBrCtrl = "*";
  else {
//     if ((brCtrl > 15) || (brCtrl < 0)) {
//       DebugN("WARNING nswPsUtil_getChanDps: " + brCtrl + " is not a valid branch Controller.");
//       return makeDynString();
//     }
    if (brCtrl <= 9) sBrCtrl = "0" + brCtrl;
    else sBrCtrl = brCtrl;
  }
  if (crate == -1) sCrate = "*";
  else sCrate = crate;
  if (board == -1) sBoard = "*";
  else if (board <= 9) sBoard = "0" + board;
  else sBoard = board;
  
  DebugN("looking for mf: " + mainframe + " bc: " + sBrCtrl + " crate: " + sCrate + " board " + sBoard);
  for (int i = 1; i <= dynlen(sysList); i++) {
    if(brCtrl != 99)
    {
      pattern = sysList[i] + "CAEN/" + mainframe + "/branchController" + sBrCtrl + "/easyCrate" + sCrate + 
              "/easyBoard" + sBoard + "/channel*";
      DebugN("Easy stuff " + pattern);
    }
    else if(brCtrl == 99)
    {
      pattern = sysList[i] + "CAEN/" + mainframe + "/board" + sBoard + "/channel*";
      DebugN("Mainframe stuff " + pattern);
    } 
    tmpList = dpNames(pattern, "FwCaenChannel");
    dynAppend(dpList, tmpList);
  }
  
  return dpList;
}


// +Channel+ <> 5.
dyn_string nswPsUtil_getChansForChambers(dyn_string chamberList, string type, int ml = 0) {
  
  string chamber, sysName, chan;
  dyn_string chanList;
  for (int i = 1; i <= dynlen(chamberList); i++) {
    chamber = dpSubStr(chamberList[i], DPSUB_DP);
    if (mdtUtil_isBarrelChamber(chamber) || mdtUtil_isRpcChamber(chamber)) sysName = nswConstants_getBarrelPSProjectName();
    else if (mdtUtil_isEndcapChamber(chamber)) sysName = nswConstants_getEndcapPSProjectName();
    else continue;
    if (dpExists(sysName + chamber)) {
      if (type == "LV") dpGet(sysName + chamber + ".Mapping.LV", chan);
      else if ((type == "HV") && (ml == 1)) dpGet(sysName + chamber + ".Mapping.ML1", chan);
      else if ((type == "HV") && (ml == 2)) dpGet(sysName + chamber + ".Mapping.ML2", chan);
      if (dpExists(chan)) dynAppend(chanList, chan);
    }
  }
  return chanList;
}

// +Channel+ <> 6.
string nswPsUtil_getChannelOfDp(string dp)
{
  int position = strpos(dp, "channel");
  string channelNumber = substr(dp, position + 7, 3); // +7 ("channel")
  
  return channelNumber;
}

// +Channel+ <> 7.
string nswPsUtil_typeOfChannel(string channelDp)
{
  string board;
  dyn_string boards;
  
  boards = nswPs_getBoardDpOutOfIdNumbers(nswPs_getIdNumbersForChannel(channelDp));
  board = dpSubStr(channelDp, DPSUB_SYS) + nswPs_removeSystemName(boards[1]);
  
  return nswPs_typeOfBoard(board);
}

// +Channel+ <> 8.
dyn_bool nswPsUtil_getFlagsOfChannel(string chamberDp, string channelType)
{
  dyn_string flags = nswPs_getFlagNamesOfChannel(channelType);
  dyn_string flagValues;
  
  for (int f=1; f<=dynlen(flags); f++)
  {
    //DebugN(channelType);
    dpGet(chamberDp + "." + channelType + ".flags." + flags[f], flagValues[f]);
  }
  
  return flagValues;
}

// +Channel+ <> 8.
nswPs_setFlagsOfChannel(string chamberDp, string channelType, dyn_bool flagValues)
{
  dyn_string flags = nswPs_getFlagNamesOfChannel(channelType);
  
  for (int f=1; f<=dynlen(flags); f++)
  {
    //DebugN(channelType);
    dpSet(chamberDp + "." + channelType + ".flags." + flags[f], flagValues[f]);
  }
}

// +Channel+ <> 8.
dyn_string nswPsUtil_getFlagNamesOfChannel(string channelType)
{
  dyn_string flags = makeDynString("tempInterlock", "disabled", "partDisabled");
  string extraFlag;
  
  if ((channelType == "ML1") || (channelType == "ML2"))
    extraFlag = "gasInterlock";
  else if (channelType == "LV")
    extraFlag = "eltxInterlock";
  else
    extraFlag = "";
  dynInsertAt(flags, extraFlag, 1);
  
  return flags;
}

// +Channel+ <> 9.
nswPsUtil_addChannelInfo(string channelDp, string infoString)
{
  dyn_string info; 
  time t = getCurrentTime();
  
  dpGet(channelDp + ".userDefined.info", info);
  dynAppend(info, infoString + " on " + formatTime("%c", t));
  dpSet(channelDp + ".userDefined.info", info);
}

// +Channel+ <> 10.
string nswPsUtil_getIdNumbersForChannel(string channelDp)
{
  string branchController, crate, board, channel, idNumbers;
  
  branchController = nswPs_getBranchControllerOfDp(channelDp);
  crate = nswPs_getCrateOfDp(channelDp);
  board = nswPs_getBoardOfDp(channelDp);
  channel = nswPs_getChannelOfDp(channelDp);
  idNumbers = branchController + "-" + crate + "-" + board + "-" + channel;
  
  return idNumbers;
}

// +Channel+ <> 11.
string nswPsUtil_getChannelDpOutOfIdNumbers(string id)
{
  string channel,mainframe;
  dyn_string dpSystems, parts;
  
  mainframe = nswPs_getMainframeOfId(id);
  parts = strsplit(id, "-");
  channel = "CAEN/"+mainframe+"/branchController" + parts[1] + "/easyCrate" + parts[2] + "/easyBoard" + parts[3] + "/channel" + parts[4];
  dpSystems = nswPs_getDpSystemName(channel);
  channel = dpSystems[1] + channel;
  
  return channel;
}

// +Channel+ <> 12.
string nswPsUtil_getChannelDpfromFsmChannel(string mmgChannelName)             //HV_CHANNEL_00-11 == "mmgChannelName"
{ 
 
 string mmgChannelName = "HV_CHANNEL_09";  // generalise it...
  string sy = "ATLMMGSCS:CAEN/ATLnswPs1";
  string board = "/board0";
  string channel = "/channel0";
  string channelNum;
  string channelDp;
  
  channelNum = strltrim("mmgChannelName","HV_CHANNEL_");       
  
  if((int)channelNum>3){
    board=board+"0"; 
    channelNum = (int)channelNum-4; 
    channelNum = "0"+channelNum;
     DebugN("......if.......channelNum: "+channelNum+" \n Board: "+board+" \n channel: " + channel +"\n DP: "+ channelDp) ;
   }
  
  else
  {
    board=board+"1";  
     DebugN("......else.......channelNum: "+channelNum+" \n Board: "+board+" \n channel: " + channel +"\n DP: "+ channelDp) ;
  }
  
  channel = channel+channelNum;
  channelDp = sy+board+channel;
 dpGet( + ".Mapping." + channelType, channel);
ebugN("............."+ channel , channelDp);
  
  return channelDp;
}

// +Channel+ <> 13.
dyn_string nswPsUtil_getChannelsOfChamber(string chamberDp)
{
  dyn_string channels;
  string drift, hv1, hv2, hv3, hv4;
  


  dpGet(chamberDp + ".Mapping.RO.L1", hv1); 
  dynAppend(channels, hv1);
  dpGet(chamberDp + ".Mapping.RO.L2", hv2); 
  dynAppend(channels, hv2);
  dpGet(chamberDp + ".Mapping.RO.L3", hv3); 
  dynAppend(channels, hv3);
  dpGet(chamberDp + ".Mapping.RO.L4", hv4); 
  dynAppend(channels, hv4);

    if(dpExists(chamberDp + ".Mapping.Drift"))
  {
    dpGet(chamberDp + ".Mapping.Drift", drift);
    dynAppend(channels, drift);
  }
  return channels;
}

// +Channel+ <> 14.
dyn_string nswPsUtil_getChannelsOfBoard(string boardDp)  //to be removed-not to be used- use nswPs_getAllChannelsOfBoard
{
  dyn_string channels;
  
  channels = dpNames(boardDp + "/*","FwCaenChannel");
  
  return channels;
}

// +Channel+ <> 15.
dyn_string nswPsUtil_getAllChannelsOfBoard(string boardDp)  
{
  dyn_string channels;
  dyn_string projects = mdtConstants_getPsProjectNames();
  
  boardDp = nswPs_removeSystemName(boardDp);
  for (int p=1; p<=dynlen(projects); p++)
    dynAppend(channels, dpNames(projects[p] + boardDp + "*", "FwCaenChannel"));
  
  return channels;
}

// +Channel+ <> 16.
dyn_string nswPsUtil_getAllChannelsOfCrate(string crateDp)
{
  dyn_string channels;
  dyn_string projects = mdtConstants_getPsProjectNames();
  
  crateDp = nswPs_removeSystemName(crateDp);
  for (int p=1; p<=dynlen(projects); p++)
    dynAppend(channels, dpNames(projects[p] + crateDp + "/*","FwCaenChannel"));
  
  return channels;
}

// +Channel+ <> 17.
dyn_string nswPsUtil_getAllChannelsOfBranchController(string controllerDp)
{
  dyn_string channels;
  dyn_string projects = mdtConstants_getPsProjectNames();
  
  controllerDp = nswPs_removeSystemName(controllerDp);
  for (int p=1; p<=dynlen(projects); p++)
    dynAppend(channels, dpNames(projects[p] + controllerDp + "/*","FwCaenChannel"));
  
  return channels;
}

// +Channel+ <> 18.
dyn_string nswPsUtil_getAllChannelsOfPartition(string partition)
{
  dyn_string channels;
  
  if (partition == "B")
    channels = dpNames(mdtConstants_getBarrelPSProjectName() + "*", "FwCaenChannel");
  if (partition == "E")
    channels = dpNames(mdtConstants_getEndcapPSProjectName() + "*", "FwCaenChannel");
  
  return channels;
}

// +Channel+ <> 18.
dyn_string nswPsUtil_getAllChannels() //only for expert panel due to getSystemName
{
  dyn_string channels = dpNames(getSystemName() + "*", "FwCaenChannel");
  
  return channels;
}

// +Channel+ <> 19.
dyn_string nswPsUtil_getAllChannelsGlobally()
{
  dyn_string channels;
  dyn_string projects = mdtConstants_getPsProjectNames();
  
  for (int p=1; p<=dynlen(projects); p++)
    dynAppend(channels, dpNames(projects[p] + "*", "FwCaenChannel"));
  
  return channels;
}

// +Channel+ <> 20.
string nswPsUtil_getChannelState(string channel)
{
  int status;
  
  dpGet(channel + ".actual.status", status);
//   DebugN("Get Channel State: " + channel + " State: " + status);
  if (getBit(status, 11))
    return "UNPLUGGED";
  else if (getBit(status, 1))
    return "RAMP_UP";
  else if (getBit(status, 2))
    return "RAMP_DOWN";
  else if (getBit(status, 0))
    return "ON";
  else if (!getBit(status, 0))
    return "OFF";
}

// +Channel+ <> 21.
string nswPsUtil_getChannelStatus(string channel)
{
  int status;
  dpGet(channel + ".actual.status", status);
//   DebugN("Get Channel Status: " + channel + " Status: " + status);
  if (getBit(status, 15)) //TEMP_ERROR
    return "FATAL";
  else if (getBit(status, 14)) //POWER_FAIL
    return "FATAL";
  else if (getBit(status, 11)) //UNPLUGGED
    return "FATAL";
  else if (getBit(status, 10)) //CALIBR_ERROR
    return "WARNING";
  else if (getBit(status, 7)) //OVER_HVMAX
    return "ERROR";
  else if (getBit(status, 5)) //UNDERVOLTAGE
    return "WARNING";
  else if (getBit(status, 4)) //OVERVOLTAGE
    return "ERROR";
  else if (getBit(status, 9)) //TRIPPED
    return "ERROR";
  else if (getBit(status, 3)) //OVERCURRENT
    return "WARNING";
  else
    return "OK";
  
}




/////////////////////////////////////////////////////////////////////////////
//////////////////     +Status+    /////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// Extract various status bits from CAEN Channel status word
/////////////////////////////////////////////////////////////////////////////////

// +Generator+ <> 1.
bit32 nswPsUtil_getStatus(string chanDp) { return muPsUtil_getStatus(chanDp); }

// +Generator+ <> 2.
bool nswPsUtil_isOn(bit32 status) { return muPsUtil_isOn(status); }
  
// +Generator+ <> 3.
bool nswPsUtil_isRamping(bit32 status) { return muPsUtil_isRamping(status); }

// +Generator+ <> 4.
bool nswPsUtil_isRampUp(bit32 status) { return muPsUtil_isRampUp(status); }
  
// +Generator+ <> 5.
bool nswPsUtil_isRampDown(bit32 status) { return muPsUtil_isRampDown(status); }

// +Generator+ <> 6.
bool nswPsUtil_isOvC(bit32 status) { return muPsUtil_isOvC(status); }

// +Generator+ <> 7.
bool nswPsUtil_isOvV(bit32 status) { return muPsUtil_isOvV(status); }

// +Generator+ <> 8.
bool nswPsUtil_isUnV(bit32 status) { return muPsUtil_isUnV(status); }
 
// +Generator+ <> 9.
bool nswPsUtil_isIntTrip(bit32 status) { return muPsUtil_isIntTrip(status); }
  
// +Generator+ <> 10.
bool nswPsUtil_isExtTrip(bit32 status) { return muPsUtil_isExtTrip(status); }
  
// +Generator+ <> 11.
bool nswPsUtil_isTrip(bit32 status) { return muPsUtil_isTrip(status); }

// +Generator+ <> 12.
bool nswPsUtil_isHVMax(bit32 status) { return muPsUtil_isHVMax(status); }
  
// +Generator+ <> 13.
bool nswPsUtil_isExtDis(bit32 status) { return muPsUtil_isExtDis(status); }

// +Generator+ <> 14.
bool nswPsUtil_isCalErr(bit32 status) { return muPsUtil_isCalErr(status); }

// +Generator+ <> 15.
bool nswPsUtil_isUnplugged(bit32 status) { return muPsUtil_isUnplugged(status); }
  
// +Generator+ <> 16.
bool nswPsUtil_isPowFail(bit32 status) { return muPsUtil_isPowFail(status); }

// +Generator+ <> 17.
bool nswPsUtil_isTErr(bit32 status) { return muPsUtil_isTErr(status); }
  
// +Generator+ <> 18.
bool nswPsUtil_isOvVProt(bit32 status) { return muPsUtil_isOvVProt(status); }

// +Generator+ <> 19.
bool nswPsUtil_isChanErr(bit32 status) { return muPsUtil_isChanErr(status); }

// +Generator+ <> 20.
dyn_string nswPsUtil_getUnpluggedChannels() // to be removed- use _getAllUnplugged
{
  dyn_string unpluggedChannels;
  dyn_string channels = nswPs_getAllChannels();
  
  for (int c=1; c<=dynlen(channels); c++)
  {
    if (nswPs_isChannelUnplugged(channels[c]))
      dynAppend(unpluggedChannels, channels[c]);
  }
  return unpluggedChannels;
}

// +Generator+ <> 21.
dyn_string nswPsUtil_getAllUnpluggedChannelsOfPartition(string partition)
{
  dyn_string unpluggedChannels;
  dyn_string channels = nswPs_getAllChannelsOfPartition(partition);
  
  for (int c=1; c<=dynlen(channels); c++)
  {
    if (nswPs_isChannelUnplugged(channels[c]))
      dynAppend(unpluggedChannels, channels[c]);
  }
  return unpluggedChannels;
}

// +Generator+ <> 22.
dyn_string nswPsUtil_getAllUnpluggedChannelsGlobally()
{
  dyn_string unpluggedChannels;
  dyn_string channels = nswPs_getAllChannelsGlobally();
  
  for (int c=1; c<=dynlen(channels); c++)
  {
    if (nswPs_isChannelUnplugged(channels[c]))
      dynAppend(unpluggedChannels, channels[c]);
  }
  return unpluggedChannels;
}

// +Generator+ <> 23.
bool nswPsUtil_isChannelUnplugged(string dp)
{
  bool unplugged;
  
  if (!dpExists(dp + ".actual.unplugged"))
  {
    DebugN("Datapoint " + dp + " does not seem to have an 'unplugged' attribute");
    return 0;
  }
  else
  {
    dpGet(dp + ".actual.unplugged", unplugged);
    return unplugged;
  }
}

// +Generator+ <> 24.
bool nswPsUtil_isChannelSpare(string channel)
{
  dyn_string chambers = nswPs_getAllChambersGlobally();
  dyn_string channels;
  
  channel = nswPs_removeSystemName(channel);
  DebugN("Checking channel: " + channel);
  for (int c=1; c<=dynlen(chambers); c++)
  {
    channels = nswPs_getChannelsOfChamber(chambers[c]);
    for (int ch=1; ch<=dynlen(channels); ch++)
    {
      if (nswPs_removeSystemName(channels[ch]) == channel)
      {
        return 0;
      }
    }
  }
  return 1;
}

// +Generator+ <> 25.
bool nswPsUtil_isChannelSpareInPartition(string channel, string partition)
{
  dyn_string chambers = nswPs_getAllChambersOfPartition(partition);
  dyn_string channels;
  
  channel = nswPs_removeSystemName(channel);
  DebugN("Checking channel: " + channel);
  for (int c=1; c<=dynlen(chambers); c++)
  {
    channels = nswPs_getChannelsOfChamber(chambers[c]);
    for (int ch=1; ch<=dynlen(channels); ch++)
    {
      if (nswPs_removeSystemName(channels[ch]) == channel)
      {
        return 0;
      }
    }
  }
  return 1;
}


string nswPsUtil_getErrorFromStatus(int val)
{
  dyn_string err;
      
  if (val >= 32768)
     dynAppend(err, "tempErr");
   
   if (val%32768 >= 16384)
     dynAppend(err, "PwrFail");
  
   if (val%16384 >= 8192)
     dynAppend(err, "OverVProtect");
  
   if (val%8192 >= 4096)
     dynAppend(err, "UnderC");
   
   if (val%4096 >= 2048)
     dynAppend(err, "unplugged");
  
   if (val%2048 >= 1024)
     dynAppend(err, "calibError");
  
   if (val%1024 >= 512)
     dynAppend(err, "internalTrip");
  
   if (val%512 >= 256)
     dynAppend(err, "ExtDisable");
  
   if (val%256 >= 128)
     dynAppend(err, "OverHVMax");
   
   if (val%128 >= 64)
     dynAppend(err, "ExtTrip");
     
   if (val%64 >= 32)
     dynAppend(err, "UnderV");
   
   if (val%32 >= 16)
     dynAppend(err, "OverV");
 
   if (val%16 >= 8)
     dynAppend(err, "OverC");
 
   if (val%8 >= 4)
     dynAppend(err, "rDown");
 
   if (val%4 >= 2)
     dynAppend(err, "rUp");
   
   return(err);
}


/////////////////////////////////////////////////////////////////////////////
//////////////////     +Generators+    /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

// +Generator+ <> 1.
// Functions to access/control CAEN generators, via mdtCaenA348x DPs ...
bool nswPsUtil_generator_isOn(string genDp) {
  
  bool on;
  float volts;
  string dpe;
  
  if (!dpExists(genDp)) {
    error("nswPsUtil_generator_isOn: DP " + genDp + " does not exist. Aborting.");
    return FALSE;
  }
  
  dpGet(genDp + ".Power.State", dpe);
  if (!dpExists(dpe)) {
    error("nswPsUtil_generator_isOn: Power State DPE " + dpe + " does not exist. Aborting.");
    return FALSE;
  }
  dpGet(dpe, volts);
  on = (volts > 4.0) ? TRUE : FALSE;
  
  return on;
  
}

// +Generator+ <> 2.
bool nswPsUtil_generator_isLocked(string genDp) {
  
  bool interlock;
  
  if (!dpExists(genDp)) {
    error("nswPsUtil_generator_isLocked: DP " + genDp + " does not exist. Aborting.");
    return FALSE;
  }
  
  dpGet(genDp + ".DcsInterlock", interlock);
  return interlock;
  
}

// +Generator+ <> 3.
// Switching of CAEN generators, based on mdtCaenA348x DPs.
// This function should not be used in any high level UI where actions should be executed via the FSM.
bool nswPsUtil_generator_switchPower(string genDp, bool on) {
  
  string dpe;
  bool ret = TRUE;
  
  if (!dpExists(genDp)) {
    error("nswPsUtil_generator_switchPower: DP " + genDp + " does not exist. Aborting.");
    return FALSE;
  }
  if (on) dpGet(genDp + ".Power.SwitchOn", dpe);
  else dpGet(genDp + ".Power.SwitchOff", dpe);
  if (!dpExists(dpe)) {
    error("nswPsUtil_generator_switchPower Power Ctrl DPE " + dpe + " does not exist. Aborting.");
    return FALSE;
  }
  
  dpSetWait(dpe, TRUE);
  dpGet(genDp + ".Power.ReadState", dpe);
  dpSetWait(dpe, TRUE);
  dpGet(genDp + ".Power.ReadActuatorState", dpe);
  dpSetWait(dpe, TRUE);
  
  return ret;  
}


// +Generator+ <> 4.
// Switch on/off CAEN LV/HV channels connected to a given 48V generator
// Args: genDp: The mdtCaenA348x DP of the generator. type: HV or LV. on: TRUE/FALSE, switch on or off.
bool nswPsUtil_generator_switchConnectedChans(string genDp, string type, bool on) {
  
  bool ret = TRUE;
  dyn_string chambers, dpeList1, dpeList2, chans;
  dyn_anytype vals;
  dyn_dyn_string crates;
  bool isUPS;
  
  if (!dpExists(genDp)) {
    error("nswPsUtil_generator_switchConnectedChans: DP " + genDp + " does not exist. Aborting.");
    return FALSE;
  }
  else if ((type != "HV") && (type != "LV")) {
    error("nswPsUtil_generator_switchConnectedChans: Invalid connection type " + type + ". Aborting.");
    return FALSE;
  }
  
  crates = nswPsUtil_getGeneratorEasyCrateConnections(dpSubStr(genDp, DPSUB_DP), "Pw", "A");
  dynAppend(crates, nswPsUtil_getGeneratorEasyCrateConnections(dpSubStr(genDp, DPSUB_DP), "Pw", "B"));
  
  dpGet(genDp + ".UPS", isUPS);
  if (isUPS) {
    dynAppend(crates, nswPsUtil_getGeneratorEasyCrateConnections(dpSubStr(genDp, DPSUB_DP), "Srv", "A"));
    dynAppend(crates, nswPsUtil_getGeneratorEasyCrateConnections(dpSubStr(genDp, DPSUB_DP), "Srv", "B"));
  }
  
  dynUnique(crates);
  
// check if both barrel and endcap power system are connected, report an error otherwise
  
  if (!mdtUtil_isDistributedConnected(nswConstants_getBarrelPSProjectName()) ||
      !mdtUtil_isDistributedConnected(nswConstants_getEndcapPSProjectName())) {
    warning("nswPsUtil_generator_switchConnectedChans: Not all PS systems reachable, can not act on some channels.");
    ret = FALSE;
  }
  for (int i = 1; i <= dynlen(crates); i++) {
    nswPsUtil_queryChambers(chambers, type, crates[i][1], crates[i][2], crates[i][3]);
    if (type == "LV") {
      for (int i = 1; i <= dynlen(chambers); i++)  {
        if (mdtUtil_isBarrelChamber(chambers[i]) || mdtUtil_isRpcChamber(chambers[i])) 
          dynAppend(dpeList1, nswConstants_getBarrelPSProjectName() + chambers[i] + ".Mapping.LV");
        else
          dynAppend(dpeList2, nswConstants_getEndcapPSProjectName() + chambers[i] + ".Mapping.LV");
      }
    }
    else {
      for (int i = 1; i <= dynlen(chambers); i++)  {
        if (mdtUtil_isBarrelChamber(chambers[i]) || mdtUtil_isRpcChamber(chambers[i])) {
          dynAppend(dpeList1, nswConstants_getBarrelPSProjectName() + chambers[i] + ".Mapping.ML1");
          dynAppend(dpeList1, nswConstants_getBarrelPSProjectName() + chambers[i] + ".Mapping.ML2");
        }
        else {
          dynAppend(dpeList2, nswConstants_getEndcapPSProjectName() + chambers[i] + ".Mapping.ML1");
          dynAppend(dpeList2, nswConstants_getEndcapPSProjectName() + chambers[i] + ".Mapping.ML2");
        }
      }
    }
  }
  
  dynUnique(dpeList1); dynUnique(dpeList2);
// We need to handle barrel and endcap chambers/channels separately since they are on different systems
  
  dynClear(chans); dynClear(vals);
  if (dynlen(dpeList1)) dpGet(dpeList1, chans);
  for (int i = dynlen(chans); i >= 1; i--) {
    if (!dpExists(chans[i])) dynRemove(chans, i);
    else {
      chans[i] += ".settings.onOff"; 
      vals[i] = on;
    }
  } 
  if (dynlen(chans)) ret = ret && !dpSetWait(chans, vals);
  dynClear(chans); dynClear(vals);
  if (dynlen(dpeList2)) dpGet(dpeList2, chans);
  for (int i = dynlen(chans); i >= 1; i--) {
    if (!dpExists(chans[i])) dynRemove(chans, i);
    else {
      chans[i] += ".settings.onOff"; 
      vals[i] = on;
    }
  } 
  if (dynlen(chans)) ret = ret && !dpSetWait(chans, vals);
  
  return ret;
  
}
  

// +Generator+ <> 5.
string nswPsUtil_getGeneratorRack(string dpName) {
  
  string rack = "";
  dyn_string comp = strsplit(dpSubStr(dpName, DPSUB_DP), "/");

  for (int i = 1; i <= dynlen(comp); i++) {
    if (patternMatch("Y*S2", comp[i]) || patternMatch("Y*X[0123456789AC]", comp[i])) {
      rack = comp[i];
      break;
    }
  }
  
  return rack;
}  

// +Generator+ <> 6.
string nswPsUtil_getGeneratorChannel(string dpeName) {
  
  string channel = "";
  
  if (patternMatch("*ChanA*", dpeName))
    channel = "A";
  else if (patternMatch("*ChanB*", dpeName))
    channel = "B";

  return channel;
} 
  
// +Generator+ <> 6.
int nswPsUtil_getGeneratorNum(string dpName) {
 
  int num = -1;
  dyn_string comp;
  
  comp = strsplit(dpSubStr(dpName, DPSUB_DP), "/");
  
  for (int i = 1; i <= dynlen(comp); i++) {
    if (patternMatch("Generator*", comp[i])) {
      strreplace(comp[i], "Generator", "");
      num = (int) comp[i];
    }
  }
  
  return num;
}

// +Generator+ <> 7.
int nswPsUtil_getGeneratorFilterNum(string dpName) {
 
  int num = -1;
  dyn_string comp;
  
  comp = strsplit(dpSubStr(dpName, DPSUB_DP), "/");
  
  for (int i = 1; i <= dynlen(comp); i++) {
    if (patternMatch("PwFilter*", comp[i])) {
      strreplace(comp[i], "PwFilter", "");
      num = (int) comp[i];
    }
  }
  
  return num;
}

// +Generator+ <> 8.
// Get generators supplying power/service to any of the same crates as the generator given in dpName.
// DP dpName can be both of muPsGenerator and mdtCaenA348x type, following MDT naming
// conventions. chan: A, B, "", connType: Pw or Srv.
// Return value: dyn_dyn_string array with entries (muPsGenerator DP, chan)
dyn_dyn_string nswPsUtil_getGeneratorAssociatedGenerators(string dpName, string connType, string chan = "") {
  
  dyn_dyn_string generatorList;
  dyn_dyn_string crateList;
  dyn_string generators;
  
  dpName = dpSubStr(dpName, DPSUB_DP);
  dynClear(generatorList);
  crateList = nswPsUtil_getGeneratorEasyCrateConnections(dpName, "", chan);
  
  for (int i = 1; i <= dynlen(crateList); i++) {
    generators = nswPsUtil_getGenerators4EasyCrate(connType, crateList[i][1], crateList[i][2], crateList[i][3]);
    for (int j = 1; j <= dynlen(generators); j++) {
      string name = generators[j];   // e.g. Y0805S2:4A
      string ch = substr(name, strlen(name) - 1, 1);
      int gen = (int) substr(name, strlen(name) - 2, 1);
      if ((chan == "") && patternMatch("*/Generator" + gen, dpName) && 
          patternMatch("*" + substr(name, 0, 7) + "/*", dpName)) continue;   // skip own generator (chan)
      if ((chan == ch) && patternMatch("*/Generator" + gen, dpName) && 
          patternMatch("*" + substr(name, 0, 7) + "/*", dpName)) continue;
      
      name = "US15/" + substr(name, 0, 7) + "/Generator" + gen;
      dynAppend(generatorList, makeDynString(name, ch));
    }
  }
  
  dynUnique(generatorList);
  
  return generatorList;
  
}
/////////////////////////////////////////////////////////////////////////////
//////////////////     +FSM+    /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// +FSM+ <> 1.
// get the FSM domain name a chamber LV or HV channel belongs to
string nswPsUtil_getFsmLayerNodeName(string chamberName) {
  
  string node = "";
  string side, layer;
  
  side = substr(chamberName, 4, 1);
  if (side == "B") side = "A";
  layer = substr(chamberName, 1, 1);                       // O, M, E, I
  if (layer == "O") layer = "OUTER";
  else if (layer == "M") layer = "MIDDLE";
  else if (layer == "I") layer = "INNER";
  else if (layer == "E") layer = "EE";
    
  if (mdtUtil_isBarrelChamber(chamberName) || mdtUtil_isRpcChamber(chamberName))
    node = "nswPs_B" + side + "_" + layer;
  else if (mdtUtil_isEndcapChamber(chamberName))
    node = "nswPs_E" + side + "_" + layer;
  
  return node;
}


// +FSM+ <> 2.
// get possible LV JTAG FSM states and associated colors  
// when using this function outside a FSM UI fwFsm_initialize should have
// been called before.
void nswPsUtil_getFsmLVStatesColors(dyn_string &states, dyn_string &colors) {
  
  dynClear(states); dynClear(colors);
  dyn_string sysList = nswConstants_getPsProjectNames();
  bool connected = FALSE;
  
  for (int i = 1; i <= dynlen(sysList); i++) {
   unDistributedControl_isConnected(connected, sysList[i]);
   if (connected) {
     fwFsm_getObjectStatesColors(sysList[i] + "FwCaenChannelLV", states, colors);
     break;
   }
 }
 if (!connected) fwFsm_getObjectStatesColors("FwCaenChannelLV", states, colors);   // see if we have a copy of the FSM type
  
}

// +FSM+ <> 3.
// get possible HV JTAG FSM states and associated colors  
// when using this function outside a FSM UI fwFsm_initialize should have
// been called before.
void nswPsUtil_getFsmHVStatesColors(dyn_string &states, dyn_string &colors) {
  
  dynClear(states); dynClear(colors);
  dyn_string sysList = nswConstants_getPsProjectNames();
  bool connected = FALSE;
  
  for (int i = 1; i <= dynlen(sysList); i++) {
   unDistributedControl_isConnected(connected, sysList[i]);
   if (connected) {
     fwFsm_getObjectStatesColors(sysList[i] + "FwCaenChannelHV", states, colors);
     break;
   }
 }
 if (!connected) fwFsm_getObjectStatesColors("FwCaenChannelHV", states, colors);   // see if we have a copy of the FSM type
  
}


// +FSM+ <> 4.
string nswPsUtil_getHVChannelFSMType() {
  return "FwCaenChannelHV";
}

// +FSM+ <> 5.
string nswPsUtil_getLVChannelFSMType() {
  return "FwCaenChannelLV";
}








/////////////////////////////////////////////////////////////////////////////
//////////////////     +FileDumps+    /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

// +FileDumps+ <> 1.
bool nswPsUtil_dumpUX15CrateMap(string fileName, bool appendOnly = FALSE) {
  
  file filePtr;
  bool ret = TRUE;
  dyn_string dpList, comp;
  string mainframe;
  int err, brCtrl, crateId;  
  string sysName, subdet, rack, crate;
  
  sysName = nswConstants_getPsRackProjectName();
  
  if (appendOnly) filePtr = fopen(fileName, "a");
  else filePtr = fopen(fileName, "w");
  err = ferror(filePtr);
  if (err) {
    error("nswPsUtil_dumpUX15CrateMap: Error opening output file " + fileName + ". Aborting.");
    return FALSE;
  }
  
  dpList = dpNames(sysName + "*", "muPsRackUX15Crate");
  
  fprintf(filePtr, "%s\t\t%s\n", "UX15 Crate Map", mdtUtil_time2text(getCurrentTime()));
  fprintf(filePtr, "%s\n", "Rack  Crate" + '\t' + '\t' + "Mainframe" + '\t' + "BrCtrl" + '\t' + "CrateId");
  for (int i = 1; i <= dynlen(dpList); i++) {
    dpGet(dpList[i] + ".SubDetector", subdet);
    if (subdet != "MDT") continue;
    nswPsUtil_getEasyCrate4UX15Crate(dpList[i], mainframe, brCtrl, crateId);
    comp = strsplit(dpSubStr(dpList[i], DPSUB_DP), "/");
    rack = comp[2];
    crate = comp[3];
    //DebugN(rack, crate, mainframe, brCtrl, crateId);
    fprintf(filePtr, "%s  %s\t%s\t\t%d\t%d\n", rack, crate, mainframe, brCtrl, crateId);
  }
  fprintf(filePtr, "%s", "\n");  
 
  fclose(filePtr);
 return ret; 
}

// +FileDumps+ <> 2.
// Dump mapping information to a file: Generator to UX15 rack/crate map
bool nswPsUtil_dumpGeneratorMap(string fileName, bool appendOnly = FALSE) {
  
  file filePtr;
  bool ret = TRUE;
  dyn_string dpList, comp;
  int err;  
  string sysName;
  
  sysName = nswConstants_getPsRackProjectName();
  
  if (appendOnly) filePtr = fopen(fileName, "a");
  else filePtr = fopen(fileName, "w");
  err = ferror(filePtr);
  if (err) {
    error("nswPsUtil_dumpGeneratorMap: Error opening output file " + fileName + ". Aborting.");
    return FALSE;
  }
  
  dpList = dpNames(sysName + "*", "muPsGeneratorCrate");
  
  fprintf(filePtr, "%s\t\t%s\n", "Generator Map", mdtUtil_time2text(getCurrentTime()));
  fprintf(filePtr, "%s\n", "US15 Rack" + '\t' + "Generator" + '\t' + "Chan" + '\t' + "Power" + '\t' + "UX Rack");
  
  for (int i = 1; i <= dynlen(dpList); i++) {
    comp = strsplit(dpSubStr(dpList[i], DPSUB_DP), "/");
    if (comp[5] != "Pw") continue;
    //DebugN(comp[2], comp[3], comp[4], comp[5], comp[7] + " " + comp[8]);
    fprintf(filePtr, "%s\t%s\t%s\t%s\t%s\n", comp[2], comp[3], comp[4], comp[5], comp[7] + " " + comp[8]);
  } 
  fprintf(filePtr, "%s", "\n");
  for (int i = 1; i <= dynlen(dpList); i++) {
    comp = strsplit(dpSubStr(dpList[i], DPSUB_DP), "/");
    if (comp[5] != "Srv") continue;
    //DebugN(comp[2], comp[3], comp[4], comp[5], comp[7] + " " + comp[8]);
    fprintf(filePtr, "%s\t%s\t%s\t%s\t%s\n", comp[2], comp[3], comp[4], comp[5], comp[7] + " " + comp[8]);
  }   
 
  fclose(filePtr);
 return ret; 
}


// +FileDumps+ <> 3.
// Dump mapping information to a file: Chamber to Easy chain map
// type can be LV, ML1 or ML2, the latter 2 for the 2 multilayers HV
// Note: This function DOES include the "MDT-RPC" chambers BMR, BOR
bool nswPsUtil_dumpChamberMap(string fileName, string type, bool appendOnly = FALSE) {
  
  file filePtr;
  bool ret = TRUE;
  dyn_string dpList, comp;
  int brCtrl, crateId, board, chan;
  string mainframe, chamber, chanDp;
  int err;
  string sysName;
  
  sysName = nswConstants_getPsRackProjectName();
  
  if (appendOnly) filePtr = fopen(fileName, "a");
  else filePtr = fopen(fileName, "w");
  err = ferror(filePtr);
  if (err) {
    error("nswPsUtil_dumpChamberMap: Error opening output file " + fileName + ". Aborting.");
    return FALSE;
  }
  
  dpList = dpNames(nswConstants_getBarrelPSProjectName() + "*", nswPsUtil_PS_CHAMBER_DPT);
  dynAppend(dpList, dpNames(nswConstants_getEndcapPSProjectName() + "*", nswPsUtil_PS_CHAMBER_DPT));
  
  fprintf(filePtr, "%s\t\t%s\n", "Chamber Map " + type, mdtUtil_time2text(getCurrentTime()));
  fprintf(filePtr, "%s\t\t%s\n", "Chamber", "Mainframe-BrCtrl-CrateId-Board-Chan");
  for (int i = 1; i <= dynlen(dpList); i++) {
    chamber = dpSubStr(dpList[i], DPSUB_DP);
    if (!mdtUtil_isValidChamberName(chamber) && !mdtUtil_isRpcChamber(chamber)) continue;
    if (type == "LV")
      dpGet(dpList[i] + ".Mapping.LV", chanDp);
    else if (type == "ML1")
      dpGet(dpList[i] + ".Mapping.ML1", chanDp);
    else if (type == "ML2")
      dpGet(dpList[i] + ".Mapping.ML2", chanDp);
    else return FALSE;
    
    mainframe = nswPsUtil_getMainframeName(chanDp);
    brCtrl = nswPsUtil_getBranchCtrlNum(chanDp);
    crateId = nswPsUtil_getCrateNum(chanDp);
    board = nswPsUtil_getBoardNum(chanDp);
    chan = nswPsUtil_getChanNum(chanDp);
    
    //DebugN(chamber, mainframe + "-" + brCtrl + "-" + crateId + "-" + board + "-" + chan);
    if (i%20 == 0) DebugN("Processing ... " + i);
    fprintf(filePtr, "%s\t\t%s\n", chamber, mainframe + "-" + brCtrl + "-" + crateId + "-" + board + "-" + chan);
  }
  fprintf(filePtr, "%s", "\n");  
 
  fclose(filePtr);
 return ret; 
}




/////////////////////////////////////////////////////////////////////////////
//////////////////     +Other+    /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

// +Other+ <> 1.

string nswPsUtil_getWatchdogChannelOfPartition(string partition)
{
  if (partition == "B")
    return "CAEN/PSMDT02/branchController00/easyCrate0/easyBoard13/channel003";
  else if (partition == "E")
    return "CAEN/PSMDT01/branchController14/easyCrate0/easyBoard09/channel003";
}

// Function to touch dp given a board
// K. Karakostas & G. Iakovidis
int nswPs_touchChannelOfBoard(string systemName, int branchController,  
               int easyCrate, int easyBoard){
  
  int rc, rcS;
  anytype any;
  dyn_string typeDp;
  typeDp = makeDynString("i0", "rDwn", "rUp", "v0", "v1", "vMaxSoftValue", "tripTime");
  string branchControllerStr;
  string easyBoardStr;
  
  if (branchController<10) branchControllerStr = "0" + branchController;
    else branchControllerStr = branchController;
    
  if (easyBoard<10) easyBoardStr = "0" + easyBoard;
    else easyBoardStr = easyBoard;
  
  string mainframeStr = nswPs_getMainframeOfBr(branchController);
  string stringOfChannels = systemName+"CAEN/"+mainframeStr+"/branchController"
                            +branchControllerStr+"/easyCrate"+easyCrate+"/easyBoard"+easyBoardStr+"/*";
  dyn_string channelsToBeUpdated = dpNames(stringOfChannels,"FwCaenChannel");
  for (int i=1; i<=dynlen(channelsToBeUpdated); i++){
    for (int j=1; j<=dynlen(typeDp); j++){
     rc=dpGet(channelsToBeUpdated[i]+".settings."+typeDp[j], any);
     if (rc==0){
       rcS=dpSetWait(channelsToBeUpdated[i]+".settings."+typeDp[j], any);
       //DebugN(channelsToBeUpdated[i]+".settings."+typeDp[j], any);
     }
   }
  }
  if (rc==0) return 1;
  else return 0;
}



// Query

/////////////////////////////////////////////////////////////////////////////////////////
// return a list with the channels and their current values of a given DPE
/////////////////////////////////////////////////////////////////////////////////////////

dyn_dyn_anytype nswPsUtil_queryChanVals(string inSystem = "", string dpeName) {
  
  dyn_string sysList;
  string query;
  dyn_dyn_anytype tab;
  dyn_dyn_anytype res;
  bool connected;
 
  if (inSystem == "Local") dynAppend(sysList, getSystemName());
  else if (inSystem != "") dynAppend(sysList, inSystem);
  else sysList = makeDynString(nswPsGeneral_SystemMM(), nswPsGeneral_SystemsTGC());
  
  for (int i = 1; i <= dynlen(sysList); i++) {
    if (sysList[i] != getSystemName()) {
      connected = nswUtil_isDistributedConnected(sysList[i]);
      if (!connected) {
        warning("mdtPsUtil_queryChanVals: " + sysList[i] + " not connected, no query possible. skip.");
        continue;
      }
    }
    query = "SELECT '_online.._value' FROM '*." + dpeName + "*' REMOTE '" + sysList[i]+
            "' WHERE _DPT = \"FwCaenChannel\"  AND _EL = \"" + dpeName + "\"";
    dpQuery(query, tab);
    //DebugN(query, tab);
    if (dynlen(tab) > 1) {
      for (int j = 2; j <= dynlen(tab); j++) 
        dynAppend(res, makeDynAnytype(mdtUtil_dpSubStr(tab[j][1], DPSUB_SYS_DP), tab[j][2]));
    }
  }
  
  return res;
} 


/////////////////////////////////////////////////////////////////////////////////
// Return channels in state ON
/////////////////////////////////////////////////////////////////////////////////

dyn_string nswPsUtil_queryChansOn(string inSystem = "") {
 
  dyn_string sysList;
  string query;
  dyn_dyn_anytype tab;
  dyn_string chanList;
  bool connected;
  bool debugBool = TRUE;
 
  if (inSystem == "Local") dynAppend(sysList, getSystemName());
  else if (inSystem != "") dynAppend(sysList, inSystem);
  else sysList = makeDynString(nswPsGeneral_SystemMM(), nswPsGeneral_SystemsTGC());
  
  for (int i = 1; i <= dynlen(sysList); i++) {
    if (sysList[i] != getSystemName()) {
      connected = nswUtil_isDistributedConnected(sysList[i]);
      if (!connected) {
        warning("nswPsUtil_queryChansOn: " + sysList[i] + " not connected, no query possible. skip.");
        continue;
      }
    }
    query = "SELECT '_online.._value' FROM '*.actual.isOn*' REMOTE '" + sysList[i]+
            "' WHERE _DPT = \"FwCaenChannel\"" + "AND '_online.._value' == \"TRUE\"";
    dpQuery(query, tab);

    if(debugBool){    DebugN("output from queryChansOn():");
                      DebugN(query, tab);    }
    
    if (dynlen(tab) > 1) {
      for (int j = 2; j <= dynlen(tab); j++) 
        if (!dynContains(chanList, nswUtil_dpSubStr(tab[j][1], DPSUB_SYS_DP)))
          dynAppend(chanList, nswUtil_dpSubStr(tab[j][1], DPSUB_SYS_DP));
    }
  }
 
  return chanList;
}


//////////////////////////////////////////////////////////////////////////////
// Find the chambers connected to a given CAEN BrCtrl/Crate/Board
// using dpQuery with sql statements.
//////////////////////////////////////////////////////////////////////////////

  
dyn_string nswPsUtil_queryChambers4Board(string mainframe, int brCtrl, int crate, int board, 
                                         string inSystem = "", bool addML = FALSE, string type = "",
                                         int gLayer="") {
  //gLayer: indicator for the Layer ( 1=L1, 2=L2, 3=L3, 4=L4, 5=Drift)
  
  dyn_string chamberList;
  dyn_string sysList;
  string sBrCtrl, sCrate, sBoard;
  string query;
  dyn_dyn_anytype tab;
  bool connected;
 
  if (inSystem == "Local") dynAppend(sysList, getSystemName());
  else if (inSystem != "") dynAppend(sysList, inSystem);
  else sysList = makeDynString(nswPsGeneral_SystemMM(), nswPsGeneral_SystemsTGC());
  
   
  if (brCtrl == -1)
    sBrCtrl = "*";
  else if (brCtrl <= 9) 
    sBrCtrl = "0" + (string) brCtrl;
  else 
    sBrCtrl = (string) brCtrl;
  
  if (board == -1) 
    sBoard = "*";
  else if (board <= 9) 
    sBoard = "0" + (string) board;
  else 
    sBoard = (string) board;
  
  if (crate == -1) 
    sCrate = "*";
  else 
    sCrate = crate;
  

  for (int i = 1; i <= dynlen(sysList); i++) {
//     DebugN("systemlist[i]", sysList[i]);
    
    if (sysList[i] != getSystemName()) {
      connected = nswUtil_isDistributedConnected(sysList[i]);
      if (!connected) {
        warning("nswPsUtil_queryChambers4Board: " + sysList[i] + " not connected, no query possible. skip.");
        continue;
      }
    } 
    if ((type == "") || (type == "HV_MM")) {
//       DebugN("Query nsw_MM");
      
      if (gLayer != 5) query = "SELECT '_online.._value' FROM '*.Mapping.RO.L" + gLayer + "' REMOTE '" + sysList[i] + 
                               "' WHERE _DPT = \"nswChamberMM\" AND '_online.._value' LIKE \"*" + mainframe + "/board" + sBoard + "/*\"";
      
//     query = "SELECT '_online.._value' FROM '*." + dpeName + "*' REMOTE '" + sysList[i]+
//             "' WHERE _DPT = \"FwCaenChannel\"  AND _EL = \"" + dpeName + "\"";
    
      else if (gLayer = 5) query = "SELECT '_online.._value' FROM '*.Mapping.Drift' REMOTE '" + sysList[i] + 
                                   "' WHERE _DPT = \"nswChamberMM\" AND '_online.._value' LIKE \"*" + mainframe + "/board" + sBoard + "/*\"";
      dynClear(tab);
      dpQuery(query, tab);  
//       DebugN(query, tab);
      if (dynlen(tab) > 1) {
        for (int j = 2; j <= dynlen(tab); j++) {
          if (tab[j][2] == "") continue;
          dynAppend(chamberList, nswUtil_dpSubStr((string) tab[j][1], DPSUB_DP));
        }
      }
    }
    if ((type == "") || (type == "HV_sTGC")) {
      query = "SELECT '_online.._value' FROM '*.Mapping.RO.L" + gLayer + "' REMOTE '" + sysList[i] + 
              "' WHERE _DPT = \"nswChambersTGC\" AND '_online.._value' LIKE \"*" + mainframe + "/branchController" + 
                sBrCtrl + "/easyCrate" + sCrate + "/easyBoard" + sBoard + "/*\"";
      dynClear(tab);
      dpQuery(query, tab);
      //DebugN(query, tab);
      if (dynlen(tab) > 1) {
        for (int j = 2; j <= dynlen(tab); j++) {
          if (tab[j][2] == "") continue;
          dynAppend(chamberList, nswUtil_dpSubStr((string) tab[j][1], DPSUB_DP));
        }
      }
    }
  }
  return chamberList;
  
}


/////////////////////////////////////////////////////////////////////////////////
// Return the maximum value of a Channel DPE
/////////////////////////////////////////////////////////////////////////////////

float nswPsUtil_queryChanMaxVal(string inSystem = "", string dpeName) {
  
  dyn_string sysList;
  string query;
  dyn_dyn_anytype tab;
  float max = 0;
  bool connected;
 
  if (inSystem == "Local") dynAppend(sysList, getSystemName());
  else if (inSystem != "") dynAppend(sysList, inSystem);
  else sysList = makeDynString(nswPsGeneral_SystemMM(), nswPsGeneral_SystemsTGC());
  
  for (int i = 1; i <= dynlen(sysList); i++) {
    if (sysList[i] != getSystemName()) {
      connected = nswUtil_isDistributedConnected(sysList[i]);
      if (!connected) {
        warning("nswPsUtil_queryChanMaxVal: Remote system " + sysList[i] + " is not connected. Skipping.");
        continue;
      }
    }
    if (sysList[i] != getSystemName()) {
      connected = FALSE;
      unDistributedControl_isConnected(connected, sysList[i]);
      if (!connected) {
        warning("nswPsUtil_queryChanMaxVal: Remote system " + sysList[i] + " is not connected. Skipping.");
        continue;
      }
    }
    query = "SELECT 'MAX(_online.._value)' FROM '*." + dpeName + "*' REMOTE '" + sysList[i]+
            "' WHERE _DPT = \"FwCaenChannel\"  AND _EL = \"" + dpeName + "\"";
    dpQuery(query, tab);
    //DebugN(query, tab);
    if (dynlen(tab) > 1) {
      for (int j = 2; j <= dynlen(tab); j++) 
        if (tab[j][2] > max) max = tab[j][2];
    }
  }
  
  return max;
}

//////////////////////////////////////////////////////////////////////////////
// Return the minimum value of a Channel DPE
//////////////////////////////////////////////////////////////////////////////

float nswPsUtil_queryChanMinVal(string inSystem = "", string dpeName) {
  
  dyn_string sysList;
  string query;
  dyn_dyn_anytype tab;
  float min;
  bool connected;
 
  if (inSystem == "Local") dynAppend(sysList, getSystemName());
  else if (inSystem != "") dynAppend(sysList, inSystem);
  else sysList = makeDynString(nswPsGeneral_SystemMM(), nswPsGeneral_SystemsTGC());
  
  for (int i = 1; i <= dynlen(sysList); i++) {
    if (sysList[i] != getSystemName()) {
      connected = nswUtil_isDistributedConnected(sysList[i]);
      if (!connected) {
        warning("nswPsUtil_queryChanMinVal: " + sysList[i] + " not connected, no query possible. skip.");
        continue;
      }
    }
    query = "SELECT 'MIN(_online.._value)' FROM '*." + dpeName + "*' REMOTE '" + sysList[i]+
            "' WHERE _DPT = \"FwCaenChannel\"  AND _EL = \"" + dpeName + "\"";
    dpQuery(query, tab);
    //DebugN(query, tab);
    if (dynlen(tab) > 1) {
      min = tab[2][2];
      for (int j = 2; j <= dynlen(tab); j++) 
        if (tab[j][2] < min) min = tab[j][2];
    }
  }
  
  return min;
}

