
///////////////////////////////////////////////////////////////////////////////////
// |> T. Klapdor-Kleingrothaus, Uni Freiburg, 2018 <|
// Replaces last stored pressure value after correction is applied
///////////////////////////////////////////////////////////////////////////////////
void mmPs_EC_replaceLastPressure(double pressure)
{
  dpSet("dist_1:nswEnvLastPressure.actualvalue",pressure);
}


///////////////////////////////////////////////////////////////////////////////////
// |> T. Klapdor-Kleingrothaus, Uni Freiburg, 2018 <|
// Returns last stored pressure value
///////////////////////////////////////////////////////////////////////////////////
double mmPs_EC_getLastPressure()
{
  double pressure;
  dpGet("dist_1:nswEnvLastPressure.actualvalue",pressure);
  return pressure;
}

///////////////////////////////////////////////////////////////////////////////////
// Function returns the vertical distance between pressure meter and center of wheel
///////////////////////////////////////////////////////////////////////////////////
float mmPs_EC_PmeterHeight()
{
  // give vertical distance between pressure meter and center of wheel
  return 3.0; // in meter
}

///////////////////////////////////////////////////////////////////////////////////
// Function returns the current pressure
///////////////////////////////////////////////////////////////////////////////////
float mmPs_EC_Pinit()
{
  dyn_string device  = dpNames("*EnvPressure*");
  string Psensor = device[1];
  Psensor = Psensor + ".actualvalue";
  double pressure;
  dpGet(Psensor,pressure);
//   DebugN("mmPs_EC_Pinit P: "  + pressure);
  return pressure; 
}

///////////////////////////////////////////////////////////////////////////////////
// Function returns the current pressure
///////////////////////////////////////////////////////////////////////////////////
float mmPs_EC_Graviation()
{
  return 9.81;
}

///////////////////////////////////////////////////////////////////////////////////
// Function returns the current pressure
///////////////////////////////////////////////////////////////////////////////////
float mmPs_EC_GasDensity()
{
  
  float densityvalue = (mmPs_EC_Pinit()/1000)/(mmPs_EC_GasConstant()*(5463/20+mmPs_EC_MainTempSensor()))*100000000;
  return densityvalue;
//   return 1.675;  // standard ArCo2 mixture at 20�C 
}

float mmPs_EC_GasDensityIdeal()
{
  
//   float densityvalue = (mmPs_EC_Pinit()/1000)/(mmPs_EC_GasConstant()*(5463/20+mmPs_EC_MainTempSensor()))*100000000;
//   return densityvalue;
  return 1675;  // standard ArCo2 mixture at 20�C 
}

///////////////////////////////////////////////////////////////////////////////////
// Function returns the current pressure
///////////////////////////////////////////////////////////////////////////////////
float mmPs_EC_MainTempSensor()
{
  float temp ;
  dpGet("dist_1:nswEnvTempMain.actualvalue",temp);
  return temp;   // Joule/(kg*K)
}
///////////////////////////////////////////////////////////////////////////////////
// Function returns the current pressure
///////////////////////////////////////////////////////////////////////////////////
float mmPs_EC_GasConstant()
{
  return 206.756;   // Joule/(kg*K)
}
///////////////////////////////////////////////////////////////////////////////////
// |> T. Klapdor-Kleingrothaus, Uni Freiburg, 2018 <|
// Function returns the offset values for the voltage. 
// The gravitational pressure causes different pressures in the sector. 
// The voltage has to be adjusted.
///////////////////////////////////////////////////////////////////////////////////
float mmPs_EC_getHeightsOfChambers(string sector_string, string radial_string)
{
  int  heightselector_x,heightselector_y;
  int sector = (int)sector_string;
  int radial= (int)radial_string;
  float height;
  dyn_float height_from_center_radial1, height_from_center_radial2, height_from_center_radial3, height_from_center_radial4 ;
  height_from_center_radial1 = makeDynFloat(0,828.9,1610,2001.03,2276.9);
  height_from_center_radial2 = makeDynFloat(0,1462.2,2795.6,3530,3953.6);

  if(sector == 1 || sector == 9){heightselector_x = 1;}
  else if(sector == 2 || sector == 8  || sector == 10 ||sector == 16  ){heightselector_x = 2;}
  else if(sector == 3 || sector == 7 || sector == 11 ||sector == 15 ){heightselector_x = 3;}
  else if(sector == 4 || sector == 6|| sector == 12 ||sector == 14  ){heightselector_x = 4;}
  else if(sector == 5 || sector == 13){heightselector_x = 5;}
  
  if(radial == 1){height = height_from_center_radial1[heightselector_x];}
  if(radial == 2){height = height_from_center_radial2[heightselector_x];}
  if(radial == 3){height = height_from_center_radial3[heightselector_x];}
  if(radial == 4){height = height_from_center_radial4[heightselector_x];}
//   DebugN("I am here");
  if(sector > 9){ height *= -1;}
  return height;
}

///////////////////////////////////////////////////////////////////////////////////
// |> T. Klapdor-Kleingrothaus, Uni Freiburg, 2018 <|
// Function returns the offset values for the voltage. 
// The gravitational pressure causes different pressures in the sector. 
// The voltage has to be adjusted.
///////////////////////////////////////////////////////////////////////////////////
mmPs_EC_getPressureOffsetsRO(dyn_dyn_float &PressureOffset)
{
  dyn_float PressureOffsetR1, PressureOffsetR2;
  // R1
  double R1value_sector_01_09 = 0;
  double R1value_sector_02_08 = 2;
  double R1value_sector_03_07 = 4;
  double R1value_sector_04_06 = 6;
  double R1value_sector_05 = 8;
  double R1value_sector_10_16 = -2;
  double R1value_sector_11_15 = -4;
  double R1value_sector_12_14 = -6;
  double R1value_sector_13 = -8;

  double R2value_sector_01_09 = 0;
  double R2value_sector_02_08 = 2;
  double R2value_sector_03_07 = 4;
  double R2value_sector_04_06 = 6;
  double R2value_sector_05 = 8;
  double R2value_sector_10_16 = -2;
  double R2value_sector_11_15 = -4;
  double R2value_sector_12_14 = -6;
  double R2value_sector_13 = -8;
  
  // Sector 01 
  dynAppend(PressureOffsetR1,R1value_sector_01_09);
  // Sector 02 
  dynAppend(PressureOffsetR1,R1value_sector_02_08);
  // Sector 03 
  dynAppend(PressureOffsetR1,R1value_sector_03_07);
  // Sector 04 
  dynAppend(PressureOffsetR1,R1value_sector_04_06);
  // Sector 05 
  dynAppend(PressureOffsetR1,R1value_sector_05);
  // Sector 06 
  dynAppend(PressureOffsetR1,R1value_sector_04_06);
  // Sector 07 
  dynAppend(PressureOffsetR1,R1value_sector_03_07);
  // Sector 08 
  dynAppend(PressureOffsetR1,R1value_sector_02_08);
  // Sector 09 
  dynAppend(PressureOffsetR1,R1value_sector_01_09);
  // Sector 10 
  dynAppend(PressureOffsetR1,R1value_sector_10_16);
  // Sector 11 
  dynAppend(PressureOffsetR1,R1value_sector_11_15);
  // Sector 12 
  dynAppend(PressureOffsetR1,R1value_sector_12_14);
  // Sector 13 
  dynAppend(PressureOffsetR1,R1value_sector_13);
  // Sector 14 
  dynAppend(PressureOffsetR1,R1value_sector_12_14);
  // Sector 15 
  dynAppend(PressureOffsetR1,R1value_sector_11_15);
  // Sector 16
  dynAppend(PressureOffsetR1,R1value_sector_10_16);
  
  
    // Sector 01 
  dynAppend(PressureOffsetR2,R2value_sector_01_09);
  // Sector 02 
  dynAppend(PressureOffsetR2,R2value_sector_02_08);
  // Sector 03 
  dynAppend(PressureOffsetR2,R2value_sector_03_07);
  // Sector 04 
  dynAppend(PressureOffsetR2,R2value_sector_04_06);
  // Sector 05 
  dynAppend(PressureOffsetR2,R2value_sector_05);
  // Sector 06 
  dynAppend(PressureOffsetR2,R2value_sector_04_06);
  // Sector 07 
  dynAppend(PressureOffsetR2,R2value_sector_03_07);
  // Sector 08 
  dynAppend(PressureOffsetR2,R2value_sector_02_08);
  // Sector 09 
  dynAppend(PressureOffsetR2,R2value_sector_01_09);
  // Sector 10 
  dynAppend(PressureOffsetR2,R2value_sector_10_16);
  // Sector 11 
  dynAppend(PressureOffsetR2,R2value_sector_11_15);
  // Sector 12 
  dynAppend(PressureOffsetR2,R2value_sector_12_14);
  // Sector 13 
  dynAppend(PressureOffsetR2,R2value_sector_13);
  // Sector 14 
  dynAppend(PressureOffsetR2,R2value_sector_12_14);
  // Sector 15 
  dynAppend(PressureOffsetR2,R2value_sector_11_15);
  // Sector 16
  dynAppend(PressureOffsetR2,R2value_sector_10_16);
  
  dynAppend(PressureOffset,PressureOffsetR1);
  dynAppend(PressureOffset,PressureOffsetR2);
}

///////////////////////////////////////////////////////////////////////////////////
// returns a mapping of theoretical values of the density of a mixture of 
// ArCo2 (93:7) for different temperaturs (Celsius)
///////////////////////////////////////////////////////////////////////////////////
mapping mmPs_EC_densityTheory()
{
  mapping densitymap;
//     densitymap[<celsius>]= <gram/m^3>;
  densitymap[10]= 1708.1472752418706;
  densitymap[11]= 1702.1358472100496;
  densitymap[12]= 1696.1665824469071;
  densitymap[13]= 1690.2390389122334;
  densitymap[14]= 1684.3527807234393;
  densitymap[15]= 1678.5073780487094;
  densitymap[16]= 1672.7024070023713;
  densitymap[17]= 1666.9374495424283;
  densitymap[18]= 1661.2120933702065;
  densitymap[19]= 1655.5259318320573;
  densitymap[20]= 1649.8785638230788;
  densitymap[21]= 1644.2695936927948;
  densitymap[22]= 1638.698631152755;
  densitymap[23]= 1633.1652911860058;
  densitymap[24]= 1627.6691939583902;
  densitymap[25]= 1622.2099647316302;
  densitymap[26]= 1616.7872337781569;
  densitymap[27]= 1611.4006362976363;
  densitymap[28]= 1606.0498123351674;
  densitymap[29]= 1600.7344067010943;
  densitymap[30]= 1595.454068892415;
  densitymap[31]= 1590.2084530157342;
  densitymap[32]= 1584.997217711734;
  densitymap[33]= 1579.8200260811223;
  densitymap[34]= 1574.6765456120318;
  densitymap[35]= 1569.5664481088288;
  densitymap[36]= 1564.4894096223052;
  densitymap[37]= 1559.4451103812205;
  densitymap[38]= 1554.4332347251668;
  densitymap[39]= 1549.4534710387172;
  densitymap[40]= 1544.5055116868452;
  
  return densitymap;
}
