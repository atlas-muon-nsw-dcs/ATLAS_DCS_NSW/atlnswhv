#uses "mmPs_EC/mmPs_EC_Constants.ctl"
#uses "nswPsUtil.ctl"

bool showdebugmessages = true;
///////////////////////////////////////////////////////////////////////////////////
// |> T. Klapdor-Kleingrothaus, Uni Freiburg, 2018 <|
// Gets the current pressure value and calls the calculation function for
// the new values
///////////////////////////////////////////////////////////////////////////////////
mmPs_EC_Pcorr(string values, double value2)
{
  mapping gpressuremap, temperaturmap;
  mapping densitymap;
//   dyn_string chambernames = dpNames("EIZ2R2A01","nswChamberMM");
    dyn_string chambernames = dpNames("*","nswChamberMM");
  if(showdebugmessages){DebugN("Hello!\n This is |> mmPs_EC/mmPs_EC_Pcorr.ctl <|");}
  // get actual pressure value
    

    // get gravitational pressure correction values
    mapping gpressuremap = mmPs_EC_P_ChamberOffsets(chambernames);
    //     DebugN("gpressuremap  : " + gpressuremap );
    
    // get temperature values from chamber
    mapping temperaturmap = mmPs_EC_P_ChamberTemperatures(chambernames);
    //     DebugN("temperaturmap  : " + temperaturmap );
    
    // calculate density
    densitymap = mmPs_EC_DensityCalc(gpressuremap,temperaturmap);
//     for(int i = 1; i<= mappinglen(densitymap);i++)
//     {
//     string  Pmapchamber = mappingGetKey(gpressuremap,i);
//     string Pmapvalue = mappingGetValue(gpressuremap,i);
//     string  Tmapchamber = mappingGetKey(temperaturmap,i);
//     string Tmapvalue = mappingGetValue(temperaturmap,i);
//     string  densitychamber = mappingGetKey(densitymap,i);
//     string densityvalue = mappingGetValue(densitymap,i);
//     DebugN(densitychamber + " == " +  Pmapchamber + " densityvalue: " + densityvalue + " Tmapkey: " + Tmapchamber+ " Tmapvalue: " + Tmapvalue);
//   }
  // call correction function
  mmPs_EC_Vcalc_setRO(densitymap);
  mmPs_EC_Vcalc_setDrift(densitymap);

}

///////////////////////////////////////////////////////////////////////////////////
// read in the temperature values from the chambers
///////////////////////////////////////////////////////////////////////////////////
mapping mmPs_EC_P_ChamberTemperatures(dyn_string chambernames )
{
  mapping temperaturemap;
  dyn_dyn_string returnvalues;
  double temp;
  string chamber;
//   dyn_string chambernames = dpNames("*","nswChamberMM");
  for ( int i = 1; i <= dynlen(chambernames);i++)
  {
    dpGet(chambernames[i] + ".Env.Temp",temp);
      chamber = nswPsUtil_removeSystemName(chambernames[i]);
//     DebugN("This is mmPs_EC_P_ChamberTemperatures: \t" + chamber + ".Env.Temp",temp);
    temperaturemap[chamber] = temp;
  }
  return temperaturemap;
}

///////////////////////////////////////////////////////////////////////////////////
// delivers the values for the  graviational pressure correction
///////////////////////////////////////////////////////////////////////////////////
mapping mmPs_EC_P_ChamberOffsets(dyn_string chambernames )
{
  mapping Pmap;
//   dyn_string chambernames = dpNames("*","nswChamberMM");
  // get pressure
  double initpressure = mmPs_EC_Pinit();
  
  // constants
  double rho = mmPs_EC_GasDensity();
  double gravitation = mmPs_EC_Graviation();
 // make map for all chambers
  for ( int i = 1; i <= dynlen(chambernames);i++)
  {
    string chamber = chambernames[i];
    // get height difference from chamber
    string radial = nswPsUtil_getRadialfromChamber(chamber);
    string sector = nswPsUtil_getSectorfromChamber(chamber);
    double height = mmPs_EC_getHeightsOfChambers(sector, radial);
//      DebugN("sector: " + sector + " --> height is: " + height);
    double heightdifference = height/1000.0 - mmPs_EC_PmeterHeight();
//      DebugN("heightdifference is: " + heightdifference);

    // calculate new pressure
//      DebugN("initpressure: " + initpressure);
//      DebugN("rho: " + rho);
//      DebugN("gravitation: " + gravitation);
    double calcpressure = initpressure * exp(- rho * gravitation * heightdifference/(100000*initpressure));
    dpSet(chamber + ".Env.Pressure",calcpressure);
//      DebugN("Sec: " + sector + " R: " + radial + " height: " + heightdifference + " init: " + initpressure + " calc: " + calcpressure);
//     DebugN("This is mmPs_EC_P_ChamberTemperatures: \t Pressure in chamber is  is" + calcpressure );
    // pack it to a dyn_dyn_string with chambernames and values
    chamber = nswPsUtil_removeSystemName(chamber);
    Pmap[chamber] = calcpressure;
  }
  return Pmap;
}
///////////////////////////////////////////////////////////////////////////////////
// |> T. Klapdor-Kleingrothaus, Uni Freiburg, 2018 <|
// Gets the pressure offset and calculates HV value for each sector for Drift Layer
///////////////////////////////////////////////////////////////////////////////////
mmPs_EC_Pcalc_setDrift(double measuredpressure)
{
  if(showdebugmessages){DebugN("Hello!\n This is |> mmPs_EC/mmPs_EC_Pcorr.ctl <|");}
  // get offset correction factors
//   dyn_float newHVValues;
  dyn_dyn_float PressureOffset;
  mmPs_EC_getPressureOffsetsDrift(PressureOffset);
  // get current voltage settings
  dyn_float mmPs_EC_GetVoltage;
  
  // calculate the change of pressure
  double pressure_diff = measuredpressure - mmPs_EC_getLastPressure();
  
  string side, chamberNameR1, chamberNameR2;
  string HVmodule ;
  double voltage;

  for(int i_sector = 1; i_sector <= 16;i_sector++)
  {
    sprintf(chamberNameR1,"EIZ*R1*%02u",i_sector);
    sprintf(chamberNameR2,"EIZ*R2*%02u",i_sector);
    dyn_string chamberNamesR1 = dpNames(chamberNameR1,"nswChamberMM");
    dyn_string chamberNamesR2 = dpNames(chamberNameR2,"nswChamberMM");
    for(int i = 1; i<= 16;i++)
    {
      // R1
      dpGet(chamberNamesR1[i] + ".Mapping.Drift",HVmodule);
      dpGet(HVmodule + ".actual.vMon",voltage);
      voltage = PressureOffset[1][i_sector] * voltage;
//         DebugN(chamberNamesR1[i] + "\t" + HVmodule  + "\t == " + voltage + " V" );
     dpSetWait(HVmodule + ".settings.v1",voltage);
        
        //R2
//         DebugN("\n\n\n R2");
      dpGet(chamberNamesR2[i] + ".Mapping.Drift",HVmodule);
      dpGet(HVmodule + ".actual.vMon",voltage);
      voltage = PressureOffset[2][i_sector] * voltage;
//         DebugN(chamberNamesR2[i] + "\t" + HVmodule  + "\t == " + voltage + " V  ");
      dpSetWait(HVmodule + ".settings.v1",voltage);
      
    }
  }
  // update stored last pressure value
  mmPs_EC_replaceLastPressure(measuredpressure);
}

///////////////////////////////////////////////////////////////////////////////////
// Calculate the voltage from the densities for the RO layers
///////////////////////////////////////////////////////////////////////////////////
double mmPs_EC_VROCalcFromDensity(double vnom,  double  density)
{
//   DebugN("This is mmPs_EC_VROCalcFromDensity: density: " + density + " vnom: " + vnom);
  double const1= 0.1156266;
  double const2 =mmPs_EC_GasDensityIdeal();
//   double newvoltage = const1*density+vnom-(const1*const2);
  double newvoltage = 0.02*density+vnom;
  return newvoltage;
}
///////////////////////////////////////////////////////////////////////////////////
// Calculate the voltage from the densities for the drift layers
///////////////////////////////////////////////////////////////////////////////////
double mmPs_EC_VDriftCalcFromDensity(double vnom,  double  density)
{
  double newvoltage = vnom+exp(density*0.002);
  return newvoltage;
}
///////////////////////////////////////////////////////////////////////////////////
// Calculate the voltage from the densities and set it for the RO layers
///////////////////////////////////////////////////////////////////////////////////
mmPs_EC_Vcalc_setRO(mapping densitymap)
{
  if(showdebugmessages){DebugN("Hello!\n This is |> mmPs_EC/mmPs_EC_Vcalc_setRO.ctl <|");}
  for(int i = 1; i<=mappinglen(densitymap);i++)
  {
    string name_from_densitymap = mappingGetKey(densitymap,i);
    name_from_densitymap = nswPsUtil_removeSystemName(name_from_densitymap);
//     DebugN(" name from Dens Map: " + name_from_densitymap );
    double density = mappingGetValue(densitymap,i);
    for(int layers = 1; layers <= 4; layers++)
    {   
      double Vnom;
      dpGet( name_from_densitymap+".Vnom.RO.L" + layers, Vnom);
      double voltage = mmPs_EC_VROCalcFromDensity(Vnom, density);
      string caenchannel;
      dpGet(name_from_densitymap+".Mapping.RO.L" + layers, caenchannel);
//       DebugN("THis is mmPs_EC_Vcalc_setRO: --> caenchannel: " + caenchannel);
      dpSet( caenchannel+".settings.v0" , voltage);
//       DebugN("THis is mmPs_EC_Vcalc_setRO: --> Vnom: " + Vnom + "\t"  + " density: " + density  + "\t" +  name_from_densitymap+".Vnom.RO.L" + layers);
//       DebugN("This is mmPs_EC_Vcalc_setRO: density: " + density + " chamber: " + name_from_densitymap + " voltage: " + voltage);
    }
  }
}


///////////////////////////////////////////////////////////////////////////////////
// Calculate the voltage from the densities and set it for the Drift layers
///////////////////////////////////////////////////////////////////////////////////
mmPs_EC_Vcalc_setDrift(mapping densitymap)
{
  if(showdebugmessages){DebugN("Hello!\n This is |> mmPs_EC/mmPs_EC_Vcalc_setDrift.ctl <|");}
  for(int i = 1; i<=mappinglen(densitymap);i++)
  {
    string name_from_densitymap = mappingGetKey(densitymap,i);
    name_from_densitymap = nswPsUtil_removeSystemName(name_from_densitymap);
//     DebugN(" name from Dens Map: " + name_from_densitymap );
    double density = mappingGetValue(densitymap,i);  
    double Vnom;
    dpGet( name_from_densitymap+".Vnom.Drift" , Vnom);
    double voltage = mmPs_EC_VDriftCalcFromDensity(Vnom, density);
    string caenchannel;
    dpGet(name_from_densitymap+".Mapping.Drift", caenchannel);
//       DebugN("THis is mmPs_EC_Vcalc_setRO: --> caenchannel: " + caenchannel);
    dpSet( caenchannel+".settings.v0" , voltage);
//     DebugN("THis is mmPs_EC_Vcalc_setDrift: --> Vnom: " + Vnom + "\t"  + " density: " + density  + "\t" +  name_from_densitymap+".Vnom.RO.Drift");
//     DebugN("This is mmPs_EC_Vcalc_setDrift: density: " + density + " chamber: " + name_from_densitymap + " voltage: " + voltage);
    
  }
}
///////////////////////////////////////////////////////////////////////////////////
// calculates the Density for each chamber and stores it in a map
///////////////////////////////////////////////////////////////////////////////////
mapping mmPs_EC_DensityCalc(mapping pressure, mapping temperature)
{
   mapping densitymap;
  double gasconstant = mmPs_EC_GasConstant();
  for(int key = 1; key<=mappinglen(pressure);key++)
  {
    string  chamberP = mappingGetKey(pressure,key);
    string  chamberT = mappingGetKey(temperature,key);
//     DebugN("This is mmPs_EC_DensityCalc: P: " + chamberP + " T: " + chamberT);
    if(patternMatch(chamberP,chamberT))
    {
      double P = mappingGetValue(pressure,key);
      double T = mappingGetValue(temperature,key);
      double density = (P*100000.)/(gasconstant*(5463./20.+T));  // P[mbar] and T[Celsius]
//       if(key<50){DebugN("This is mmPs_EC_DensityCalc: " + chamberP + ": P: " + P + " T: " + T + " gasconstant: " + gasconstant + " density: " + density +" g/m^3");
//         DebugN("This is mmPs_EC_DensityCalc: (P*100000.): " + (P*100000.) + " (gasconstant*(5463/20+T)): " + (gasconstant*(5463/20+T)) +" g/m^3");}
      densitymap[chamberP] = density;
    }
  }
  return densitymap;
}

