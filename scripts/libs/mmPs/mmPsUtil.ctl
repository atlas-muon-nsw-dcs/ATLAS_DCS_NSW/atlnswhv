///////////////////////////////////////////////////////////////////////////////////
// |> T. Klapdor-Kleingrothaus, Uni Freiburg, 2018 <|
// 
///////////////////////////////////////////////////////////////////////////////////


// +Chamber+ <> 16.
dyn_string mmPsUtil_getAllChambersOfSectorandSide(anytype sector, string side)
{
  dyn_string chambers;
//   DebugN("This is nswPsUtil_getAllChambersOfSectorandSide: sector " +  sector + " side " + side);
//   dyn_string projects = mdtConstants_getPsProjectNames();
  string Sector;
  
  if ((int)sector < 10)
    Sector = "0" + (int)sector;
  else
    Sector = (int)sector;
//   for (int p=1; p<=dynlen(projects); p++)
//     dynAppend(chambers, dpNames(projects[p] + "?????" + Sector, "Fw_DUwithScript"));
    dynAppend(chambers, dpNames("??????" + side  + Sector, "nswChamberMM"));
  
  return chambers;
}
