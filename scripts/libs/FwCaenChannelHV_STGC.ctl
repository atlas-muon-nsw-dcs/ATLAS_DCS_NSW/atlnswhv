#uses "nswPs_deviceUnitHandling.ctl"
FwCaenChannelHV_STGC_initialize(string domain, string device)
{
//   DebugN("this is STGC init");
  nswPsHandl_Fsm_setupHvDuDpConnect_STGC(domain,device);
}

FwCaenChannelHV_STGC_valueChanged( string domain, string device, string &fwState )
{
}


FwCaenChannelHV_STGC_doCommand(string domain, string device, string command)
{
      DebugN("::::::::::::::::::::: THIS IST STGC _ DO COMMAND ::::::::::::::::::::");
      DebugN("Domain is: " + domain);
      DebugN("Device is: " + device);
      DebugN("command is: " + command);
	if (command == "SWITCH_OFF")
	{
                  int firstvalue, secondvalue;
                  dpGet(device+".actual.status",firstvalue);
		dpSet(device+".actual.status",0);
                  dpGet(device+".actual.status",secondvalue);
                  DebugN("Value was set from: " + firstvalue + " to: " + secondvalue);
	}
	if (command == "REFRESH")
	{
	}
	if (command == "SWITCH_ON")
	{
		int firstvalue, secondvalue;
                  dpGet(device+".actual.status",firstvalue);
		dpSet(device+".actual.status",1);
                  dpGet(device+".actual.status",secondvalue);
                  DebugN("Value was set from: " + firstvalue + " to: " + secondvalue);
	}
	if (command == "LOCK_UNLOCK")
	{
	}
	if (command == "RESET_TRIP")
	{
	}
}


